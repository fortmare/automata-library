cmake_minimum_required(VERSION 3.12)

project(algorithms_library_toolkit
	VERSION 0.0.0
	LANGUAGES CXX)

set(CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/CMake)
set(CMAKE_COLOR_MAKEFILE ON)

if (NOT UNIX)
    message(FATAL_ERROR "Unavailable if not Unix")
endif ()

# If not specified whether Debug or Release, select Release with debug info
if(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
	message(STATUS "Setting build type to 'RelWithDebInfo' as none was specified.")
	set(CMAKE_BUILD_TYPE RelWithDebInfo CACHE STRING "Choose the type of build." FORCE)
	set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS "Debug" "Release" "MinSizeRel" "RelWithDebInfo")
endif()

option(WITH_DOCS "Build Doxygen")
option(WITH_AGUI "Build agui2 interface (deprecated gui)")
option(BUILD_TESTING "Compile and run tests" ON)

include(alt)
include(CompilerFlags)
include(Install)
include(Version)

include_directories(${PROJECT_BINARY_DIR})

# testing
include(CTest)
if(BUILD_TESTING)
	include(Catch)
	add_subdirectory(lib/catch2)
	include_directories(${PROJECT_SOURCE_DIR}/tests)

	add_subdirectory(tests)
endif()

alt_module(alib2std)
alt_module(alib2measure)
alt_module(alib2abstraction)
alt_module(alib2common)
alt_module(alib2xml)

alt_module(alib2str)
alt_module(alib2data)
alt_module(alib2algo)
alt_module(alib2elgo)
alt_module(alib2algo_experimental)
alt_module(alib2data_experimental)
alt_module(alib2graph_data)
alt_module(alib2graph_algo)

alt_module(alib2raw)

alt_module(alib2aux)
alt_module(alib2dummy)

alt_module(alib2cli)
alt_module(alib2str_cli_integration)
alt_module(alib2raw_cli_integration)
alt_module(aql2)

if(WITH_AGUI)
	alt_module(alib2gui)
	alt_module(agui2)
endif()

# docs
if(WITH_DOCS)
	alt_doxygen()
endif()

