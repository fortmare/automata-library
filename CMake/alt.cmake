set(ALT_MODULES "")

function(alt_module name)
	if (NOT IS_DIRECTORY ${CMAKE_SOURCE_DIR}/${NAME})
		message(FATAL "${name} is not a directory")
	endif()
	add_subdirectory(${name})

	list(APPEND ALT_MODULES "${name}")
	set(ALT_MODULES ${ALT_MODULES} PARENT_SCOPE)
endfunction()

function(alt_executable name)
	add_executable(${name} "")
	alt__add_target(${name} "${ARGN}")

	install(TARGETS ${name} RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})
endfunction()

function(alt_library name)
	add_library(${name} SHARED "")
	alt__add_target(${name} "${ARGN}")

	set_target_properties(${name} PROPERTIES
		VERSION ${PROJECT_VERSION}
		SOVERSION ${PROJECT_VERSION_MAJOR}
		)

	install(TARGETS ${name} LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
	install(DIRECTORY src/
		DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/algorithms-library
		FILES_MATCHING
			PATTERN "*.h"
			PATTERN "*.hpp"
			PATTERN "*.hxx"
			REGEX "/[^.]+$"
		)
endfunction()

function(alt__add_target name)
	cmake_parse_arguments(ARGUMENTS "" "" "DEPENDS;INCLUDES;TEST_INCLUDES;TEST_DEPENDS" ${ARGN})
	# message("Registering target ${name} (dependencies: ${ARGUMENTS_DEPENDS}) (includes: ${ARGUMENTS_INCLUDES}) (test-includes: ${ARGUMENTS_TEST_INCLUDES})")

	target_include_directories(${name}
		PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/src
		PRIVATE ${ARGUMENTS_INCLUDES}
		)

	target_link_libraries(${name}
		PUBLIC ${ARGUMENTS_DEPENDS}
		)

	file(GLOB_RECURSE PROJECT_SOURCES LIST_DIRECTORIES false CONFIGURE_DEPENDS src/*)
	target_sources(${name} PRIVATE "${PROJECT_SOURCES}")

	if (BUILD_TESTING)
		alt__unittests(${name} "${ARGUMENTS_TEST_DEPENDS}" "${ARGUMENTS_TEST_INCLUDES}")
	endif()
endfunction()

function(alt__unittests name depends includes)
	# message(STATUS "Adding unittests ${name} (depends: ${depends}) (includes: ${includes})")
    set(test_executable "test_${name}")
	add_executable(${test_executable} "")

	file(GLOB_RECURSE TEST_SOURCES LIST_DIRECTORIES false CONFIGURE_DEPENDS test-src/*)
	target_sources(${test_executable} PRIVATE "${TEST_SOURCES}")

	target_link_libraries(${test_executable}
		PRIVATE catch2
		PRIVATE ${depends}
		)
	if(TARGET ${name})
		get_target_property(target_type ${name} TYPE)
		if (NOT (target_type STREQUAL "EXECUTABLE"))
			target_link_libraries(${test_executable}
				PRIVATE ${name}
			)
		endif()
	endif()

	target_include_directories(${test_executable}
		PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/src
		PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/test-src
		PRIVATE ${CMAKE_BINARY_DIR}/test-src
		PRIVATE ${includes}
	)

	catch_discover_tests(
		${test_executable}
		TEST_PREFIX "[${test_executable}]"
	)
endfunction()


function(alt_doxygen)
	find_package(Doxygen COMPONENTS dot)

	if (NOT(${DOXYGEN_FOUND}))
		message ( WARNING "[Doxygen] Doxygen not found. Target 'doxygen' not created" )
		return()
	endif()

	# construct input for doxygen
	set(ALT_DOXYGEN_INPUTS)
	foreach (module ${ALT_MODULES})
		if (EXISTS ${CMAKE_SOURCE_DIR}/${module}/src)
			list(APPEND ALT_DOXYGEN_INPUTS "${module}/src")
		endif()
	endforeach ()
	string (REPLACE ";" " " ALT_DOXYGEN_INPUTS "${ALT_DOXYGEN_INPUTS}")

	set(ALT_DOXYGEN_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/apidoc")

	configure_file (
		${CMAKE_SOURCE_DIR}/docs/Doxyfile.in
		${CMAKE_BINARY_DIR}/Doxyfile
	)

	file(MAKE_DIRECTORY ${CMAKE_BINARY_DIR}/doxygen)
	add_custom_target ( "doxygen"
		python3 ${CMAKE_SOURCE_DIR}/extra/scripts/doxygen.py "${CMAKE_SOURCE_DIR}" "${CMAKE_BINARY_DIR}/doxygen" "${ALT_DOXYGEN_INPUTS}"
		COMMAND doxygen ../Doxyfile
		WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/doxygen
		COMMENT "Generating API documentation with Doxygen"
		VERBATIM )

	message ( "[Doxygen] Doxyfile generated. Run target 'doxygen' to generate Doxygen XML output" )
endfunction()
