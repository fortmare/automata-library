/*
 * Random.h
 *
 *  Created on: 18. 3. 2019
 *	  Author: Jan Travnicek
 */

#pragma once

#include <alib/random>

namespace debug {

template < class T >
class Random {
public:
	static T random ( );
};

template < class T >
T Random < T >::random ( ) {
	return ext::random_devices::semirandom ( );
}

} /* namespace debug */

