/*
 * PairSetSecond.h
 *
 *  Created on: 20. 9. 2014
 *	  Author: Jan Travnicek
 */

#pragma once

#include <alib/set>
#include <alib/pair>

namespace dataAccess {

class PairSetSecond {
public:
	template < class First, class Second >
	static ext::set < Second > access ( const ext::set < ext::pair < First, Second > > & pairSet ) {
		ext::set < Second > res;
		for ( const ext::pair < First, Second > & pair : pairSet )
			res.insert ( pair.first );

		return res;
	}

};

} /* namespace dataAccess */

