/*
 * StringCompare.cpp
 *
 *  Created on: Apr 4, 2017
 *      Author: Tomas Pecka
 */

#include "StringCompare.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto StringCompareLinear2 = registration::AbstractRegister < compare::StringCompare, bool, const string::LinearString < > &, const string::LinearString < > & > ( compare::StringCompare::compare );
auto StringCompareCyclic2 = registration::AbstractRegister < compare::StringCompare, bool, const string::CyclicString < > &, const string::CyclicString < > & > ( compare::StringCompare::compare );

} /* namespace */
