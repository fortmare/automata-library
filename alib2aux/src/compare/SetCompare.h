/*
 * SetCompare.h
 *
 *  Created on: Jan 14, 2019
 *      Author: Tomas Pecka
 */

#pragma once

#include <alib/set>

namespace compare {

class SetCompare {
public:
	template < class T >
	static bool compare ( const ext::set < T > & a, const ext::set < T > & b ) {
		return a == b;
	}
};

} /* namespace compare */

