/*
 * SetCompare.cpp
 *
 *  Created on: Jna 14, 2019
 *      Author: Tomas Pecka
 */

#include "SetCompare.h"
#include <registration/AlgoRegistration.hpp>

#include <string/LinearString.h>

namespace {

auto SetCompareLinearString = registration::AbstractRegister < compare::SetCompare, bool, const ext::set < string::LinearString < > > &, const ext::set < string::LinearString < > > & > ( compare::SetCompare::compare );
auto SetCompareUnsignedInt  = registration::AbstractRegister < compare::SetCompare, bool, const ext::set < unsigned > &, const ext::set < unsigned > & > ( compare::SetCompare::compare );

} /* namespace */
