/*
 * Author: Radovan Cerveny
 */

#pragma once

#include <chrono>
#include <deque>
#include "../MeasurementTypes.hpp"

namespace measurements {

struct MeasurementFrame;

struct TimeDataFrame {
	using value_type = std::chrono::microseconds;

	std::chrono::time_point < std::chrono::high_resolution_clock > start;

	value_type duration;
	value_type inFrameDuration;

	static void init ( unsigned, measurements::stealth_vector < MeasurementFrame > & );
	static void update ( unsigned, measurements::stealth_vector < MeasurementFrame > & );

	static TimeDataFrame aggregate ( const std::vector < MeasurementFrame > & );
};

std::ostream & operator <<( std::ostream &, const std::chrono::microseconds & );
std::ostream & operator <<( std::ostream &, const TimeDataFrame & );
}

