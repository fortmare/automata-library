/*
 * Author: Radovan Cerveny
 */

#pragma once

#include <deque>
#include "../MeasurementTypes.hpp"

namespace measurements {

struct MeasurementFrame;
struct MemoryDataFrame;

struct MemoryHint {
	using frame_type = MemoryDataFrame;
	using value_type = ssize_t;
	enum class Type {
		NEW, DELETE
	};

	Type	   type;
	value_type size;
};

struct MemoryDataFrame {
	using value_type = ssize_t;

	value_type startHeapUsage;
	value_type endHeapUsage;

	value_type highWatermark;
	value_type inFrameHighWatermark;

	value_type currentHeapUsage;

	static void init ( unsigned, measurements::stealth_vector < MeasurementFrame > & );
	static void update ( unsigned, measurements::stealth_vector < MeasurementFrame > & );
	static void hint ( unsigned, measurements::stealth_vector < MeasurementFrame > &, MemoryHint );

	static MemoryDataFrame aggregate ( const std::vector < MeasurementFrame > & );
};

std::ostream & operator <<( std::ostream &, const MemoryDataFrame & );
}

