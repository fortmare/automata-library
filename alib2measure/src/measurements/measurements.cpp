/*
 * Author: Radovan Cerveny
 */

#include "measurements.hpp"

namespace measurements {

void start ( measurements::stealth_string name, measurements::Type type ) {
	if ( MeasurementEngine::OPERATIONAL )
		MeasurementEngine::INSTANCE.pushMeasurementFrame ( std::move ( name ), type );
}

void end ( ) {
	if ( MeasurementEngine::OPERATIONAL )
		MeasurementEngine::INSTANCE.popMeasurementFrame ( );
}

void reset ( ) {
	if ( MeasurementEngine::OPERATIONAL )
		MeasurementEngine::INSTANCE.resetMeasurements ( );
}

MeasurementResults results ( ) {
	if ( MeasurementEngine::OPERATIONAL )
		return MeasurementEngine::INSTANCE.getResults ( );
	else
		return MeasurementResults ( );
}

template < typename Hint >
void hint ( Hint hint ) {
	if ( MeasurementEngine::OPERATIONAL )
		MeasurementEngine::INSTANCE.hint ( std::move ( hint ) );
}

template void hint < MemoryHint > ( MemoryHint );
template void hint < CounterHint > ( CounterHint );

void counterInc ( const measurements::stealth_string & counterName, CounterHint::value_type val ) {
	hint ( CounterHint { counterName, CounterHint::Type::ADD, val } );
}

void counterDec ( const measurements::stealth_string & counterName, CounterHint::value_type val ) {
	hint ( CounterHint { counterName, CounterHint::Type::SUB, val } );
}

}
