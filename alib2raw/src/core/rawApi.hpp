/*
 * RawApi.hpp
 *
 * Created on: Apr 1, 2013
 * Author: Jan Travnicek
 */

#pragma once

namespace core {

template < typename T >
struct rawApi;

} /* namespace core */

