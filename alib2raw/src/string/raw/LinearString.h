/*
 * LinearString.h
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#pragma once

#include <string/LinearString.h>
#include <core/rawApi.hpp>

#include <iterator>

namespace core {

template < class SymbolType >
struct rawApi < string::LinearString < SymbolType > > {
	static string::LinearString < SymbolType > parse ( std::istream & input );
	static void compose ( std::ostream & output, const string::LinearString < SymbolType > & string );
};

template < class SymbolType >
string::LinearString < SymbolType > rawApi < string::LinearString < SymbolType > >::parse ( std::istream & input ) {
	ext::vector < SymbolType > data;

	std::istream_iterator < char > input_iter ( input );
	input >> std::noskipws;

	for ( ; input_iter != std::istream_iterator < char > ( ); ++ input_iter ) {
		data.push_back ( SymbolType ( * input_iter ) );
	}
	return string::LinearString < SymbolType > { data };
}

template < class SymbolType >
void rawApi < string::LinearString < SymbolType > >::compose ( std::ostream & output, const string::LinearString < SymbolType > & string ) {
	for(const SymbolType & symbol : string.getContent()) {
		output << ext::to_string ( symbol );
	}
}

} /* namespace core */

