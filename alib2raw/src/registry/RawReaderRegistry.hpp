/*
 * RawReaderRegistry.hpp
 *
 *  Created on: 11. 7. 2017
 *	  Author: Jan Travnicek
 */

#pragma once

#include <alib/memory>
#include <alib/string>
#include <alib/map>
#include <alib/typeinfo>

#include <abstraction/OperationAbstraction.hpp>

namespace abstraction {

class RawReaderRegistry {
public:
	class Entry {
	public:
		virtual std::shared_ptr < abstraction::OperationAbstraction > getAbstraction ( ) const = 0;

		virtual ~Entry ( ) = default;
	};

private:
	template < class Return >
	class EntryImpl : public Entry {
	public:
		std::shared_ptr < abstraction::OperationAbstraction > getAbstraction ( ) const override;
	};

	static ext::map < std::string, std::unique_ptr < Entry > > & getEntries ( );

public:
	static void unregisterRawReader ( const std::string & type );

	template < class ReturnType >
	static void unregisterRawReader ( ) {
		std::string type = ext::to_string < ReturnType > ( );
		unregisterRawReader ( type );
	}

	static void registerRawReader ( std::string type, std::unique_ptr < Entry > entry );

	template < class ReturnType >
	static void registerRawReader ( std::string type ) {
		registerRawReader ( std::move ( type ), std::unique_ptr < Entry > ( new EntryImpl < ReturnType > ( ) ) );
	}

	template < class ReturnType >
	static void registerRawReader ( ) {
		std::string type = ext::to_string < ReturnType > ( );
		registerRawReader < ReturnType > ( std::move ( type ) );
	}

	static std::shared_ptr < abstraction::OperationAbstraction > getAbstraction ( const std::string & type );
};

} /* namespace abstraction */

#include <abstraction/RawReaderAbstraction.hpp>

namespace abstraction {

template < class Return >
std::shared_ptr < abstraction::OperationAbstraction > RawReaderRegistry::EntryImpl < Return >::getAbstraction ( ) const {
	return std::make_shared < abstraction::RawReaderAbstraction < Return > > ( );
}

} /* namespace abstraction */

