/*
 * RawWriterAbstraction.hpp
 *
 *  Created on: 11. 7. 2017
 *	  Author: Jan Travnicek
 */

#pragma once

#include <abstraction/NaryOperationAbstraction.hpp>
#include <abstraction/ValueOperationAbstraction.hpp>

#include <factory/RawDataFactory.hpp>

namespace abstraction {

template < class ParamType >
class RawWriterAbstraction : virtual public NaryOperationAbstraction < const ParamType & >, virtual public ValueOperationAbstraction < std::string > {
public:
	std::shared_ptr < abstraction::Value > run ( ) override {
		std::shared_ptr < abstraction::Value > & param = std::get < 0 > ( this->getParams ( ) );
		return std::make_shared < abstraction::ValueHolder < std::string > > ( factory::RawDataFactory::toString ( abstraction::retrieveValue < const ParamType & > ( param ) ), true );
	}

};

} /* namespace abstraction */

