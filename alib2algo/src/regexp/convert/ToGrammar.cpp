/*
 * ToGrammar.cpp
 *
 *  Created on: 6. 3. 2014
 *	  Author: Jan Travnicek
 */

#include "ToGrammar.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ToGrammarFormalRegExp = registration::AbstractRegister < regexp::convert::ToGrammar, grammar::RightRG < DefaultSymbolType, ext::pair < DefaultSymbolType, unsigned > >, const regexp::FormalRegExp < > & > ( regexp::convert::ToGrammar::convert, "regexp" ).setDocumentation (
"Converts the regular expression into a grammar (@sa regexp::convert::ToGrammarRightRGGlushkov::convert).\n\
\n\
@param regexp the regular expression\n\
@return right regular grammar equivalent to original regular expression" );

auto ToGrammarUnboundedRegExp = registration::AbstractRegister < regexp::convert::ToGrammar, grammar::RightRG < DefaultSymbolType, ext::pair < DefaultSymbolType, unsigned > >, const regexp::UnboundedRegExp < > & > ( regexp::convert::ToGrammar::convert, "regexp" ).setDocumentation (
"Converts the regular expression into a grammar (@sa regexp::convert::ToGrammarRightRGGlushkov::convert).\n\
\n\
@param regexp the regular expression\n\
@return right regular grammar equivalent to original regular expression" );

} /* namespace */
