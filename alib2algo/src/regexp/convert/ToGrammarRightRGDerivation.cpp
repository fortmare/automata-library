/*
 * ToGrammarRightRGDerivation.cpp
 *
 *  Created on: 6. 3. 2014
 *	  Author: Tomas Pecka
 */

#include "ToGrammarRightRGDerivation.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ToGrammarRightRGDerivationUnboundedRegExp = registration::AbstractRegister < regexp::convert::ToGrammarRightRGDerivation, grammar::RightRG < DefaultSymbolType, unsigned >, const regexp::UnboundedRegExp < > & > ( regexp::convert::ToGrammarRightRGDerivation::convert, "regexp" ).setDocumentation (
"Implements conversion of regular expressions to regular grammars usign Brzozowski's derivation algorithm.\n\
\n\
@param regexp the regexp to convert\n\
@return right regular grammar generating the language described by the original regular expression" );

auto ToGrammarRightRGDerivationFormalRegExp = registration::AbstractRegister < regexp::convert::ToGrammarRightRGDerivation, grammar::RightRG < DefaultSymbolType, unsigned >, const regexp::FormalRegExp < > & > ( regexp::convert::ToGrammarRightRGDerivation::convert, "regexp" ).setDocumentation (
"Implements conversion of regular expressions to regular grammars usign Brzozowski's derivation algorithm.\n\
\n\
@param regexp the regexp to convert\n\
@return right regular grammar generating the language described by the original regular expression" );

} /* namespace */
