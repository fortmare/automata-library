/*
 * NumberModuloAutomaton.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: 9. 2. 2017
 *	  Author: Jan Travnicek
 */

#pragma once

#include <automaton/FSM/DFA.h>

namespace automaton {

namespace generate {

/**
 * Algorithm to generate an automaton recognising numbers in given base that have modulo equal to a concrete value.
 */
class NumberModuloAutomaton {
public:
	/**
	 * Generates an automaton recognising numbers in given base that have modulo equal to a concrete value.
	 *
	 * @param base the base of read number
	 * @param modulo the requested modulo
	 * @param result_modulo the final modulo
	 */
	static automaton::DFA < std::string, unsigned > generate ( unsigned base, unsigned modulo, unsigned result_modulo );
};

} /* namespace generate */

} /* namespace automaton */

