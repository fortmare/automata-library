/*
 * ReachableStates.cpp
 *
 *  Created on: 23. 3. 2014
 *	  Author: Tomas Pecka
 */

#include "ReachableStates.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ReachableStatesEpsilonNFA = registration::AbstractRegister < automaton::properties::ReachableStates, ext::set < DefaultStateType >, const automaton::EpsilonNFA < > & > ( automaton::properties::ReachableStates::reachableStates, "fsm" ).setDocumentation (
"Finds all reachable states of a finite automaton.\n\
Using closure implementation of the BFS algorithm.\n\
\n\
@param fsm automaton\n\
@return set of reachable states from the initial state of @p fsm" );

auto ReachableStatesNFA = registration::AbstractRegister < automaton::properties::ReachableStates, ext::set < DefaultStateType >, const automaton::NFA < > & > ( automaton::properties::ReachableStates::reachableStates, "fsm" ).setDocumentation (
"Finds all reachable states of a finite automaton.\n\
Using closure implementation of the BFS algorithm.\n\
\n\
@param fsm automaton\n\
@return set of reachable states from the initial state of @p fsm" );

auto ReachableStatesCompactNFA = registration::AbstractRegister < automaton::properties::ReachableStates, ext::set < DefaultStateType >, const automaton::CompactNFA < > & > ( automaton::properties::ReachableStates::reachableStates, "fsm" ).setDocumentation (
"Finds all reachable states of a finite automaton.\n\
Using closure implementation of the BFS algorithm.\n\
\n\
@param fsm automaton\n\
@return set of reachable states from the initial state of @p fsm" );

auto ReachableStatesExtendedNFA = registration::AbstractRegister < automaton::properties::ReachableStates, ext::set < DefaultStateType >, const automaton::ExtendedNFA < > & > ( automaton::properties::ReachableStates::reachableStates, "fsm" ).setDocumentation (
"Finds all reachable states of a finite automaton.\n\
Using closure implementation of the BFS algorithm.\n\
\n\
@param fsm automaton\n\
@return set of reachable states from the initial state of @p fsm" );

auto ReachableStatesMultiInitialStateNFA = registration::AbstractRegister < automaton::properties::ReachableStates, ext::set < DefaultStateType >, const automaton::MultiInitialStateNFA < > &  > ( automaton::properties::ReachableStates::reachableStates, "fsm" ).setDocumentation (
"Finds all reachable states of a finite automaton.\n\
Using closure implementation of the BFS algorithm.\n\
\n\
@param fsm automaton\n\
@return set of reachable states from the initial state of @p fsm" );

auto ReachableStatesDFA = registration::AbstractRegister < automaton::properties::ReachableStates, ext::set < DefaultStateType >, const automaton::DFA < > & > ( automaton::properties::ReachableStates::reachableStates, "fsm" ).setDocumentation (
"Finds all reachable states of a finite automaton.\n\
Using closure implementation of the BFS algorithm.\n\
\n\
@param fsm automaton\n\
@return set of reachable states from the initial state of @p fsm" );

auto ReachableStatesDFTA = registration::AbstractRegister < automaton::properties::ReachableStates, ext::set < DefaultStateType >, const automaton::DFTA < > & > ( automaton::properties::ReachableStates::reachableStates, "fta" ).setDocumentation (
"Finds all reachable states of a finite tree automaton.\n\
Using closure implementation of the BFS algorithm.\n\
\n\
@param fta automaton\n\
@return set of reachable states from states that read leaves of @p fta" );

auto ReachableStatesNFTA = registration::AbstractRegister < automaton::properties::ReachableStates, ext::set < DefaultStateType >, const automaton::NFTA < > & > ( automaton::properties::ReachableStates::reachableStates, "fta" ).setDocumentation (
"Finds all reachable states of a finite tree automaton.\n\
Using closure implementation of the BFS algorithm.\n\
\n\
@param fta automaton\n\
@return set of reachable states from states that read leaves of @p fta" );

} /* namespace */
