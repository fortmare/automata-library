/*
 * UndistinguishableStates.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: 27. 3. 2019
 *	  Author: Tomas Pecka
 */

#pragma once

#include <alib/set>
#include <alib/map>

#include <automaton/FSM/DFA.h>
#include <automaton/TA/DFTA.h>

#include <automaton/properties/DistinguishableStates.h>

namespace automaton {

namespace properties {

/**
 * Partition undistinguishable states in automata
 *
 * @sa DistinguishableStates
 */
class UndistinguishableStates {
	template < class StateType >
	static ext::set < ext::pair < StateType, StateType > > initial ( const ext::set < StateType > & states, const ext::set < StateType > & finals ) {
		ext::set < ext::pair < StateType, StateType > > init;

		for ( const StateType & a : states ) {
			for ( const StateType & b : states ) {
				if ( finals.count ( a ) == finals.count ( b ) ) {
					init.insert ( ext::make_pair ( a, b ) );
					init.insert ( ext::make_pair ( b, a ) );
				}
			}
		}

		return init;
	}

public:
	/**
	 * Creates state partitioning of undistinguishable states.
	 *
	 * @tparam SymbolType Type for input symbols.
	 * @tparam StateType Type for states.
	 * @param dfa automaton which states are to be partitioned
	 *
	 * @return state partitioning of undistinguishable states
	 */
	template < class SymbolType, class StateType >
	static ext::set < ext::pair < StateType, StateType > > undistinguishable ( const automaton::DFA < SymbolType, StateType > & dfa );

	/**
	 * Creates state partitioning of undistinguishable states-
	 *
	 * @tparam SymbolType Type for input symbols.
	 * @tparam RankType Type of ranks of input symbols.
	 * @tparam StateType Type for states.
	 * @param dfta automaton which states are to be partitioned
	 *
	 * @return state partitioning of undistinguishable states
	 */
	template < class SymbolType, class StateType >
	static ext::set < ext::pair < StateType, StateType > > undistinguishable ( const automaton::DFTA < SymbolType, StateType > & fta );
};

template < class SymbolType, class StateType >
ext::set < ext::pair < StateType, StateType > > UndistinguishableStates::undistinguishable ( const automaton::DFA < SymbolType, StateType > & dfa ) {
	ext::set < ext::pair < StateType, StateType > > undistinguishable = initial ( dfa.getStates ( ), dfa.getFinalStates ( ) );

	do {
		ext::set < ext::pair < StateType, StateType > > undistinguishable2 = undistinguishable;

		for ( const ext::pair < StateType, StateType > & iter : undistinguishable2 ) {
			for ( const std::pair < const ext::pair < StateType, SymbolType >, StateType > & transition : dfa.getTransitions ( ).equal_range ( ext::slice_comp ( iter.first ) ) ) {
				const auto & transition2 = dfa.getTransitions ( ).find ( std::make_pair ( iter.second, transition.first.second ) );

				if ( transition2 == dfa.getTransitions ( ).end ( ) || ! undistinguishable2.count ( ext::make_pair ( transition.second, transition2->second ) ) ) {
					// end up in dead state  - dead state is be distinguishable from every other state OR
					// end up in distinguishable states
					undistinguishable.erase ( ext::make_pair ( iter.first, iter.second ) );
					undistinguishable.erase ( ext::make_pair ( iter.second, iter.first ) );
				}
			}
		}
		if ( undistinguishable == undistinguishable2 )
			break;

	} while ( true );

	return undistinguishable;
}

template < class SymbolType, class StateType >
ext::set < ext::pair < StateType, StateType > > UndistinguishableStates::undistinguishable ( const automaton::DFTA < SymbolType, StateType > & fta ) {
	ext::set < ext::pair < StateType, StateType > > undistinguishable = initial ( fta.getStates ( ), fta.getFinalStates ( ) );

	do {
		ext::set < ext::pair < StateType, StateType > > undistinguishable2 = undistinguishable;

		for ( const ext::pair < StateType, StateType > & iter : undistinguishable2 ) {
			for ( const std::pair < const ext::pair < common::ranked_symbol < SymbolType >, ext::vector < StateType > >, StateType > & transition : fta.getTransitions ( ) ) {
				for ( size_t i = 0; i < transition.first.second.size ( ); ++ i ) {
					if ( transition.first.second [ i ] == iter . first ) {
						ext::vector < StateType > copy = transition.first.second;
						copy [ i ] = iter.second;

						const auto & transition2 = fta.getTransitions ( ).find ( std::make_pair ( transition.first.first, std::move ( copy ) ) );

						if ( transition2 == fta.getTransitions ( ).end ( ) || ! undistinguishable2.count ( ext::make_pair ( transition.second, transition2->second ) ) ) {
							// end up in dead state  - dead state is be distinguishable from every other state OR
							// end up in distinguishable states
							undistinguishable.erase ( ext::make_pair ( iter.first, iter.second ) );
							undistinguishable.erase ( ext::make_pair ( iter.second, iter.first ) );
						}
					}
				}
			}
		}
		if ( undistinguishable == undistinguishable2 )
			break;

	} while ( true );

	return undistinguishable;
}

} /* namespace properties */

} /* namespace automaton */

