/*
 * DistinguishableStates.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: 27. 3. 2019
 *	  Author: Tomas Pecka
 */

#pragma once

#include <alib/set>
#include <alib/map>

#include <automaton/FSM/DFA.h>
#include <automaton/TA/DFTA.h>

#include <automaton/TA/UnorderedDFTA.h>

namespace automaton {

namespace properties {

/**
 * Find all distinguishable pairs of states of DFA.
 * Implements table-filling algorithm, Hopcroft 2nd edition, 4.4.1
 */
class DistinguishableStates {
	template < class StateType >
	static ext::set < ext::pair < StateType, StateType > > initial ( const ext::set < StateType > & states, const ext::set < StateType > & finals ) {
		ext::set < ext::pair < StateType, StateType > > init;

		for ( const StateType & a : states ) {
			for ( const StateType & b : states ) {
				if ( finals.count ( a ) != finals.count ( b ) ) {
					init.insert ( ext::make_pair ( a, b ) );
					init.insert ( ext::make_pair ( b, a ) );
				}
			}
		}

		return init;
	}

public:
	/**
	 * Find all distinguishable pairs of states of DFA.
	 * Implements table-filling algorithm, Hopcroft 2nd edition, 4.4.1
	 * O(n^4) version. TODO quadratic-time version.
	 *
	 * @param fsm automaton
	 * @return set of pairs of distinguishable states of @p fsm
	 */
	template < class SymbolType, class StateType >
	static ext::set < ext::pair < StateType, StateType > > distinguishable ( const automaton::DFA < SymbolType, StateType > & fsm );

	template < class SymbolType, class StateType >
	static ext::set < ext::pair < StateType, StateType > > distinguishable ( const automaton::DFTA < SymbolType, StateType > & fta );

	template < class SymbolType, class StateType >
	static ext::set < ext::pair < StateType, StateType > > distinguishable ( const automaton::UnorderedDFTA < SymbolType, StateType > & fta );
};

template < class SymbolType, class StateType >
ext::set < ext::pair < StateType, StateType > > DistinguishableStates::distinguishable ( const automaton::DFA < SymbolType, StateType > & fsm ) {
	ext::set < ext::pair < StateType, StateType > > distinguishable = initial ( fsm.getStates ( ), fsm.getFinalStates ( ) );

	bool changed;
	do {
		changed = false;

		for ( const StateType & a : fsm.getStates ( ) ) {
			for ( const StateType & b : fsm.getStates ( ) ) {
				if ( distinguishable.count ( ext::make_pair ( a, b ) ) ) // we have not yet found this
					continue;

				for ( const SymbolType & symb : fsm.getInputAlphabet ( ) ) {
					auto trA = fsm.getTransitions ( ).find ( ext::make_pair ( a, symb ) );
					auto trB = fsm.getTransitions ( ).find ( ext::make_pair ( b, symb ) );

					if ( trA == fsm.getTransitions ( ).end ( ) && trB == fsm.getTransitions ( ).end ( ) ) {
						// end up in the same target state (dead state) -> not distinguishable
					} else if ( trA == fsm.getTransitions ( ).end ( ) || trB == fsm.getTransitions ( ).end ( ) || distinguishable.count ( ext::make_pair ( trA -> second, trB -> second ) ) ) {
						// end up in dead state  - dead state is be distinguishable from every other state OR
						// end up in distinguishable states
						distinguishable.insert ( ext::make_pair ( a, b ) );
						distinguishable.insert ( ext::make_pair ( b, a ) );
						changed = true;
					}
				}
			}
		}

	} while ( changed );

	return distinguishable;
}

template < class SymbolType, class StateType >
ext::set < ext::pair < StateType, StateType > > DistinguishableStates::distinguishable ( const automaton::DFTA < SymbolType, StateType > & fta ) {
	ext::set < ext::pair < StateType, StateType > > distinguishable = initial ( fta.getStates ( ), fta.getFinalStates ( ) );

	bool changed;
	do {
		changed = false;

		for ( const StateType & a : fta.getStates ( ) ) {
			for ( const StateType & b : fta.getStates ( ) ) {
				if ( distinguishable.count ( ext::make_pair ( a, b ) ) )
					continue;

				for ( const std::pair < const ext::pair < common::ranked_symbol < SymbolType >, ext::vector < StateType > >, StateType > & transition : fta.getTransitions ( ) ) {
					for ( size_t i = 0; i < transition.first.second.size ( ); ++ i ) {
						if ( transition.first.second [ i ] == a ) {
							ext::vector < StateType > copy = transition.first.second;
							copy [ i ] = b;

							const auto & transition2 = fta.getTransitions ( ).find ( std::make_pair ( transition.first.first, std::move ( copy ) ) );

							if ( transition2 == fta.getTransitions ( ).end ( ) || distinguishable.count ( ext::make_pair ( transition.second, transition2->second ) ) ) {
								// end up in dead state - dead state is be distinguishable from every other state OR
								// end up in distinguishable states
								distinguishable.insert ( ext::make_pair ( a, b ) );
								distinguishable.insert ( ext::make_pair ( b, a ) );
								changed = true;
							}
						}
					}
				}
			}
		}
	} while ( changed );

	return distinguishable;
}

template < class SymbolType, class StateType >
ext::set < ext::pair < StateType, StateType > > DistinguishableStates::distinguishable ( const automaton::UnorderedDFTA < SymbolType, StateType > & fta ) {
	ext::set < ext::pair < StateType, StateType > > distinguishable = initial ( fta.getStates ( ), fta.getFinalStates ( ) );

	bool changed;
	do {
		changed = false;

		for ( const StateType & a : fta.getStates ( ) ) {
			for ( const StateType & b : fta.getStates ( ) ) {
				if ( distinguishable.count ( ext::make_pair ( a, b ) ) )
					continue;

				for ( const std::pair < const ext::pair < common::ranked_symbol < SymbolType >, ext::multiset < StateType > >, StateType > & transition : fta.getTransitions ( ) ) {
					for ( const StateType & state : transition.first.second ) {
						if ( state == a ) {
							ext::multiset < StateType > copy = transition.first.second;
							copy.erase ( copy.find ( state ) );
							copy.insert ( b );

							const auto & transition2 = fta.getTransitions ( ).find ( std::make_pair ( transition.first.first, std::move ( copy ) ) );

							if ( transition2 == fta.getTransitions ( ).end ( ) || distinguishable.count ( ext::make_pair ( transition.second, transition2->second ) ) ) {
								// end up in dead state - dead state is be distinguishable from every other state OR
								// end up in distinguishable states
								distinguishable.insert ( ext::make_pair ( a, b ) );
								distinguishable.insert ( ext::make_pair ( b, a ) );
								changed = true;
							}
						}
					}
				}
			}
		}
	} while ( changed );

	return distinguishable;
}

} /* namespace properties */

} /* namespace automaton */

