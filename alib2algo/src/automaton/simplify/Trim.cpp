/*
 * Trim.cpp
 *
 *  Created on: 23. 3. 2014
 *	  Author: Tomas Pecka
 */

#include "Trim.h"

#include <automaton/FSM/ExtendedNFA.h>
#include <automaton/FSM/CompactNFA.h>
#include <automaton/FSM/EpsilonNFA.h>
#include <automaton/FSM/NFA.h>
#include <automaton/FSM/DFA.h>
#include <automaton/TA/DFTA.h>
#include <registration/AlgoRegistration.hpp>

namespace {

auto TrimDFA = registration::AbstractRegister < automaton::simplify::Trim, automaton::DFA < >, const automaton::DFA < > & > ( automaton::simplify::Trim::trim, "fsm" ).setDocumentation (
"Removes inaccessible and useless states from the given automaton. Uses first the @sa UselessStatesRemover and next @sa UnreachableStatesRemover in the process.\n\
\n\
@param fsm finite automaton or finite tree automaton to trim\n\
@return the trimmed automaton equivalent to @p fsm" );

auto TrimNFA = registration::AbstractRegister < automaton::simplify::Trim, automaton::NFA < >, const automaton::NFA < > & > ( automaton::simplify::Trim::trim, "fsm" ).setDocumentation (
"Removes inaccessible and useless states from the given automaton. Uses first the @sa UselessStatesRemover and next @sa UnreachableStatesRemover in the process.\n\
\n\
@param fsm finite automaton or finite tree automaton to trim\n\
@return the trimmed automaton equivalent to @p fsm" );

auto TrimMultiInitialStateNFA = registration::AbstractRegister < automaton::simplify::Trim, automaton::MultiInitialStateNFA < >, const automaton::MultiInitialStateNFA < > & > ( automaton::simplify::Trim::trim, "fsm" ).setDocumentation (
"Removes inaccessible and useless states from the given automaton. Uses first the @sa UselessStatesRemover and next @sa UnreachableStatesRemover in the process.\n\
\n\
@param fsm finite automaton or finite tree automaton to trim\n\
@return the trimmed automaton equivalent to @p fsm" );

auto TrimEpsilonNFA = registration::AbstractRegister < automaton::simplify::Trim, automaton::EpsilonNFA < >, const automaton::EpsilonNFA < > & > ( automaton::simplify::Trim::trim, "fsm" ).setDocumentation (
"Removes inaccessible and useless states from the given automaton. Uses first the @sa UselessStatesRemover and next @sa UnreachableStatesRemover in the process.\n\
\n\
@param fsm finite automaton or finite tree automaton to trim\n\
@return the trimmed automaton equivalent to @p fsm" );

auto TrimCompactNFA = registration::AbstractRegister < automaton::simplify::Trim, automaton::CompactNFA < >, const automaton::CompactNFA < > & > ( automaton::simplify::Trim::trim, "fsm" ).setDocumentation (
"Removes inaccessible and useless states from the given automaton. Uses first the @sa UselessStatesRemover and next @sa UnreachableStatesRemover in the process.\n\
\n\
@param fsm finite automaton or finite tree automaton to trim\n\
@return the trimmed automaton equivalent to @p fsm" );

auto TrimExtendedNFA = registration::AbstractRegister < automaton::simplify::Trim, automaton::ExtendedNFA < >, const automaton::ExtendedNFA < > & > ( automaton::simplify::Trim::trim, "fsm" ).setDocumentation (
"Removes inaccessible and useless states from the given automaton. Uses first the @sa UselessStatesRemover and next @sa UnreachableStatesRemover in the process.\n\
\n\
@param fsm finite automaton or finite tree automaton to trim\n\
@return the trimmed automaton equivalent to @p fsm" );

auto TrimDFTA = registration::AbstractRegister < automaton::simplify::Trim, automaton::DFTA < >, const automaton::DFTA < > & > ( automaton::simplify::Trim::trim, "fta" ).setDocumentation (
"Removes inaccessible and useless states from the given automaton. Uses first the @sa UselessStatesRemover and next @sa UnreachableStatesRemover in the process.\n\
\n\
@param fta finite tree automaton or finite tree automaton to trim\n\
@return the trimmed automaton equivalent to @p fta" );

auto TrimNFTA = registration::AbstractRegister < automaton::simplify::Trim, automaton::NFTA < >, const automaton::NFTA < > & > ( automaton::simplify::Trim::trim, "fta" ).setDocumentation (
"Removes inaccessible and useless states from the given automaton. Uses first the @sa UselessStatesRemover and next @sa UnreachableStatesRemover in the process.\n\
\n\
@param fta finite automaton or finite tree automaton to trim\n\
@return the trimmed automaton equivalent to @p fta" );

} /* namespace */
