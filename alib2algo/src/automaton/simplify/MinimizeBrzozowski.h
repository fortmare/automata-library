/*
 * MinimizeBrzozowski.cpp
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: 18. 11. 2014
 *	  Author: Tomas Pecka
 */

#pragma once

#include <automaton/FSM/DFA.h>
#include <automaton/FSM/NFA.h>
#include <automaton/FSM/MultiInitialStateNFA.h>

#include <automaton/transform/Reverse.h>
#include <automaton/determinize/Determinize.h>

namespace automaton {

namespace simplify {

/**
 * Minimization of finite automata.
 *
 * For finite automata, we implement Brzozowski's method. This method works also for the minimization of NFA to minimal DFA.
 *
 * Implements: Brzozowski, J.A.: Canonical regular expressions and minimal state graphs for definite events (1962)
 *
 * @sa automaton::simplify::Minimize
 * @sa automaton::simplify::MinimizeDistinguishableStates
 */
class MinimizeBrzozowski {
public:
	/**
	 * Minimizes the given automaton using Brzozowski's method.
	 *
	 * @tparam SymbolType Type for input symbols.
	 * @tparam StateType Type for states.
	 *
	 * @param dfa finite automaton to minimize.
	 *
	 * @return Minimal deterministic finite automaton equivalent to @p dfa
	 */
	template < class SymbolType, class StateType >
	static automaton::DFA < SymbolType, ext::set < ext::set < StateType > > > minimize(const automaton::DFA < SymbolType, StateType > & dfa);

	/**
	 * @overload
	 */
	template < class SymbolType, class StateType >
	static automaton::DFA < SymbolType, ext::set < ext::set < StateType > > > minimize(const automaton::NFA < SymbolType, StateType > & nfa);
};

template < class SymbolType, class StateType >
automaton::DFA < SymbolType, ext::set < ext::set < StateType > > > MinimizeBrzozowski::minimize(const automaton::DFA < SymbolType, StateType > & dfa) {
	return automaton::determinize::Determinize::determinize(automaton::transform::Reverse::convert(automaton::determinize::Determinize::determinize(automaton::transform::Reverse::convert(dfa))));
}

template < class SymbolType, class StateType >
automaton::DFA < SymbolType, ext::set < ext::set < StateType > > > MinimizeBrzozowski::minimize(const automaton::NFA < SymbolType, StateType > & nfa) {
	return automaton::determinize::Determinize::determinize(automaton::transform::Reverse::convert(automaton::determinize::Determinize::determinize(automaton::transform::Reverse::convert(nfa))));
}

} /* namespace simplify */

} /* namespace automaton */

