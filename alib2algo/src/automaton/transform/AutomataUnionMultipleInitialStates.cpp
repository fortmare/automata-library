/*
 * AutomataUnionMultipleInitialStates.cpp
 *
 *  Created on: 20. 11. 2014
 *	  Author: Tomas Pecka
 */

#include "AutomataUnionMultipleInitialStates.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto AutomataUnionEpsilonNFA = registration::AbstractRegister < automaton::transform::AutomataUnionMultipleInitialStates, automaton::MultiInitialStateEpsilonNFA < DefaultSymbolType, ext::pair < DefaultStateType, unsigned > >, const automaton::EpsilonNFA < > &, const automaton::EpsilonNFA < > & > ( automaton::transform::AutomataUnionMultipleInitialStates::unification, "first", "second" ).setDocumentation (
"Union of two automata using epsilon transitions.\n\
\n\
@param first First automaton (A1)\n\
@param second Second automaton (A2)\n\
@return multi initial state nondeterministic epsilon FA representing the union of two automata" );

auto AutomataUnionNFA = registration::AbstractRegister < automaton::transform::AutomataUnionMultipleInitialStates, automaton::MultiInitialStateNFA < DefaultSymbolType, ext::pair < DefaultStateType, unsigned > >, const automaton::NFA < > &, const automaton::NFA < > & > ( automaton::transform::AutomataUnionMultipleInitialStates::unification, "first", "second" ).setDocumentation (
"Union of two automata using epsilon transitions.\n\
\n\
@param first First automaton (A1)\n\
@param second Second automaton (A2)\n\
@return multi initial state nondeterministic FA representing the union of two automata" );

auto AutomataUnionDFA = registration::AbstractRegister < automaton::transform::AutomataUnionMultipleInitialStates, automaton::MultiInitialStateNFA < DefaultSymbolType, ext::pair < DefaultStateType, unsigned > >, const automaton::DFA < > &, const automaton::DFA < > & > ( automaton::transform::AutomataUnionMultipleInitialStates::unification, "first", "second" ).setDocumentation (
"Union of two automata using epsilon transitions.\n\
\n\
@param first First automaton (A1)\n\
@param second Second automaton (A2)\n\
@return multi initial state nondeterministic FA representing the union of two automata" );

auto AutomataUnionNFTA = registration::AbstractRegister < automaton::transform::AutomataUnionMultipleInitialStates, automaton::NFTA < DefaultSymbolType, ext::pair < DefaultStateType, unsigned > >, const automaton::NFTA < > &, const automaton::NFTA < > & > ( automaton::transform::AutomataUnionMultipleInitialStates::unification, "first", "second" ).setDocumentation (
"Union of two finite tree automata.\n\
\n\
@param first First automaton (A1)\n\
@param second Second automaton (A2)\n\
@return nondeterministic FTA representing the union of two automata" );

} /* namespace */
