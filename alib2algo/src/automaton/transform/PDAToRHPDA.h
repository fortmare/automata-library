/*
 * PDAToRHPDA.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: 23. 3. 2014
 *	  Author: Jan Travnicek
 */

#pragma once

#include <automaton/PDA/RealTimeHeightDeterministicNPDA.h>
#include <automaton/PDA/RealTimeHeightDeterministicDPDA.h>
#include <automaton/PDA/NPDA.h>
#include <automaton/PDA/DPDA.h>

namespace automaton {

namespace transform {

/**
 * Transforms a pushdown automaton to a real-time height-deterministic pushdown automaton (RHPDA).
 */
class PDAToRHPDA {
public:
	/**
	 * Transformation of a PDA to a RHPDA.
	 * @param pda automaton to transform
	 * @return RHPDA equivalent to @p automaton
	 */
	static automaton::RealTimeHeightDeterministicDPDA < > convert( const automaton::RealTimeHeightDeterministicDPDA < > & pda);

	/**
	 * @overload
	 */
	static automaton::RealTimeHeightDeterministicDPDA < > convert( const automaton::DPDA < > & pda);

	/**
	 * @overload
	 */
	static automaton::RealTimeHeightDeterministicNPDA < > convert( const automaton::RealTimeHeightDeterministicNPDA < > & pda);

	/**
	 * @overload
	 */
	static automaton::RealTimeHeightDeterministicNPDA < > convert( const automaton::NPDA < > & pda);
};

} /* namespace transform */

} /* namespace automaton */

