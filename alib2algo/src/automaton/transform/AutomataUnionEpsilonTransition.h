/*
 * AutomataUnionEpsilonTransition.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: 20. 11. 2014
 *	  Author: Tomas Pecka
 */

#pragma once

#include <automaton/FSM/EpsilonNFA.h>
#include <label/InitialStateLabel.h>
#include <common/createUnique.hpp>

namespace automaton {

namespace transform {

/**
 * Union of two finite automata.
 * For finite automata A1, A2, we create a finite automaton A such that L(A) = L(A1) \cup L(A2).
 * This method utilizes epsilon transitions in finite automata (Melichar: Jazyky a překlady, 2.73).
 */
class AutomataUnionEpsilonTransition {
public:
	/**
	 * Union of two automata using epsilon transitions.
	 * @tparam SymbolType Type for input symbols.
	 * @tparam StateType Type for states.
	 * @param first First automaton (A1)
	 * @param second Second automaton (A2)
	 * @return epsilon nondeterministic FA representing the union of two automata
	 */
	template < class SymbolType, class StateType >
	static automaton::EpsilonNFA < SymbolType, ext::pair < StateType, unsigned > > unification(const automaton::EpsilonNFA < SymbolType, StateType > & first, const automaton::EpsilonNFA < SymbolType, StateType > & second);

	/**
	 * @overload
	 */
	template < class SymbolType, class StateType >
	static automaton::EpsilonNFA < SymbolType, ext::pair < StateType, unsigned > > unification(const automaton::NFA < SymbolType, StateType > & first, const automaton::NFA < SymbolType, StateType > & second);

	/**
	 * @overload
	 */
	template < class SymbolType, class StateType >
	static automaton::EpsilonNFA < SymbolType, ext::pair < StateType, unsigned > > unification(const automaton::DFA < SymbolType, StateType > & first, const automaton::DFA < SymbolType, StateType > & second);

};

template < class SymbolType, class StateType >
automaton::EpsilonNFA < SymbolType, ext::pair < StateType, unsigned > > AutomataUnionEpsilonTransition::unification(const automaton::EpsilonNFA < SymbolType, StateType > & first, const automaton::EpsilonNFA < SymbolType, StateType > & second) {
	unsigned firstDefault = 1;
	unsigned secondDefault = 2;

	ext::pair < StateType, unsigned > q0 = ext::make_pair ( label::InitialStateLabel::instance < StateType > ( ), 0 );
	automaton::EpsilonNFA < SymbolType, ext::pair < StateType, unsigned > > res ( q0 );

	for(const auto& a : first.getInputAlphabet())
		res.addInputSymbol(a);
	for(const auto& a : second.getInputAlphabet())
		res.addInputSymbol(a);

	for ( const auto & q : first.getStates ( ) )
		res.addState ( ext::make_pair ( q, firstDefault ) );
	for ( const auto & q : second.getStates ( ) )
		res.addState ( ext::make_pair ( q, secondDefault ) );

	for(const auto& q : first.getFinalStates())
		res.addFinalState ( ext::make_pair ( q, firstDefault ) );
	for(const auto& q : second.getFinalStates())
		res.addFinalState ( ext::make_pair ( q, secondDefault ) );

	res.addTransition ( q0, ext::make_pair ( first.getInitialState ( ), firstDefault ) );
	res.addTransition ( q0, ext::make_pair ( second.getInitialState ( ), secondDefault ) );

	for(const auto& t : first.getTransitions())
		res.addTransition ( ext::make_pair ( t.first.first, firstDefault ), t.first.second, ext::make_pair ( t.second, firstDefault ) );

	for(const auto& t : second.getTransitions())
		res.addTransition ( ext::make_pair ( t.first.first, secondDefault ), t.first.second, ext::make_pair ( t.second, secondDefault ) );

	return res;
}

template < class SymbolType, class StateType >
automaton::EpsilonNFA < SymbolType, ext::pair < StateType, unsigned > > AutomataUnionEpsilonTransition::unification(const automaton::NFA < SymbolType, StateType > & first, const automaton::NFA < SymbolType, StateType > & second) {
	unsigned firstDefault = 1;
	unsigned secondDefault = 2;

	ext::pair < StateType, unsigned > q0 = ext::make_pair ( label::InitialStateLabel::instance < StateType > ( ), 0 );
	automaton::EpsilonNFA < SymbolType, ext::pair < StateType, unsigned > > res ( q0 );

	for(const auto& a : first.getInputAlphabet())
		res.addInputSymbol(a);
	for(const auto& a : second.getInputAlphabet())
		res.addInputSymbol(a);

	for ( const auto & q : first.getStates ( ) )
		res.addState ( ext::make_pair ( q, firstDefault ) );
	for ( const auto & q : second.getStates ( ) )
		res.addState ( ext::make_pair ( q, secondDefault ) );

	for(const auto& q : first.getFinalStates())
		res.addFinalState ( ext::make_pair ( q, firstDefault ) );
	for(const auto& q : second.getFinalStates())
		res.addFinalState ( ext::make_pair ( q, secondDefault ) );

	res.addTransition ( q0, ext::make_pair ( first.getInitialState ( ), firstDefault ) );
	res.addTransition ( q0, ext::make_pair ( second.getInitialState ( ), secondDefault ) );

	for(const auto& t : first.getTransitions())
		res.addTransition ( ext::make_pair ( t.first.first, firstDefault ), t.first.second, ext::make_pair ( t.second, firstDefault ) );

	for(const auto& t : second.getTransitions())
		res.addTransition ( ext::make_pair ( t.first.first, secondDefault ), t.first.second, ext::make_pair ( t.second, secondDefault ) );

	return res;
}

template < class SymbolType, class StateType >
automaton::EpsilonNFA < SymbolType, ext::pair < StateType, unsigned > > AutomataUnionEpsilonTransition::unification(const automaton::DFA< SymbolType, StateType > & first, const automaton::DFA < SymbolType, StateType > & second) {
	unsigned firstDefault = 1;
	unsigned secondDefault = 2;

	ext::pair < StateType, unsigned > q0 = ext::make_pair ( label::InitialStateLabel::instance < StateType > ( ), 0 );
	automaton::EpsilonNFA < SymbolType, ext::pair < StateType, unsigned > > res ( q0 );

	for(const auto& a : first.getInputAlphabet())
		res.addInputSymbol(a);
	for(const auto& a : second.getInputAlphabet())
		res.addInputSymbol(a);

	for ( const auto & q : first.getStates ( ) )
		res.addState ( ext::make_pair ( q, firstDefault ) );
	for ( const auto & q : second.getStates ( ) )
		res.addState ( ext::make_pair ( q, secondDefault ) );

	for(const auto& q : first.getFinalStates())
		res.addFinalState ( ext::make_pair ( q, firstDefault ) );
	for(const auto& q : second.getFinalStates())
		res.addFinalState ( ext::make_pair ( q, secondDefault ) );

	res.addTransition ( q0, ext::make_pair ( first.getInitialState ( ), firstDefault ) );
	res.addTransition ( q0, ext::make_pair ( second.getInitialState ( ), secondDefault ) );

	for(const auto& t : first.getTransitions())
		res.addTransition ( ext::make_pair ( t.first.first, firstDefault ), t.first.second, ext::make_pair ( t.second, firstDefault ) );

	for(const auto& t : second.getTransitions())
		res.addTransition ( ext::make_pair ( t.first.first, secondDefault ), t.first.second, ext::make_pair ( t.second, secondDefault ) );

	return res;
}

} /* namespace transform */

} /* namespace automaton */

