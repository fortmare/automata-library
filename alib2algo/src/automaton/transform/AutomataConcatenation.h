/*
 * AutomataConcatenation.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: 20. 11. 2014
 *	  Author: Tomas Pecka
 */

#pragma once

#include <automaton/FSM/NFA.h>
#include <automaton/FSM/DFA.h>

#include <label/InitialStateLabel.h>

namespace automaton {

namespace transform {

/**
 * Concatenation of two finite automata.
 * For finite automata A1, A2, we create a finite automaton A such that L(A) = L(A1).L(A2).
 * This method does not utilize epsilon transitions in the resulting finite automata (Melichar: Jazyky a překlady, 2.82).
 */
class AutomataConcatenation {
public:
	/**
	 * Concatenates two finite automata without using epsilon transitions.
	 * @tparam SymbolType Type for input symbols.
	 * @tparam StateType Type for states.
	 * @param first First automaton (A1)
	 * @param second Second automaton (A2)
	 * @return nondeterministic FA representing the concatenation of two automata
	 */
	template < class SymbolType, class StateType >
	static automaton::NFA < SymbolType, ext::pair < StateType, unsigned > > concatenation(const automaton::DFA < SymbolType, StateType > & first, const automaton::DFA < SymbolType, StateType > & second);

	/**
	 * @overload
	 */
	template < class SymbolType, class StateType >
	static automaton::NFA < SymbolType, ext::pair < StateType, unsigned > > concatenation(const automaton::NFA < SymbolType, StateType > & first, const automaton::NFA < SymbolType, StateType > & second);
};

template < class SymbolType, class StateType >
automaton::NFA < SymbolType, ext::pair < StateType, unsigned > > AutomataConcatenation::concatenation(const automaton::NFA < SymbolType, StateType > & first, const automaton::NFA < SymbolType, StateType > & second) {
	unsigned firstDefault = 1;
	unsigned secondDefault = 2;

	automaton::NFA < SymbolType, ext::pair < StateType, unsigned > > res ( ext::make_pair ( first.getInitialState ( ), firstDefault ) );

	for ( const StateType & q : first.getStates ( ) )
		res.addState ( ext::make_pair ( q, firstDefault ) );
	for ( const StateType & q : second.getStates ( ) )
		res.addState ( ext::make_pair ( q, secondDefault ) );

	res.addInputSymbols ( first.getInputAlphabet ( ) );
	res.addInputSymbols ( second.getInputAlphabet ( ) );

	for ( const auto & t : first.getTransitions ( ) ) {
		res.addTransition ( ext::make_pair ( t.first.first, firstDefault ), t.first.second, ext::make_pair ( t.second, firstDefault ) );

		if(first.getFinalStates().count(t.second) > 0)
			res.addTransition ( ext::make_pair ( t.first.first, firstDefault ), t.first.second, ext::make_pair ( second.getInitialState ( ), secondDefault ) );
	}

	for ( const auto& t : second.getTransitions ( ) )
		res.addTransition ( ext::make_pair ( t.first.first, secondDefault ), t.first.second, ext::make_pair ( t.second, secondDefault ) );

	for ( const StateType & q : second.getFinalStates ( ) )
		res.addFinalState ( ext::make_pair ( q, secondDefault ) );

	if ( first.getFinalStates ( ).count ( first.getInitialState ( ) ) > 0 ) {
		ext::pair < StateType, unsigned > q01q02 ( label::InitialStateLabel::instance < StateType > ( ), 0 );

		res.addState ( q01q02 );

		res.setInitialState ( q01q02 );

		for ( const auto & t : first.getTransitionsFromState ( first.getInitialState ( ) ) )
			res.addTransition ( q01q02, t.first.second, ext::make_pair ( t.second, firstDefault ) );

		for ( const auto & t : second.getTransitionsFromState ( second.getInitialState ( ) ) )
			res.addTransition ( q01q02, t.first.second, ext::make_pair ( t.second, secondDefault ) );

		res.addFinalState ( q01q02 );
	}

	return res;
}

template < class SymbolType, class StateType >
automaton::NFA < SymbolType, ext::pair < StateType, unsigned > > AutomataConcatenation::concatenation(const automaton::DFA < SymbolType, StateType > & first, const automaton::DFA < SymbolType, StateType > & second) {
	unsigned firstDefault = 1;
	unsigned secondDefault = 2;

	automaton::NFA < SymbolType, ext::pair < StateType, unsigned > > res ( ext::make_pair ( first.getInitialState ( ), firstDefault ) );

	for ( const StateType & q : first.getStates ( ) )
		res.addState ( ext::make_pair ( q, firstDefault ) );
	for ( const StateType & q : second.getStates ( ) )
		res.addState ( ext::make_pair ( q, secondDefault ) );

	res.addInputSymbols ( first.getInputAlphabet ( ) );
	res.addInputSymbols ( second.getInputAlphabet ( ) );

	for ( const auto & t : first.getTransitions ( ) ) {
		res.addTransition ( ext::make_pair ( t.first.first, firstDefault ), t.first.second, ext::make_pair ( t.second, firstDefault ) );

		if ( first.getFinalStates ( ).count ( t.second ) > 0)
			res.addTransition ( ext::make_pair ( t.first.first, firstDefault ), t.first.second, ext::make_pair ( second.getInitialState ( ), secondDefault ) );
	}
	for ( const auto & t : second.getTransitions ( ) )
		res.addTransition ( ext::make_pair ( t.first.first, secondDefault ), t.first.second, ext::make_pair ( t.second, secondDefault ) );

	for(const StateType & q : second.getFinalStates ( ) )
		res.addFinalState ( ext::make_pair ( q, secondDefault ) );

	if ( first.getFinalStates ( ).count ( first.getInitialState ( ) ) > 0 ) {
		ext::pair < StateType, unsigned > q01q02 ( label::InitialStateLabel::instance < StateType > ( ), 0 );

		res.addState ( q01q02 );

		res.setInitialState ( q01q02 );

		for ( const auto & t : first.getTransitionsFromState ( first.getInitialState ( ) ) )
			res.addTransition ( q01q02, t.first.second, ext::make_pair ( t.second, firstDefault ) );
		for ( const auto & t : second.getTransitionsFromState ( second.getInitialState ( ) ) )
			res.addTransition ( q01q02, t.first.second, ext::make_pair ( t.second, secondDefault ) );

		res.addFinalState ( q01q02 );
	}

	return res;
}

} /* namespace transform */

} /* namespace automaton */

