/*
 * StringConcatenate.h
 *
 *  Created on: 17. 10. 2014
 *	  Author: Jan Travnicek
 */

#pragma once

#include <string/LinearString.h>

namespace string {

namespace transform {

/**
 * Implements concatenation of two strings.
 *
 */
class StringConcatenate {
public:
	/**
	 * Implements concatenation of two strings.
	 *
	 * \tparam SymbolType the type of symbols in the string
	 *
	 * \param first the first string to concatenate
	 * \param second the second string to concatenate
	 *
	 * \return string @p first . @p second
	 */
	template < class SymbolType >
	static string::LinearString < SymbolType > concatenate ( const string::LinearString < SymbolType > & first, const string::LinearString < SymbolType > & second );

};

template < class SymbolType >
string::LinearString < SymbolType > StringConcatenate::concatenate ( const string::LinearString < SymbolType > & first, const string::LinearString < SymbolType > & second ) {
	ext::vector < SymbolType > content = first.getContent ( );
	content.insert ( content.end ( ), second.getContent ( ).begin ( ), second.getContent ( ).end ( ) );

	return string::LinearString < SymbolType > ( first.getAlphabet ( ) + second.getAlphabet ( ), content );
}

} /* namespace transform */

} /* namespace string */

