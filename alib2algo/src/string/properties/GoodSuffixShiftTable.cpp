/*
 * GoodSuffixShiftTable.cpp
 *
 *  Created on: 23. 3. 2017
 *      Author: Jan Travnicek
 */

#include "GoodSuffixShiftTable.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto GoodSuffixShiftTableLinearString = registration::AbstractRegister < string::properties::GoodSuffixShiftTable, ext::vector < size_t >, const string::LinearString < > & > ( string::properties::GoodSuffixShiftTable::gss );

} /* namespace */
