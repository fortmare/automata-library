/*
 * ReversedBadCharacterShiftTable.h
 *
 *  Created on: 5. 11. 2014
 *      Author: Jan Travnicek
 */

#pragma once

#include <alib/set>
#include <alib/map>

#include <string/LinearString.h>

namespace string {

namespace properties {

/**
 * Computation of BCS table for BMH from MI(E+\eps)-EVY course 2014
 * To get rid of zeros in BCS table we ignore last haystack character
 */
class ReversedBadCharacterShiftTable {
public:
	/**
	 * Search for pattern in linear string.
	 * @return set set of occurences
	 */
	template < class SymbolType >
	static ext::map < SymbolType, size_t > bcs ( const string::LinearString < SymbolType > & pattern );

};

template < class SymbolType >
ext::map < SymbolType, size_t > ReversedBadCharacterShiftTable::bcs ( const string::LinearString < SymbolType > & pattern ) {
	const ext::set < SymbolType > & alphabet = pattern.getAlphabet ( );
	ext::map < SymbolType, size_t > bcs;

	 /* Initialization of BCS to the length of the needle. */
	for ( const auto & symbol : alphabet )
		bcs.insert ( std::make_pair ( symbol, pattern.getContent ( ).size ( ) ) );

	 /* Filling out BCS, ignoring first character. */
	for ( size_t i = pattern.getContent ( ).size ( ) - 1; i > 0; i-- )
		bcs[pattern.getContent ( ).at ( i )] = i;

	return bcs;
}

} /* namespace properties */

} /* namespace string */

