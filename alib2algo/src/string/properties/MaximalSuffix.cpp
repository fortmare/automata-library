/*
 * MaximalSuffix.cpp
 *
 *  Created on: 27.3.2020
 *      Author: Jan Jirak
 */

#include "MaximalSuffix.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto MaxSuffixLinearString = registration::AbstractRegister < string::properties::MaximalSuffix, ext::pair < size_t, size_t >, const string::LinearString < > & > ( string::properties::MaximalSuffix::construct );

} /* namespace */
