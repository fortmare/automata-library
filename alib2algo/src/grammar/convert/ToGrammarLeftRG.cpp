/*
 * ToGrammarLeftRG.cpp
 *
 *  Created on: 8. 3. 2014
 *      Author: Tomas Pecka
 */

#include "ToGrammarLeftRG.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ToGrammarLeftRGRightRG = registration::AbstractRegister < grammar::convert::ToGrammarLeftRG, grammar::LeftRG < >, const grammar::RightRG < > & > ( grammar::convert::ToGrammarLeftRG::convert, "grammar" ).setDocumentation (
"Transforms a right regular grammar to left regular grammar.\n\
\n\
@param grammar the right regular grammar to convert\n\
@return left regular grammar which is equivalent to source right regular grammar." );

} /* namespace */
