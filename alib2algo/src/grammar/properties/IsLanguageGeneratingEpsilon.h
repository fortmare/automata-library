/*
 * IsLanguageGeneratingEpsilon.h
 *
 *  Created on: 22. 3. 2014
 *	  Author: Jan Travnicek
 */

#pragma once

#include <grammar/properties/NullableNonterminals.h>

namespace grammar {

namespace properties {

/**
 * Based on algorithm 2.4 from Melichar, chapter 2.2
 */
class IsLanguageGeneratingEpsilon {
public:
	/*
	 * Decides whether \e \in L( grammar )
	 *
	 * Severals steps implemented in method @see grammar::properties::NullableNonterminals::getNullableNonterminals();
	 *
	 * \tparam T the type of the tested grammar
	 *
	 * \param grammar the tested grammar
	 *
	 * \returns true if \e \in L(@p grammar)
	 */
	template<class T>
	static bool isLanguageGeneratingEpsilon( const T & grammar );
};

template<class T>
bool IsLanguageGeneratingEpsilon::isLanguageGeneratingEpsilon( const T & grammar ) {
	return grammar::properties::NullableNonterminals::getNullableNonterminals( grammar ).contains ( grammar.getInitialSymbol( ) );
}

} /* namespace properties */

} /* namespace grammar */

