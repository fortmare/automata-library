/*
 * Trim.h
 *
 *  Created on: 22. 3. 2014
 *	  Author: Tomas Pecka
 */

#pragma once

#include <grammar/ContextFree/CFG.h>
#include <grammar/ContextFree/EpsilonFreeCFG.h>
#include <grammar/ContextFree/GNF.h>
#include <grammar/ContextFree/CNF.h>
#include <grammar/ContextFree/LG.h>
#include <grammar/Regular/LeftLG.h>
#include <grammar/Regular/LeftRG.h>
#include <grammar/Regular/RightLG.h>
#include <grammar/Regular/RightRG.h>

#include "UnreachableSymbolsRemover.h"
#include "UnproductiveSymbolsRemover.h"

namespace grammar {

namespace simplify {

/**
 * Algorithm for the removal of dead symbols from a context free grammar.
 * Firstly, it calls the unproductive symbols removal algorithm, then unreachable symbols removal algorithm.
 *
 * @sa grammar::simplify::UnproduciveSymbolsRemover
 * @sa grammar::simplify::UnreachableSymbolsRemover
 */
class Trim {
public:
	/**
	 * Removes unproductive and unreachable symbols from the given grammar. Uses first the @sa UnproductiveSymbolsRemover and next @sa UnreachableSymbolsRemover in the process.
	 *
	 * \tparam T type of a finite automaton
	 *
	 * \param grammar the context free grammar to trim
	 *
	 * \return the trimmed grammar equivalent to @p grammar
	 */
	template<class T>
	static T trim( const T & grammar );
};

template<class T>
T Trim::trim( const T & grammar ) {
	return grammar::simplify::UnreachableSymbolsRemover::remove( grammar::simplify::UnproductiveSymbolsRemover::remove( grammar ) );
}

} /* namespace simplify */

} /* namespace grammar */

