/*
 * BeginToEndIndex.h
 *
 *  Created on: 5. 11. 2014
 *      Author: Jan Travnicek
 */

#pragma once

#include <alib/set>
#include <tree/properties/SubtreeJumpTable.h>

#include <tree/ranked/PrefixRankedBarTree.h>
#include <tree/ranked/PrefixRankedTree.h>

namespace arbology {

namespace transform {

/**
 */
class BeginToEndIndex {
public:
	/**
	 * Search for pattern in linear string.
	 * @return set set of occurences
	 */
	template < class SymbolType >
	static ext::set < unsigned > transform ( const tree::PrefixRankedBarTree < SymbolType > & subject, const ext::set < unsigned > & indexes );
	template < class SymbolType >
	static ext::set < unsigned > transform ( const tree::PrefixRankedTree < SymbolType > & subject, const ext::set < unsigned > & indexes );

};

template < class SymbolType >
ext::set < unsigned > BeginToEndIndex::transform ( const tree::PrefixRankedBarTree < SymbolType > & subject, const ext::set < unsigned > & indexes ) {
	ext::vector < int > subjectSubtreeJumpTable = tree::properties::SubtreeJumpTable::compute ( subject );
	ext::set < unsigned > res;

	for ( unsigned index : indexes )
		res.insert ( subjectSubtreeJumpTable[index] );

	return res;
}

template < class SymbolType >
ext::set < unsigned > BeginToEndIndex::transform ( const tree::PrefixRankedTree < SymbolType > & subject, const ext::set < unsigned > & indexes ) {
	ext::vector < int > subjectSubtreeJumpTable = tree::properties::SubtreeJumpTable::compute ( subject );
	ext::set < unsigned > res;

	for ( unsigned index : indexes )
		res.insert ( subjectSubtreeJumpTable[index] );

	return res;
}

} /* namespace transform */

} /* namespace arbology */

