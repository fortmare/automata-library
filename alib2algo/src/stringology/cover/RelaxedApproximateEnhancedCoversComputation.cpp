//

// Created by hruskraj on 1. 5. 2020.
//

#include "RelaxedApproximateEnhancedCoversComputation.h"
#include <registration/AlgoRegistration.hpp>

namespace stringology::cover {
auto RelaxedApproximateEnhancedCoversString = registration::AbstractRegister < RelaxedApproximateEnhancedCoversComputation, ext::set < string::LinearString < DefaultSymbolType > >, const string::LinearString < > &, unsigned > ( RelaxedApproximateEnhancedCoversComputation::compute );
} /* namespace stringology::cover */
