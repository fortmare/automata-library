/*
 * SuffixAutomatonFactors.cpp
 *
 *  Created on: 10. 1. 2017
 *      Author: Jan Travnicek
 */

#include "SuffixAutomatonFactors.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto SuffixAutomatonFactorsLinearString = registration::AbstractRegister < stringology::query::SuffixAutomatonFactors, ext::set < unsigned >, const indexes::stringology::SuffixAutomaton < > &, const string::LinearString < > & > ( stringology::query::SuffixAutomatonFactors::query );

} /* namespace */
