//
// Created by shushiri on 18.2.19.
//

#include "NondeterministicApproximateSuffixAutomatonForHammingDistance.h"
#include <registration/AlgoRegistration.hpp>

namespace stringology::indexing {

auto NDApproximateSuffixAutomaton = registration::AbstractRegister < NondeterministicApproximateSuffixAutomatonForHammingDistance, automaton::NFA < DefaultSymbolType, ext::pair < unsigned, unsigned > >, const string::LinearString < > &, unsigned > ( NondeterministicApproximateSuffixAutomatonForHammingDistance::construct );

}  /* namespace stringology::indexing */
