/*
 * Author: Radovan Cerveny
 */

#include "ExactSuffixAutomaton.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto SuffixAutomatonLinearString = registration::AbstractRegister < stringology::indexing::ExactSuffixAutomaton, indexes::stringology::SuffixAutomaton < >, const string::LinearString < > & > ( stringology::indexing::ExactSuffixAutomaton::construct );

} /* namespace */
