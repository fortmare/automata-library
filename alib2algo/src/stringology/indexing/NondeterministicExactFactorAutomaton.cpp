/*
 * NondeterministicExactFactorAutomaton.cpp
 *
 *  Created on: 7. 4. 2015
 *      Author: Jan Travnicek
 */

#include "NondeterministicExactFactorAutomaton.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ExactFactorAutomatonLinearString = registration::AbstractRegister < stringology::indexing::NondeterministicExactFactorAutomaton, automaton::EpsilonNFA < DefaultSymbolType, unsigned >, const string::LinearString < > & > ( stringology::indexing::NondeterministicExactFactorAutomaton::construct );

} /* namespace */
