/*
 *  BitParalelism.h
 *
 *  Created on: 29.4.2018
 *      Author: Tomas Capek
 */

#pragma once

#include <string/LinearString.h>


namespace stringology {

namespace simulations {

class BitParalelism {
public:
  template <class SymbolType>
  static ext::map<SymbolType, ext::vector<bool> > constructDVectors(const ext::set<SymbolType> & alphabet, const string::LinearString<SymbolType> & pattern);
};

template <class SymbolType>
ext::map<SymbolType, ext::vector<bool> > BitParalelism::constructDVectors(const ext::set<SymbolType> & alphabet, const string::LinearString<SymbolType> & pattern) {
  ext::map<SymbolType, ext::vector<bool> > D_vectors;

  for(const SymbolType & symbol : alphabet) {
    D_vectors[symbol] = ext::vector<bool>(pattern.getContent().size());

    for(unsigned int i = 0; i<pattern.getContent().size(); i++) {
      if(pattern.getContent()[i] != symbol) {
        D_vectors[symbol][i] = 1;
      } else {
        D_vectors[symbol][i] = 0;
      }
    }
  }

  return D_vectors;
}

}

}

