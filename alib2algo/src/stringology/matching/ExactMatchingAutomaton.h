/*
 * ExactMatchingAutomaton.h
 *
 *  Created on: 9. 2. 2014
 *      Author: Jan Travnicek
 */

#pragma once

#include <automaton/FSM/NFA.h>
#include <string/LinearString.h>

namespace stringology {

namespace matching {

class ExactMatchingAutomaton {
public:
	/**
	 * Performs conversion.
	 * @return left regular grammar equivalent to source automaton.
	 */
	template < class SymbolType >
	static automaton::NFA < SymbolType, unsigned > construct(const string::LinearString < SymbolType > & pattern);
};

template < class SymbolType >
automaton::NFA < SymbolType, unsigned > ExactMatchingAutomaton::construct(const string::LinearString < SymbolType > & pattern) {
	automaton::NFA < SymbolType, unsigned > res( 0 );
	res.setInputAlphabet(pattern.getAlphabet());
	for(const SymbolType& symbol : pattern.getAlphabet()) {
		res.addTransition( 0, symbol, 0);
	}
	unsigned i = 1;
	for(const SymbolType& symbol : pattern.getContent()) {
		res.addState( i );
		res.addTransition( i - 1, symbol, i );
		i++;
	}
	res.addFinalState( i - 1 );
	return res;
}

} /* namespace matching */

} /* namespace stringology */

