/*
 * BNDMMatcherConstruction.h
 *
 *  Created on: 6. 2. 2017
 *      Author: Jan Travnicek
 */

#pragma once

#include <indexes/stringology/BitSetIndex.h>
#include <string/LinearString.h>
#include <exception/CommonException.h>

namespace stringology {

namespace matching {

/**
 * Constructs a bit parallel index for given string.
 *
 */

class BNDMMatcherConstruction {
public:
	/**
	 * Creates suffix trie
	 * @param string string to construct suffix trie for
	 * @return automaton
	 */
	template < class SymbolType, size_t BitmaskBitCount = 64 >
	static indexes::stringology::BitSetIndex < SymbolType, BitmaskBitCount > construct ( const string::LinearString < SymbolType > & w );

};

template < class SymbolType, size_t BitmaskBitCount >
indexes::stringology::BitSetIndex < SymbolType, BitmaskBitCount > BNDMMatcherConstruction::construct ( const string::LinearString < SymbolType > & w ) {
	size_t bitmaskLength = std::min ( w.getContent ( ).size ( ), BitmaskBitCount );

	ext::map < SymbolType, ext::bitset < BitmaskBitCount > > res;
	for ( const SymbolType & symbol : w.getAlphabet ( ) )
		res [ symbol ] = ext::bitset < BitmaskBitCount > ( 0 );

	for ( unsigned i = 0; i < bitmaskLength; ++i )
		res [ w.getContent ( ) [ i ] ] [ bitmaskLength - i - 1 ] = true;

	return indexes::stringology::BitSetIndex < SymbolType, BitmaskBitCount > ( res, string::LinearString < SymbolType > ( w ) );
}

} /* namespace matching */

} /* namespace stringology */

