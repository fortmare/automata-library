/*
 * LZ77Decompression.cpp
 *
 *  Created on: 1. 3. 2017
 *      Author: Jan Parma
 */

#include "LZ77Decompression.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto LZ77Decompression = registration::AbstractRegister < stringology::compression::LZ77Decompression, string::LinearString < >, const std::vector < std::tuple < unsigned, unsigned, DefaultSymbolType > > & > ( stringology::compression::LZ77Decompression::decompress );

} /* namespace */
