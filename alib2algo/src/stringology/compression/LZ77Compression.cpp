/*
 * LZ77.cpp
 *
 *  Created on: 1. 3. 2017
 *      Author: Jan Parma
 */

#include "LZ77Compression.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto LZ77CompressionLinearString = registration::AbstractRegister < stringology::compression::LZ77Compression, std::vector < std::tuple < unsigned, unsigned, DefaultSymbolType > >, const string::LinearString < > &, unsigned, unsigned > ( stringology::compression::LZ77Compression::compress );

} /* namespace */
