/*
 * QuickSearch.cpp
 *
 *  Created on: 23. 2. 2018
 *	  Author: Michal Cvach
 */

#include "QuickSearch.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto QuickSearchLinearString = registration::AbstractRegister < stringology::exact::QuickSearch, ext::set < unsigned >, const string::LinearString < > &, const string::LinearString < > & > ( stringology::exact::QuickSearch::match );

} /* namespace */
