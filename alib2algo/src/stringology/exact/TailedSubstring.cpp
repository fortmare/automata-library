/*
 * TailedSubstring.cpp
 *
 *  Created on: 19. 3. 2020
 *      Author: Jan Jirak
 */

#include "TailedSubstring.h"
#include <registration/AlgoRegistration.hpp>

namespace {

    auto TailedSubstring = registration::AbstractRegister < stringology::exact::TailedSubstring, ext::set < unsigned >, const string::LinearString < > &, const string::LinearString < > & > ( stringology::exact::TailedSubstring::match );

} /* namespace */
