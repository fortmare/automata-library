#include <catch2/catch.hpp>

#include "automaton/transform/AutomatonIterationEpsilonTransition.h"

TEST_CASE ( "Automata Iteration Epsilon Transition", "[unit][algo][automaton][transform]" ) {
	SECTION ( "AAG slides lecture 3 (fixed example from Melichar 2.83)" ) {
		DefaultStateType q0('0'), q1('1'), q2('2');
		DefaultSymbolType a('a'), b('b');

		automaton::DFA < > m1(q0);
		m1.setStates({q0, q1, q2});
		m1.setFinalStates({q2});
		m1.setInputAlphabet({a, b});
		m1.addTransition(q0, a, q1);
		m1.addTransition(q1, b, q1);
		m1.addTransition(q1, a, q2);

		DefaultStateType S = common::createUnique(q0, m1.getStates());

		automaton::EpsilonNFA< > expected(S);
		expected.setStates({S, q0, q1, q2});
		expected.setInputAlphabet({a, b});
		expected.setFinalStates({S, q2});
		expected.addTransition(q0, a, q1);
		expected.addTransition(q1, b, q1);
		expected.addTransition(q1, a, q2);
		expected.addTransition(S, q0);
		expected.addTransition(q2, q0);

		REQUIRE(automaton::transform::AutomatonIterationEpsilonTransition::iteration(m1) == expected);
	}
}

