#include <catch2/catch.hpp>

#include <string/LinearString.h>
#include <stringology/simulations/ExactBitParalelism.h>

TEST_CASE ( "Exact Bit Parallelism", "[unit][algo][stringology][simulations]" ) {
	SECTION ( "Simple" ) {
		auto text = string::LinearString<>("adcabcaabadbbca");
		auto pattern = string::LinearString<>("adbbca");

		ext::set<unsigned int> expected_result = {15};

		CHECK(expected_result == stringology::simulations::ExactBitParalelism::search(text, pattern));
	}
}
