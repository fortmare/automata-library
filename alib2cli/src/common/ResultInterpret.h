#pragma once

#include <abstraction/ValueHolderInterface.hpp>
#include <exception/CommonException.h>
#include <memory>

namespace cli {

class ResultInterpret {
public:
	static int cli ( std::shared_ptr < abstraction::Value > result ) {
		if ( result ) {
			std::shared_ptr < abstraction::ValueHolderInterface < int > > ptr1 = std::dynamic_pointer_cast < abstraction::ValueHolderInterface < int > > ( result );
			if ( ptr1 )
				return ptr1->getValue ( );
			std::shared_ptr < abstraction::ValueHolderInterface < unsigned > > ptr2 = std::dynamic_pointer_cast < abstraction::ValueHolderInterface < unsigned > > ( result );
			if ( ptr2 )
				return ptr2->getValue ( );
			std::shared_ptr < abstraction::ValueHolderInterface < bool > > ptr3 = std::dynamic_pointer_cast < abstraction::ValueHolderInterface < bool > > ( result );
			if ( ptr3 )
				return static_cast < int > ( ! ptr3->getValue ( ) );

			throw exception::CommonException ( "Invalid result type. Provided: " + result->getType ( ) );
		} else {
			return 0;
		}
	}
};

} /* namespace cli */

