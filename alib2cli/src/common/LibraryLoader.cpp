#include "LibraryLoader.h"

namespace cli {

std::list < LibraryLoader::Library >::iterator LibraryLoader::find ( const std::string & name ) {
	for ( std::list < LibraryLoader::Library >::iterator iter = libraries.begin ( ); iter != libraries.end ( ); ++ iter ) {
		if ( iter->path ( ) == name )
			return iter;
	}

	return libraries.end ( );
}

std::list < LibraryLoader::Library > LibraryLoader::libraries;

void LibraryLoader::load ( std::string name ) {
	std::list < LibraryLoader::Library >::iterator iter = find ( name );
	if ( iter == libraries.end ( ) )
		libraries.emplace ( libraries.end ( ), std::move ( name ) );
}

void LibraryLoader::unload ( const std::string & name ) {
	std::list < LibraryLoader::Library >::iterator iter = find ( name );
	if ( iter != libraries.end ( ) )
		libraries.erase ( iter );
}

} /* namespace cli */
