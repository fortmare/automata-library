/*
 * LineInterface.h
 *
 *  Created on: 20. 3. 2017
 *	  Author: Jan Travnicek
 */

#pragma once

#include <string>

namespace cli {

/**
 * Line reader serves as a base class for adaptors reding from various source.
 */
class LineInterface {
public:
	virtual bool readline ( std::string & line, bool first ) = 0;

	virtual void lineCallback ( const std::string & ) const {
	}

public:
	LineInterface ( ) = default;

	LineInterface ( LineInterface && ) noexcept = default;

	LineInterface ( const LineInterface & ) = delete;

	LineInterface & operator = ( LineInterface && ) noexcept = delete;

	LineInterface & operator = ( const LineInterface & ) = delete;

	virtual ~LineInterface ( ) noexcept = default;
};

} // namespace cli

