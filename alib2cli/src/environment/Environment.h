#pragma once

#include <alib/string>
#include <alib/map>
#include <alib/memory>
#include <alib/typeinfo>
#include <alib/optional>

#include <abstraction/Value.hpp>
#include <exception/CommonException.h>

#include <abstraction/OperationAbstraction.hpp>
#include <abstraction/ValueHolder.hpp>
#include <ast/command/CommandResult.h>
#include <lexer/CharSequence.h>

#include <abstraction/TemporariesHolder.h>

namespace cli {

class Command;

class Environment : public abstraction::TemporariesHolder {
	ext::map < std::string, std::string > m_bindings;
	ext::map < std::string, std::shared_ptr < abstraction::Value > > m_variables;
	std::shared_ptr < abstraction::Value > m_result;

	ext::optional_ref < const Environment > m_upper;

	std::shared_ptr < abstraction::Value > getVariableInt ( const std::string & name ) const;

	void setVariableInt ( std::string name, std::shared_ptr < abstraction::Value > value ) {
		m_variables [ std::move ( name ) ] = std::move ( value );
	}

public:
	Environment ( ) = default;

	Environment ( ext::optional_ref < const Environment > upper ) : m_upper ( upper ) {
	}

	std::string getBinding ( const std::string & name ) const;

	std::set < std::string > getBindingNames ( ) const {
		std::set < std::string > res;

		for ( const std::pair < const std::string, std::string > & kv : m_bindings ) {
			res.insert ( kv.first );
		}

		return res;
	}

	void setBinding ( std::string name, std::string value ) {
		m_bindings [ std::move ( name ) ] = std::move ( value );
	}

	bool clearBinding ( const std::string & name ) {
		return m_bindings.erase ( name ) != 0;
	}

	std::shared_ptr < abstraction::Value > getVariable ( const std::string & name ) const {
		return getVariableInt ( name );
	}

	template < class T >
	const T & getVariable ( const std::string & name ) const {
		return abstraction::retrieveValue < const T & > ( getVariableInt ( name ) );
	}

	std::set < std::string > getVariableNames ( ) const {
		std::set < std::string > res;

		for ( const std::pair < const std::string, std::shared_ptr < abstraction::Value > > & kv : m_variables ) {
			res.insert ( kv.first );
		}

		return res;
	}

	void setVariable ( std::string name, std::shared_ptr < abstraction::Value > value ) {
		setVariableInt ( std::move ( name ), std::move ( value ) );
	}

	template < class T >
	void setVariable ( std::string name, T value ) {
		auto variable = std::make_shared < abstraction::ValueHolder < T > > ( std::move ( value ), false );
		setVariableInt ( std::move ( name ), variable );
	}

	bool clearVariable ( const std::string & name ) {
		return m_variables.erase ( name ) != 0u;
	}

	void setResult ( std::shared_ptr < abstraction::Value > value ) {
		m_result = std::move ( value );
	}

	std::shared_ptr < abstraction::Value > getResult ( ) const {
		return m_result;
	}

	cli::CommandResult execute ( std::shared_ptr < cli::LineInterface > lineInterface );

	cli::CommandResult execute_line ( cli::CharSequence charSequence );

	Environment & getGlobalScope ( );
};

} /* namespace cli */

