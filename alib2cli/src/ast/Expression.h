#pragma once

#include <ast/Ast.h>
#include <abstraction/OperationAbstraction.hpp>
#include <environment/Environment.h>

namespace cli {

class Expression {
public:
	enum class Operation {
		LOGICAL_OR,
		LOGICAL_AND,
		COMPARE_EQUALITY,
		COMPARE_INEQUALITY,
		COMPARE_LESS_OR_EQUAL,
		COMPARE_LESS,
		COMPARE_MORE_OR_EQUAL,
		COMPARE_MORE,
		ADD,
		SUB,
		MUL,
		MOD,
		DIV,
		NOT
	};

	virtual ~Expression ( ) noexcept = default;

	virtual std::shared_ptr < abstraction::Value > translateAndEval ( Environment & environment ) const = 0;
};

} /* namespace cli */

