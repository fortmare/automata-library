#pragma once

#include <ast/Command.h>
#include <environment/Environment.h>
#include <ast/Expression.h>

namespace cli {

class QuitCommand : public Command {
	std::unique_ptr < Expression > m_expr;

public:
	QuitCommand ( std::unique_ptr < Expression > expr ) : m_expr ( std::move ( expr ) ) {
	}

	CommandResult run ( Environment & environment ) const override {
		if ( m_expr )
			environment.setResult ( m_expr->translateAndEval ( environment ) );

		return CommandResult::QUIT;
	}
};

} /* namespace cli */

