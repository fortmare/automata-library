#pragma once

#include <ast/Command.h>
#include <environment/Environment.h>

namespace cli {

class BindingsIntrospectionCommand : public Command {
	std::unique_ptr < cli::Arg > m_param;

public:
	BindingsIntrospectionCommand ( std::unique_ptr < cli::Arg > param ) : m_param ( std::move ( param ) ) {
	}

	CommandResult run ( Environment & environment ) const override {
		std::string param;
		if ( m_param != nullptr )
			param = m_param->eval ( environment );

		if ( param.empty ( ) )
			for ( const std::string & name : environment.getBindingNames ( ) )
				common::Streams::out << name << std::endl;
		else
			common::Streams::out << param << " " << environment.getBinding ( param ) << std::endl;

		return CommandResult::OK;
	}
};

} /* namespace cli */

