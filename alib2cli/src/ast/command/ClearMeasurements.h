#pragma once

#include <ast/Command.h>
#include <environment/Environment.h>

#include <alib/measure>

namespace cli {

class ClearMeasurements : public Command {
public:
	CommandResult run ( Environment & ) const override {
		measurements::reset ( );
		return CommandResult::OK;
	}
};

} /* namespace cli */

