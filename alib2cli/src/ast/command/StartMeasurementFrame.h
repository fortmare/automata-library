#pragma once

#include <ast/Command.h>
#include <environment/Environment.h>

#include <alib/measure>

namespace cli {

class StartMeasurementFrame : public Command {
	std::string m_frameName;
	measurements::Type m_type;

public:
	StartMeasurementFrame ( std::string frameName, measurements::Type type ) : m_frameName ( std::move ( frameName ) ), m_type ( type ) {
	}

	CommandResult run ( Environment & ) const override {
		measurements::start ( measurements::stealthStringFromString ( m_frameName ), m_type );
		return CommandResult::OK;
	}
};

} /* namespace cli */

