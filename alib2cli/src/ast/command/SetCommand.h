#pragma once

#include <ast/Command.h>
#include <environment/Environment.h>
#include <global/GlobalData.h>
#include <alib/random>

namespace cli {

class SetCommand : public Command {
	std::string m_param;
	std::string m_value;

public:
	SetCommand ( std::string param, std::string value ) : m_param ( std::move ( param ) ), m_value ( std::move ( value ) ) {
	}

	CommandResult run ( Environment & ) const override {
		if ( m_param == "verbose" ) {
			common::GlobalData::verbose = ext::from_string < bool > ( m_value );
		} else if ( m_param == "measure" ) {
			common::GlobalData::measure = ext::from_string < bool > ( m_value );
		} else if ( m_param == "optimizeXml" ) {
			common::GlobalData::optimizeXml = ext::from_string < bool > ( m_value );
		} else if ( m_param == "seed" ) {
			ext::random_devices::semirandom.seed ( ext::from_string < unsigned > ( m_value ) );
		} else {
			common::Streams::out << "The set parameter " << m_param << " does not exist." << std::endl;
		}
		return CommandResult::OK;
	}
};

} /* namespace cli */

