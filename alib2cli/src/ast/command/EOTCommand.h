#pragma once

#include <ast/Command.h>
#include <environment/Environment.h>

namespace cli {

class EOTCommand : public Command {
public:
	CommandResult run ( Environment & /* environment */ ) const override {
		return CommandResult::EOT;
	}
};

} /* namespace cli */

