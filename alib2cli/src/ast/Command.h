#pragma once

#include <ast/command/CommandResult.h>

namespace cli {

class Environment;

class Command {
public:
	virtual ~Command ( ) noexcept = default;

	virtual CommandResult run ( Environment & environment ) const = 0;
};

} /* namespace cli */

