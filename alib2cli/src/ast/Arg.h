#pragma once

#include <ast/Ast.h>

namespace cli {

class Arg {
public:
	virtual ~Arg ( ) noexcept = default;

	virtual std::string eval ( Environment & environment ) const = 0;
};

} /* namespace cli */

