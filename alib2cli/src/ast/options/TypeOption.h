#pragma once

#include <ast/Option.h>
#include <exception/CommonException.h>

namespace cli {

class TypeOption final : public Option {
	std::string m_type;

public:
	TypeOption ( std::string type ) : m_type ( std::move ( type ) ) {
	}

	const std::string & getType ( ) const {
		return m_type;
	}
};

} /* namespace cli */

