/*
 * forward_list.hpp
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Created on: Apr 1, 2013
 * Author: Jan Travnicek
 */

#pragma once

#include <forward_list>
#include <ostream>
#include <sstream>
#include <string>

#include <extensions/range.hpp>

namespace ext {

/**
 * \brief
 * Class extending the forward_list class from the standard library. Original reason is to allow printing of the container with overloaded operator <<.
 *
 * The class mimics the behavior of the forward_list from the standatd library.
 *
 * \tparam T the type of values inside the forward_list
 * \tparam Alloc the allocator of values of type T
 */
template < class T, class Alloc = std::allocator < T > >
class forward_list : public std::forward_list < T, Alloc >, AllocFix < Alloc > {
public:
	/**
	 * Inherit constructors of the standard forward_list
	 */
	using std::forward_list < T, Alloc >::forward_list; // NOLINT(modernize-use-equals-default)

	/**
	 * Inherit operator = of the standard forward_list
	 */
	using std::forward_list < T, Alloc >::operator =;
#ifndef __clang__

	/**
	 * Default constructor needed by g++ since it is not inherited
	 */
	forward_list ( ) = default;

	/**
	 * Copy constructor needed by g++ since it is not inherited
	 */
	forward_list ( const forward_list & other ) = default;

	/**
	 * Move constructor needed by g++ since it is not inherited
	 */
	forward_list ( forward_list && other ) = default;

	/**
	 * Copy operator = needed by g++ since it is not inherited
	 */
	forward_list & operator = ( forward_list && other ) = default;

	/**
	 * Move operator = needed by g++ since it is not inherited
	 */
	forward_list & operator = ( const forward_list & other ) = default;
#endif

	/**
	 * Constructor from range of values.
	 *
	 * \tparam Iterator the type of range iterator
	 *
	 * \param range the source range
	 */
	template < class Iterator >
	forward_list ( const ext::iterator_range < Iterator > & range ) : forward_list ( range.begin ( ), range.end ( ) ) {
	}

	/**
	 * \brief
	 * Inherited behavior of begin for non-const instance.
	 *
	 * \return iterator the first element of forward_list
	 */
	auto begin ( ) & {
		return std::forward_list < T, Alloc >::begin ( );
	}

	/**
	 * \brief
	 * Inherited behavior of begin for const instance.
	 *
	 * \return const_iterator the first element of forward_list
	 */
	auto begin ( ) const & {
		return std::forward_list < T, Alloc >::begin ( );
	}

	/**
	 * \brief
	 * Inherited behavior of begin for rvalues.
	 *
	 * \return move_iterator the first element of forward_list
	 */
	auto begin ( ) && {
		return make_move_iterator ( this->begin ( ) );
	}

	/**
	 * \brief
	 * Inherited behavior of end for non-const instance.
	 *
	 * \return iterator to one after the last element of forward_list
	 */
	auto end ( ) & {
		return std::forward_list < T, Alloc >::end ( );
	}

	/**
	 * \brief
	 * Inherited behavior of end for const instance.
	 *
	 * \return const_iterator to one after the last element of forward_list
	 */
	auto end ( ) const & {
		return std::forward_list < T, Alloc >::end ( );
	}

	/**
	 * \brief
	 * Inherited behavior of end for rvalues.
	 *
	 * \return move_iterator to one after the last element of forward_list
	 */
	auto end ( ) && {
		return make_move_iterator ( this->end ( ) );
	}

	/**
	 * \brief
	 * Make range of non-const begin to end iterators.
	 *
	 * \return full range over container values
	 */
	auto range ( ) & {
		auto endIter = end ( );
		auto beginIter = begin ( );
		return ext::iterator_range < decltype ( endIter ) > ( beginIter, endIter );
	}

	/**
	 * \brief
	 * Make range of non-const begin to end iterators.
	 *
	 * \return full range over container values
	 */
	auto range ( ) const & {
		auto endIter = end ( );
		auto beginIter = begin ( );
		return ext::iterator_range < decltype ( endIter ) > ( beginIter, endIter );
	}

	/**
	 * \brief
	 * Make range of move begin to end iterators.
	 *
	 * \return full range over container values
	 */
	auto range ( ) && {
		auto endIter = std::move ( * this ).end ( );
		auto beginIter = std::move ( * this ).begin ( );
		return ext::iterator_range < decltype ( endIter ) > ( beginIter, endIter );
	}
};

/**
 * \brief
 * Operator to print the forward_list to the output stream.
 *
 * \param out the output stream
 * \param forward_list the forward_list to print
 *
 * \tparam T the type of values inside the forward_list
 * \tparam Ts ... remaining unimportant template parameters of the forward_list
 *
 * \return the output stream from the \p out
 */
template< class T, class ... Ts >
std::ostream& operator<<(std::ostream& out, const ext::forward_list<T, Ts ... >& forward_list) {
	out << "[";

	bool first = true;
	for(const T& item : forward_list) {
		if(!first) out << ", ";
		first = false;
		out << item;
	}

	out << "]";
	return out;
}

/**
 * \brief
 * Overload of to_string function.
 *
 * \param value the forward_list to be converted to string
 *
 * \tparam T the type of values inside the forward_list
 * \tparam Ts ... remaining unimportant template parameters of the forward_list
 *
 * \return string representation
 */
template < class T, class ... Ts >
std::string to_string ( const ext::forward_list < T, Ts ... > & value ) {
	std::stringstream ss;
	ss << value;
	return ss.str();
}

} /* namespace ext */


