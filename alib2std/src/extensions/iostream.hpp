/*
 * iostream.hpp
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Created on: Feb 12, 2016
 * Author: Radovan Cerveny
 */

#pragma once

#include <iostream>
#include "fdstream.hpp"

namespace ext {

/**
 * \brief
 * Instance of the cmeasure stream. Default behavior is output to cout, but when the descriptor number 5 is redirected the cmeasure destination is to that descriptor.
 */
extern ofdstream cmeasure;

} /* namespace ext */

