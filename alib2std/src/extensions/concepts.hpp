/*
 * concepts.hpp
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Created on: Feb 28, 2014
 * Author: Jan Travnicek
 */

#pragma once

#include <concepts>
#include <alib/type_traits>

namespace ext {

	/**
	 * \brief
	 * Concept to test whether type T is in a types pack. The trait provides field value set to true if the type T is in pack of types Ts ..., false othervise.
	 *
	 * \tparam T the type to look for
	 * \tparam Ts ... the types pack to look in
	 */
	template < typename T, typename ... Ts >
	concept Included = ext::is_in < T, Ts ... >::value;

} /* namespace ext */

