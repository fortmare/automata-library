#include <catch2/catch.hpp>
#include <alib/tuple>
#include <sstream>
#include <alib/optional>
#include <compare>
#include <alib/vector>

TEST_CASE ( "Tuple", "[unit][std][container]" ) {
	SECTION ( "Test call on nth" ) {
		ext::tuple < int, int, int, int, int > t = ext::make_tuple ( 1, 2, 3, 4, 5 );

		std::stringstream ss;
		ss << t;
		CAPTURE ( ss.str ( ) );
		CHECK ( ss.str ( ) == "(1, 2, 3, 4, 5)" );
	}

	SECTION ( "Three way with optional" ) {
		ext::optional < int > a ( 1 );
		ext::optional < int > b ( 2 );

		CHECK ( ( std::tie ( std::as_const ( a ) ) <=> std::tie ( std::as_const ( b ) ) < 0 ) );

		CHECK ( a != b );

		ext::vector < int > av { 1, 2, 3 };
		ext::vector < int > bv { 2, 3, 4 };

		CHECK ( ( std::tie ( std::as_const ( av ) ) <=> std::tie ( std::as_const ( bv ) ) < 0 ) );
	}
}

