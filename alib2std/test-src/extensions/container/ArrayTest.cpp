#include <catch2/catch.hpp>
#include <alib/map>
#include <alib/algorithm>
#include <alib/array>

TEST_CASE ( "Array", "[unit][std][container]" ) {
	SECTION ( "Basic" ) {
		ext::array < int, 3 > a = ext::make_array ( 1, 2, 3 );
		CHECK ( a [ 0 ] == 1 );

		ext::array < int, 3 > b = a;
		CHECK ( b [ 1 ] == a [ 1 ] );
	}
}

