// stacktrace.h (c) 2008, Timo Bingmann from http://idlebox.net/
// published under the WTFPL v2.0

#pragma once

#include <alib/string>

namespace ext {

/**
 * \brief
 * Analyzes the backtrace (stack trace) and creates its sring representation, one function call per line.
 */
void simpleStacktrace ( std::ostream & out, unsigned int max_frames = 1000);

} /* namespace ext */

