/*
 * Follow.cpp
 *
 *  Created on: 9. 6. 2015
 *	  Author: Tomas Pecka
 */

#include "Follow.h"

#include <grammar/ContextFree/CFG.h>
#include <grammar/ContextFree/EpsilonFreeCFG.h>
#include <grammar/ContextFree/CNF.h>
#include <grammar/ContextFree/GNF.h>
#include <grammar/ContextFree/LG.h>
#include <grammar/Regular/LeftLG.h>
#include <grammar/Regular/LeftRG.h>
#include <grammar/Regular/RightLG.h>
#include <grammar/Regular/RightRG.h>
#include <registration/AlgoRegistration.hpp>

namespace {

auto FollowCFG = registration::AbstractRegister < grammar::parsing::Follow, ext::map < DefaultSymbolType, ext::set < ext::vector < DefaultSymbolType > > >, const grammar::CFG < > & > ( grammar::parsing::Follow::follow );
auto FollowEpsilonFreeCFG = registration::AbstractRegister < grammar::parsing::Follow, ext::map < DefaultSymbolType, ext::set < ext::vector < DefaultSymbolType > > >, const grammar::EpsilonFreeCFG < > & > ( grammar::parsing::Follow::follow );
auto FollowGNF = registration::AbstractRegister < grammar::parsing::Follow, ext::map < DefaultSymbolType, ext::set < ext::vector < DefaultSymbolType > > >, const grammar::GNF < > & > ( grammar::parsing::Follow::follow );
auto FollowCNF = registration::AbstractRegister < grammar::parsing::Follow, ext::map < DefaultSymbolType, ext::set < ext::vector < DefaultSymbolType > > >, const grammar::CNF < > & > ( grammar::parsing::Follow::follow );
auto FollowLG = registration::AbstractRegister < grammar::parsing::Follow, ext::map < DefaultSymbolType, ext::set < ext::vector < DefaultSymbolType > > >, const grammar::LG < > & > ( grammar::parsing::Follow::follow );
auto FollowLeftLG = registration::AbstractRegister < grammar::parsing::Follow, ext::map < DefaultSymbolType, ext::set < ext::vector < DefaultSymbolType > > >, const grammar::LeftLG < > & > ( grammar::parsing::Follow::follow );
auto FollowLeftRG = registration::AbstractRegister < grammar::parsing::Follow, ext::map < DefaultSymbolType, ext::set < ext::vector < DefaultSymbolType > > >, const grammar::LeftRG < > & > ( grammar::parsing::Follow::follow );
auto FollowRightLG = registration::AbstractRegister < grammar::parsing::Follow, ext::map < DefaultSymbolType, ext::set < ext::vector < DefaultSymbolType > > >, const grammar::RightLG < > & > ( grammar::parsing::Follow::follow );
auto FollowRightRG = registration::AbstractRegister < grammar::parsing::Follow, ext::map < DefaultSymbolType, ext::set < ext::vector < DefaultSymbolType > > >, const grammar::RightRG < > & > ( grammar::parsing::Follow::follow );

auto FollowCFG2 = registration::AbstractRegister < grammar::parsing::Follow, ext::set < ext::vector < DefaultSymbolType > >, const grammar::CFG < > &, const DefaultSymbolType & > ( grammar::parsing::Follow::follow );
auto FollowEpsilonFreeCFG2 = registration::AbstractRegister < grammar::parsing::Follow, ext::set < ext::vector < DefaultSymbolType > >, const grammar::EpsilonFreeCFG < > &, const DefaultSymbolType & > ( grammar::parsing::Follow::follow );
auto FollowGNF2 = registration::AbstractRegister < grammar::parsing::Follow, ext::set < ext::vector < DefaultSymbolType > >, const grammar::GNF < > &, const DefaultSymbolType & > ( grammar::parsing::Follow::follow );
auto FollowCNF2 = registration::AbstractRegister < grammar::parsing::Follow, ext::set < ext::vector < DefaultSymbolType > >, const grammar::CNF < > &, const DefaultSymbolType & > ( grammar::parsing::Follow::follow );
auto FollowLG2 = registration::AbstractRegister < grammar::parsing::Follow, ext::set < ext::vector < DefaultSymbolType > >, const grammar::LG < > &, const DefaultSymbolType & > ( grammar::parsing::Follow::follow );
auto FollowLeftLG2 = registration::AbstractRegister < grammar::parsing::Follow, ext::set < ext::vector < DefaultSymbolType > >, const grammar::LeftLG < > &, const DefaultSymbolType & > ( grammar::parsing::Follow::follow );
auto FollowLeftRG2 = registration::AbstractRegister < grammar::parsing::Follow, ext::set < ext::vector < DefaultSymbolType > >, const grammar::LeftRG < > &, const DefaultSymbolType & > ( grammar::parsing::Follow::follow );
auto FollowRightLG2 = registration::AbstractRegister < grammar::parsing::Follow, ext::set < ext::vector < DefaultSymbolType > >, const grammar::RightLG < > &, const DefaultSymbolType & > ( grammar::parsing::Follow::follow );
auto FollowRightRG2 = registration::AbstractRegister < grammar::parsing::Follow, ext::set < ext::vector < DefaultSymbolType > >, const grammar::RightRG < > &, const DefaultSymbolType & > ( grammar::parsing::Follow::follow );

} /* namespace */
