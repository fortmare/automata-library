/*
 * DeterministicLL1Grammar.h
 *
 *  Created on: 9. 6. 2015
 *	  Author: Jan Travnicek
 */

#pragma once

#include <grammar/ContextFree/CFG.h>

namespace grammar {

namespace parsing {

class DeterministicLL1Grammar {
public:
	static grammar::CFG < > convert ( const grammar::CFG < > & param );

};

} /* namespace parsing */

} /* namespace grammar */

