/*
 * LR0Parser.h
 *
 *  Created on: 2. 5. 2016
 *	  Author: Martin Kocicka
 */

#pragma once

#include <automaton/FSM/DFA.h>
#include <grammar/ContextFree/CFG.h>
#include <grammar/parsing/LRParserTypes.h>

namespace grammar {

namespace parsing {

class LR0Parser {
public:
	static LR0Items getClosure ( LR0Items items, const grammar::CFG < > & originalGrammar );

	static LR0Items getNextStateItems ( const LR0Items & items, const ext::variant < DefaultSymbolType, DefaultSymbolType > & symbol, const grammar::CFG < > & originalGrammar );

	static automaton::DFA < ext::variant < DefaultSymbolType, DefaultSymbolType >, LR0Items > getAutomaton ( const grammar::CFG < > & originalGrammar );
};

} /* namespace parsing */

} /* namespace grammar */

