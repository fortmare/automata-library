/*
 * LeftFactorize.h
 *
 *  Created on: 9. 6. 2015
 *	  Author: Jan Travnicek
 */

#pragma once

#include <grammar/ContextFree/CFG.h>

namespace grammar {

namespace parsing {

class LeftFactorize {
public:
	static void leftFactorize ( grammar::CFG < > & grammar, const DefaultSymbolType & terminal, const DefaultSymbolType & nonterminal );

};

} /* namespace parsing */

} /* namespace grammar */

