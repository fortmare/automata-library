/*
 * HandleFirstFirstConflict.h
 *
 *  Created on: 9. 6. 2015
 *	  Author: Jan Travnicek
 */

#pragma once

#include <grammar/ContextFree/CFG.h>
#include <alib/vector>

namespace grammar {

namespace parsing {

class HandleFirstFirstConflict {
public:
	static void handleFirstFirstConflict ( grammar::CFG < > & grammar, const DefaultSymbolType & terminal, const DefaultSymbolType & nonterminal, const ext::set < ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > > > & rhsds );
};

} /* namespace parsing */

} /* namespace grammar */

