/*
 * Substitute.h
 *
 *  Created on: 9. 6. 2015
 *	  Author: Jan Travnicek
 */

#pragma once

#include <grammar/ContextFree/CFG.h>
#include <alib/vector>

namespace grammar {

namespace parsing {

class Substitute {
public:
	static void substitute ( const grammar::CFG < > & orig, grammar::CFG < > & res, const DefaultSymbolType & origLHS, const ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > > & origRHS, ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > >::const_iterator nonterminal );

};

} /* namespace parsing */

} /* namespace grammar */

