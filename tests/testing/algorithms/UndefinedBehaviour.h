/*
 * UndefinedBehaviour.h
 *
 *  Created on: 4. 4. 2020
 *	  Author: Tomas Pecka
 */

#pragma once

namespace debug {

class UndefinedBehaviour {

public:
	static int undefined_behaviour ( );
};

} /* namespace debug */
