/*
 * Throw.h
 *
 *  Created on: 7. 10. 2020
 *	  Author: Jan Travnicek
 */

#pragma once

namespace debug {

class Throw {
public:
	static int throwme ( );
};

} /* namespace debug */
