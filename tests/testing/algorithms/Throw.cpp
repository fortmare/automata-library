/*
 * Throw.h
 *
 *  Created on: 7. 10. 2020
 *	  Author: Jan Travnicek
 */

#include "Throw.h"
#include <registration/AlgoRegistration.hpp>

namespace debug {

int debug::Throw::throwme ( ) {
	throw 10;
}

} /* namespace debug */

namespace {

auto ThrowInt = registration::AbstractRegister < debug::Throw, int > ( debug::Throw::throwme );

} /* namespace */
