/*
 * Segfault.h
 *
 *  Created on: 18. 3. 2019
 *	  Author: Tomas Pecka
 */

#pragma once

namespace debug {

class Segfault {
	static int * NULL_VALUE;

public:
	static int segfault ( );
};

} /* namespace debug */
