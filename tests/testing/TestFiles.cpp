#include "TestFiles.hpp"
#include <exception/CommonException.h>

#include "configure_tests.hpp"

using namespace std::literals;


std::filesystem::path TestFiles::TEST_FILES_BASEDIR = std::filesystem::path ( CMAKE_CURRENT_SOURCE_DIR ) / ".." / "examples2";

std::vector < std::string > TestFiles::Get ( const std::string& regex ) {
	return Get ( std::regex ( regex ) );
}

std::vector < std::string > TestFiles::Get ( const std::regex& regex ) {
	std::vector < std::string > res;

	try {
		for ( const std::filesystem::directory_entry & entry: std::filesystem::recursive_directory_iterator ( TEST_FILES_BASEDIR ) ) {
			std::smatch match;
			const std::string path = entry.path ( );

			if ( std::regex_search ( path, match, regex ) ) {
				res.push_back ( path );
			}
		}
	} catch ( const std::filesystem::filesystem_error & e ) {
		throw;
	}

	return res;
}

std::string TestFiles::GetOne ( const std::string& regex ) {
	return GetOne ( std::regex ( regex ) );
}

std::string TestFiles::GetOne ( const std::regex& regex ) {
	std::vector < std::string > res = Get ( regex );

	if ( res.size ( ) > 1 )
		throw std::invalid_argument ( "Multiple files found for TestFiles::GetOne in '"s + std::string ( TEST_FILES_BASEDIR ) + "'"s );

	if ( res.empty ( ) )
		throw std::invalid_argument ( "No files found for TestFiles::GetOne. TestFilesDir is "s + std::string ( TEST_FILES_BASEDIR ) + "'"s );

	return res.at ( 0 );
}
