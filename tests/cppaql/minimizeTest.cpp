#include <catch2/catch.hpp>
#include <alib/vector>

#include "testing/TimeoutAqlTest.hpp"
#include "testing/TestFiles.hpp"

const unsigned RAND_STATES   = 15;
const unsigned RAND_ALPHABET = 5;
const double   RAND_DENSITY  = 2.5;
const size_t   ITERATIONS = 100;

static std::string qGenNFA ( ) {
	std::ostringstream oss;
	oss << "execute automaton::generate::RandomAutomatonFactory ";
	oss << "(size_t)" << rand ( ) % RAND_STATES + 1 << " ";
	oss << "(size_t)" << rand ( ) % RAND_ALPHABET + 1 << " ";
	oss << "(bool)true ";
	oss << "(double)\"" << RAND_DENSITY << "\"";
	return oss.str ( );
}

TEST_CASE ( "Minimization FA test", "[integration]" ) {
	static const std::string qDet ( "automaton::determinize::Determinize - | automaton::simplify::Trim -" );
	static const std::string qMinimizeHop ( qDet + " | " + "automaton::simplify::Minimize - | automaton::simplify::Normalize -" );
	static const std::string qMinimizeBrz ( qDet + " | " + "automaton::simplify::MinimizeBrzozowski - | automaton::simplify::Trim - | automaton::simplify::Normalize -" );
	static const std::string qMinimizeDis ( qDet + " | " + "automaton::simplify::MinimizeByPartitioning - <( $gen | " + qDet + " | automaton::properties::DistinguishableStates - | relation::RelationComplement - <( $gen | " + qDet + " | automaton::States::get - ) | relation::InducedEquivalence - ) | automaton::simplify::Normalize -" );
	static const std::string qMinimizeUnd ( qDet + " | " + "automaton::simplify::MinimizeByPartitioning - <( $gen | " + qDet + " | automaton::properties::UndistinguishableStates - | relation::InducedEquivalence - ) | automaton::simplify::Normalize -" );


	SECTION ( "Files tests" ) {
		for ( const std::string & inputFile : TestFiles::Get ( "/automaton/aconversion.test.*.xml$" ) ) {
			ext::vector < std::string > qs;

			qs = {
				"execute < " + inputFile + " > $gen",
				"quit compare::AutomatonCompare <( $gen | " + qMinimizeHop + " )" + " <( $gen | " + qMinimizeBrz + ")"
			};
			TimeoutAqlTest ( 2s, qs );

			qs = {
				"execute < " + inputFile + " > $gen",
				"quit compare::AutomatonCompare <( $gen | " + qMinimizeHop + " )" + " <( $gen | " + qMinimizeDis + ")"
			};
			TimeoutAqlTest ( 2s, qs );

			qs = {
				"execute < " + inputFile + " > $gen",
				"quit compare::AutomatonCompare <( $gen | " + qMinimizeHop + " )" + " <( $gen | " + qMinimizeUnd + ")"
			};
			TimeoutAqlTest ( 2s, qs );
		}
	}

	SECTION ( "Random tests" ) {
		for ( size_t i = 0; i < ITERATIONS; i++ ) {
			ext::vector < std::string > qs;

			qs = {
				qGenNFA ( ) + " > $gen",
				"quit compare::AutomatonCompare <( $gen | " + qMinimizeHop + " )" + " <( $gen | " + qMinimizeBrz + ")"
			};
			TimeoutAqlTest ( 2s, qs );

			qs = {
				qGenNFA ( ) + " > $gen",
				"quit compare::AutomatonCompare <( $gen | " + qMinimizeHop + " )" + " <( $gen | " + qMinimizeDis + ")"
			};
			TimeoutAqlTest ( 2s, qs );

			qs = {
				qGenNFA ( ) + " > $gen",
				"quit compare::AutomatonCompare <( $gen | " + qMinimizeHop + " )" + " <( $gen | " + qMinimizeUnd + ")"
			};
			TimeoutAqlTest ( 2s, qs );
		}
	}
}
