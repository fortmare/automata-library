#include <alib/vector>
#include <catch2/catch.hpp>

#include "testing/TestFiles.hpp"
#include "testing/TimeoutAqlTest.hpp"

const unsigned RAND_STATES = 15;
const unsigned RAND_ALPHABET = 5;
const unsigned RAND_RANK = 4;
const double RAND_DENSITY = 5;
const size_t ITERATIONS = 50;

std::string qGenNFTA ( ) {
    std::ostringstream oss;
    oss << "execute automaton::generate::RandomTreeAutomatonFactory ";
    oss << "(size_t)" << rand ( ) % RAND_STATES + 1 << " ";
    oss << "(size_t)" << rand ( ) % RAND_ALPHABET + 1 << " ";
    oss << "(size_t)" << rand ( ) % RAND_RANK << " ";
    oss << "(bool)true ";
    oss << "(double)\"" << RAND_DENSITY << "\"";
    return oss.str ( );
}

TEST_CASE ( "FTA-RTE conversions test", "[integration]" ) {
    static const std::string qMinimize ( "automaton::determinize::Determinize - | automaton::simplify::Trim - | automaton::simplify::Minimize - | automaton::simplify::Normalize -" );

    SECTION ( "RTE Files test" ) {
        for ( const std::string& inputFile : TestFiles::Get ( "/rte/rte*.xml$" ) ) {
            ext::vector< std::string > qs = {
                "execute < " + inputFile + " | rte::convert::ToFTAGlushkov - > $gen",
                "quit compare::AutomatonCompare <( $gen | " + qMinimize + " )" + " <( $gen | automaton::convert::ToRTEStateElimination - | rte::convert::ToFTAGlushkov - | " + qMinimize + ")" };

            TimeoutAqlTest ( 10s, qs );
        }
    }

    SECTION ( "RTE Random tests" ) {
        for ( size_t i = 0; i < ITERATIONS; i++ ) {
            ext::vector< std::string > qs = {
                qGenNFTA ( ) + " > $gen",
                "quit compare::AutomatonCompare <( $gen | " + qMinimize + " )" + " <( $gen | automaton::convert::ToRTEStateElimination - | rte::convert::ToFTAGlushkov - | " + qMinimize + ")" };

            TimeoutAqlTest ( 10s, qs );
        }
    }

    // ------------------------------------------------------------------------------------------------------

    SECTION ( "DFTA Files test" ) {
        auto files = GENERATE (
            TestFiles::Get ( "/automaton/DFTA.*.xml$" ),
            TestFiles::Get ( "/automaton/NFTA.*.xml$" ) );

        for ( const std::string& file : files ) {
            ext::vector< std::string > qs = {
                "execute < " + file + " > $fta1",
                "execute $fta1 | automaton::convert::ToRTEStateElimination - | rte::convert::ToFTAGlushkov - > $fta2",
                "execute $fta1 | " + qMinimize + " > $m1",
                "execute $fta2 | " + qMinimize + " > $m2",
                "quit compare::AutomatonCompare $m1 $m2",
            };

            TimeoutAqlTest ( 10s, qs );
        }
    }
}
