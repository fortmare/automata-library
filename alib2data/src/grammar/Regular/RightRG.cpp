/*
 * RightRG.cpp
 *
 *  Created on: Nov 17, 2013
 *      Author: Jan Travnicek
 */

#include "RightRG.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class grammar::RightRG < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < grammar::RightRG < > > ( );

} /* namespace */
