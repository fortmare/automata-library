/*
 * GrammarException.h
 *
 *  Created on: Apr 1, 2013
 *      Author: Martin Zak
 */

#pragma once

#include <exception/CommonException.h>

namespace grammar {

/**
 * Exception thrown by an grammar, grammar parser or grammar printer.
 */
class GrammarException: public exception::CommonException {
public:
	explicit GrammarException(const std::string& cause);
};

} /* namespace grammar */

