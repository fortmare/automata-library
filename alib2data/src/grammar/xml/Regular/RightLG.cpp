/*
 * RightLG.cpp
 *
 *  Created on: Nov 17, 2013
 *      Author: Jan Travnicek
 */

#include "RightLG.h"
#include <grammar/Grammar.h>
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < grammar::RightLG < > > ( );
auto xmlRead = registration::XmlReaderRegister < grammar::RightLG < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, grammar::RightLG < > > ( );

} /* namespace */
