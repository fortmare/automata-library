/*
 * RightLG.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Nov 17, 2013
 *      Author: Jan Travnicek
 */

#pragma once

#include <grammar/Regular/RightLG.h>
#include "../common/GrammarFromXMLParser.h"
#include "../common/GrammarToXMLComposer.h"

#include <grammar/AddRawRule.h>

#include <container/xml/ObjectsVariant.h>

namespace core {

template < class TerminalSymbolType, class NonterminalSymbolType >
struct xmlApi < grammar::RightLG < TerminalSymbolType, NonterminalSymbolType > > {
	/**
	 * \brief The XML tag name of class.
	 *
	 * \details Intentionaly a static member function to be safe in the initialisation before the main function starts.
	 *
	 * \returns string representing the XML tag name of the class
	 */
	static std::string xmlTagName() {
		return "RightLG";
	}

	/**
	 * \brief Tests whether the token stream starts with this type
	 *
	 * \params input the iterator to sequence of xml tokens to test
	 *
	 * \returns true if the token stream iterator points to opening tag named with xml tag name of this type, false otherwise.
	 */
	static bool first ( const ext::deque < sax::Token >::const_iterator & input ) {
		return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
	}

	/**
	 * Parsing from a sequence of xml tokens helper.
	 *
	 * \params input the iterator to sequence of xml tokens to parse from
	 *
	 * \returns the new instance of the grammar
	 */
	static grammar::RightLG < TerminalSymbolType, NonterminalSymbolType > parse ( ext::deque < sax::Token >::iterator & input );

	/**
	 * Helper for parsing of individual rules of the grammar from a sequence of xml tokens.
	 *
	 * \params input the iterator to sequence of xml tokens to parse from
	 * \params grammar the grammar to add the rule to
	 */
	static void parseRule ( ext::deque < sax::Token >::iterator & input, grammar::RightLG < TerminalSymbolType, NonterminalSymbolType > & grammar );

	/**
	 * Composing to a sequence of xml tokens helper.
	 *
	 * \param out sink for new xml tokens representing the grammar
	 * \param grammar the grammar to compose
	 */
	static void compose ( ext::deque < sax::Token > & out, const grammar::RightLG < TerminalSymbolType, NonterminalSymbolType > & grammar );

	/**
	 * Helper for composing rules of the grammar to a sequence of xml tokens.
	 *
	 * \param out sink for xml tokens representing the rules of the grammar
	 * \param grammar the grammar to compose
	 */
	static void composeRules ( ext::deque < sax::Token > & out, const grammar::RightLG < TerminalSymbolType, NonterminalSymbolType > & grammar );
};

template < class TerminalSymbolType, class NonterminalSymbolType >
grammar::RightLG < TerminalSymbolType, NonterminalSymbolType > xmlApi < grammar::RightLG < TerminalSymbolType, NonterminalSymbolType > >::parse ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );

	ext::set < NonterminalSymbolType > nonterminalAlphabet = grammar::GrammarFromXMLParser::parseNonterminalAlphabet < NonterminalSymbolType > ( input );
	ext::set < TerminalSymbolType > terminalAlphabet = grammar::GrammarFromXMLParser::parseTerminalAlphabet < TerminalSymbolType > ( input );
	NonterminalSymbolType initialSymbol = grammar::GrammarFromXMLParser::parseInitialSymbol < NonterminalSymbolType > ( input );

	grammar::RightLG < TerminalSymbolType, NonterminalSymbolType > grammar ( std::move ( initialSymbol ) );

	grammar.setNonterminalAlphabet ( std::move ( nonterminalAlphabet ) );
	grammar.setTerminalAlphabet ( std::move ( terminalAlphabet ) );

	grammar::GrammarFromXMLParser::parseRules ( input, grammar );

	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, xmlTagName ( ) );
	return grammar;
}

template < class TerminalSymbolType, class NonterminalSymbolType >
void xmlApi < grammar::RightLG < TerminalSymbolType, NonterminalSymbolType > >::parseRule ( ext::deque < sax::Token >::iterator & input, grammar::RightLG < TerminalSymbolType, NonterminalSymbolType > & grammar ) {
	NonterminalSymbolType lhs = grammar::GrammarFromXMLParser::parseRuleSingleSymbolLHS < NonterminalSymbolType > ( input );
	ext::vector < ext::variant < TerminalSymbolType, NonterminalSymbolType > > rhs = grammar::GrammarFromXMLParser::parseRuleRHS < ext::variant < TerminalSymbolType, NonterminalSymbolType > > ( input );

	grammar::AddRawRule::addRawRule ( grammar, std::move ( lhs ), std::move ( rhs ) );
}

template < class TerminalSymbolType, class NonterminalSymbolType >
void xmlApi < grammar::RightLG < TerminalSymbolType, NonterminalSymbolType > >::compose ( ext::deque < sax::Token > & out, const grammar::RightLG < TerminalSymbolType, NonterminalSymbolType > & grammar ) {
	out.emplace_back ( xmlTagName ( ), sax::Token::TokenType::START_ELEMENT );

	grammar::GrammarToXMLComposer::composeNonterminalAlphabet ( out, grammar.getNonterminalAlphabet ( ) );
	grammar::GrammarToXMLComposer::composeTerminalAlphabet ( out, grammar.getTerminalAlphabet ( ) );
	grammar::GrammarToXMLComposer::composeInitialSymbol ( out, grammar.getInitialSymbol ( ) );
	composeRules ( out, grammar );

	out.emplace_back ( xmlTagName ( ), sax::Token::TokenType::END_ELEMENT );
}

template < class TerminalSymbolType, class NonterminalSymbolType >
void xmlApi < grammar::RightLG < TerminalSymbolType, NonterminalSymbolType > >::composeRules ( ext::deque < sax::Token > & out, const grammar::RightLG < TerminalSymbolType, NonterminalSymbolType > & grammar ) {
	out.emplace_back ( "rules", sax::Token::TokenType::START_ELEMENT );

	for ( const auto & rule : grammar.getRules ( ) )

		for ( const auto & rhs : rule.second ) {
			out.emplace_back ( "rule", sax::Token::TokenType::START_ELEMENT );

			grammar::GrammarToXMLComposer::composeRuleSingleSymbolLHS ( out, rule.first );
			grammar::GrammarToXMLComposer::composeRuleRightLGRHS ( out, rhs );

			out.emplace_back ( "rule", sax::Token::TokenType::END_ELEMENT );
		}

	out.emplace_back ( "rules", sax::Token::TokenType::END_ELEMENT );
}

} /* namespace core */

