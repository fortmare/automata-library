/*
 * GNF.cpp
 *
 *  Created on: Nov 17, 2013
 *      Author: Jan Travnicek
 */

#include "GNF.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class grammar::GNF < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < grammar::GNF < > > ( );

} /* namespace */
