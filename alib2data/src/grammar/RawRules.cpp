/*
 * RawRules.cpp
 *
 *  Created on: 22. 3. 2014
 *	  Author: Tomas Pecka
 */

#include "RawRules.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto RawRulesCFG = registration::AbstractRegister < grammar::RawRules, ext::map < DefaultSymbolType, ext::set < ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > > > >, const grammar::CFG < > & > ( grammar::RawRules::getRawRules );
auto RawRulesEpsilonFreeCFG = registration::AbstractRegister < grammar::RawRules, ext::map < DefaultSymbolType, ext::set < ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > > > >, const grammar::EpsilonFreeCFG < > & > ( grammar::RawRules::getRawRules );
auto RawRulesGNF = registration::AbstractRegister < grammar::RawRules, ext::map < DefaultSymbolType, ext::set < ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > > > >, const grammar::GNF < > & > ( grammar::RawRules::getRawRules );
auto RawRulesCNF = registration::AbstractRegister < grammar::RawRules, ext::map < DefaultSymbolType, ext::set < ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > > > >, const grammar::CNF < > & > ( grammar::RawRules::getRawRules );
auto RawRulesLG = registration::AbstractRegister < grammar::RawRules, ext::map < DefaultSymbolType, ext::set < ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > > > >, const grammar::LG < > & > ( grammar::RawRules::getRawRules );
auto RawRulesLeftLG = registration::AbstractRegister < grammar::RawRules, ext::map < DefaultSymbolType, ext::set < ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > > > >, const grammar::LeftLG < > & > ( grammar::RawRules::getRawRules );
auto RawRulesLeftRG = registration::AbstractRegister < grammar::RawRules, ext::map < DefaultSymbolType, ext::set < ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > > > >, const grammar::LeftRG < > & > ( grammar::RawRules::getRawRules );
auto RawRulesRightLG = registration::AbstractRegister < grammar::RawRules, ext::map < DefaultSymbolType, ext::set < ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > > > >, const grammar::RightLG < > & > ( grammar::RawRules::getRawRules );
auto RawRulesRightRG = registration::AbstractRegister < grammar::RawRules, ext::map < DefaultSymbolType, ext::set < ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > > > >, const grammar::RightRG < > & > ( grammar::RawRules::getRawRules );

} /* namespace */
