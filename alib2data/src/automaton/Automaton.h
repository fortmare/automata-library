/*
 * Automaton.h
 *
 *  Created on: Apr 10, 2013
 *      Author: Martin Zak
 */

#pragma once

#include <type_traits>

namespace automaton {

/**
 * \brief Wrapper around any automaton type.
 */
class Automaton;

} /* namespace automaton */

