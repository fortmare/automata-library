/*
 * VisiblyPushdownNPDA.cpp
 *
 *  Created on: Apr 10, 2013
 *      Author: Jan Travnicek
 */

#include "VisiblyPushdownNPDA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::VisiblyPushdownNPDA < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::VisiblyPushdownNPDA < > > ( );

} /* namespace */
