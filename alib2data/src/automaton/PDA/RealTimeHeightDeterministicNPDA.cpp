/*
 * RealTimeHeightDeterministicNPDA.cpp
 *
 *  Created on: Apr 10, 2013
 *      Author: Jan Travnicek
 */

#include "RealTimeHeightDeterministicNPDA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::RealTimeHeightDeterministicNPDA < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::RealTimeHeightDeterministicNPDA < > > ( );

} /* namespace */
