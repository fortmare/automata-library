/*
 * NPDTA.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: 10. 5. 2016
 *      Author: Jakub Doupal
 */

#pragma once

#include <sstream>

#include <alib/set>
#include <alib/multimap>
#include <alib/vector>
#include <alib/algorithm>

#include <core/components.hpp>

#include <common/DefaultStateType.h>
#include <common/DefaultSymbolType.h>

#include <automaton/AutomatonException.h>

#include <core/normalize.hpp>
#include <alphabet/common/SymbolNormalize.h>
#include <automaton/common/AutomatonNormalize.h>

namespace automaton {

class InputAlphabet;
class OutputAlphabet;
class PushdownStoreAlphabet;
class InitialSymbol;
class States;
class FinalStates;
class InitialState;

/**
 * Nondeterministic Pushdown Translation Automaton. Translates context free languages.
 *
 * \details
 * Definition
 * A = (Q, T, D, G, I, Z, \delta, F),
 * Q (States) = nonempty finite set of states,
 * T (TerminalAlphabet) = finite set of terminal symbols - having this empty won't let automaton do much though,
 * T (OutputAlphabet) = finite set of output symbols - having this empty won't let automaton do translation at all,
 * G (PushdownStoreAlphabet) = finite set of pushdown store symbol - having this empty makes the automaton equivalent to DFA
 * I (InitialState) = initial state,
 * Z (InitialPushdownStoreSymbol) = initial pushdown store symbol
 * \delta = transition function of the form A \times a \times \alpha -> B \times \beta \times \gamma, where A, B \in Q, a \in T \cup { \eps }, \alpha, \beta \in G*, and \gamma \in D*,
 * F (FinalStates) = set of final states
 *
 * \tparam InputSymbolTypeT used for the terminal alphabet
 * \tparam OutputSymbolTypeT used for the output alphabet
 * \tparam PushdownSymbolTypeT used for the pushdown store alphabet
 * \tparam StateTypeT used to the states, and the initial state of the automaton.
 */
template < class InputSymbolTypeT = DefaultSymbolType, class OutputSymbolTypeT = DefaultSymbolType, class PushdownStoreSymbolTypeT = DefaultSymbolType, class StateTypeT = DefaultStateType >
class NPDTA final : public core::Components < NPDTA < InputSymbolTypeT, OutputSymbolTypeT, PushdownStoreSymbolTypeT, StateTypeT >, ext::set < InputSymbolTypeT >, component::Set, InputAlphabet, ext::set < OutputSymbolTypeT >, component::Set, OutputAlphabet, ext::set < PushdownStoreSymbolTypeT >, component::Set, PushdownStoreAlphabet, PushdownStoreSymbolTypeT, component::Value, InitialSymbol, ext::set < StateTypeT >, component::Set, std::tuple < States, FinalStates >, StateTypeT, component::Value, InitialState > {
public:
	typedef InputSymbolTypeT InputSymbolType;
	typedef OutputSymbolTypeT OutputSymbolType;
	typedef PushdownStoreSymbolTypeT PushdownStoreSymbolType;
	typedef StateTypeT StateType;

private:
	/**
	 * Transition function as mapping from a state times an input symbol or epsilon times string of pushdown store symbols on the left hand side to a state times string of pushdown store symbols times string of output symbols.
	 */
	ext::multimap < ext::tuple < StateType, common::symbol_or_epsilon < InputSymbolType >, ext::vector < PushdownStoreSymbolType > >, ext::tuple < StateType, ext::vector < PushdownStoreSymbolType >, ext::vector < OutputSymbolType > > > transitions;

public:
	/**
	 * \brief Creates a new instance of the automaton with a concrete set of states, input alphabet, pushdown store alphabet, initial state, initial pushdown symbol and a set of final states.
	 *
	 * \param states the initial set of states of the automaton
	 * \param inputAlphabet the initial input alphabet
	 * \param outputAlphabet the initial output alphabet
	 * \param pushdownStoreAlphabet the initial set of symbols used in the pushdown store by the automaton
	 * \param initialState the initial state of the automaton
	 * \param initialPushdownSymbol the initial pushdown symbol of the automaton
	 * \param finalStates the initial set of final states of the automaton
	 */
	explicit NPDTA ( ext::set < StateType > states, ext::set < InputSymbolType > inputAlphabet, ext::set < OutputSymbolType > outputAlphabet, ext::set < PushdownStoreSymbolType > pushdownStoreAlphabet, StateType initialState, PushdownStoreSymbolType initialSymbol, ext::set < StateType > finalStates );

	/**
	 * \brief Creates a new instance of the automaton with a concrete initial state and initial pushdown store symbol.
	 *
	 * \param initialState the initial state of the automaton
	 * \param initialPushdownSymbol the initial pushdown symbol of the automaton
	 */
	explicit NPDTA ( StateType initialState, PushdownStoreSymbolType initialPushdownSymbol );

	/**
	 * Getter of the initial state.
	 *
	 * \returns the initial state of the automaton
	 */
	const StateType & getInitialState ( ) const & {
		return this-> template accessComponent < InitialState > ( ).get ( );
	}

	/**
	 * Getter of the initial state.
	 *
	 * \returns the initial state of the automaton
	 */
	StateType && getInitialState ( ) && {
		return std::move ( this-> template accessComponent < InitialState > ( ).get ( ) );
	}

	/**
	 * Setter of the initial state.
	 *
	 * \param state new initial state of the automaton
	 *
	 * \returns true if the initial state was indeed changed
	 */
	bool setInitialState ( StateType state ) {
		return this-> template accessComponent < InitialState > ( ).set ( std::move ( state ) );
	}

	/**
	 * Getter of states.
	 *
	 * \returns the states of the automaton
	 */
	const ext::set < StateType > & getStates ( ) const & {
		return this-> template accessComponent < States > ( ).get ( );
	}

	/**
	 * Getter of states.
	 *
	 * \returns the states of the automaton
	 */
	ext::set < StateType > && getStates ( ) && {
		return std::move ( this-> template accessComponent < States > ( ).get ( ) );
	}

	/**
	 * Adder of a state.
	 *
	 * \param state the new state to be added to a set of states
	 *
	 * \returns true if the state was indeed added
	 */
	bool addState ( StateType state ) {
		return this-> template accessComponent < States > ( ).add ( std::move ( state ) );
	}

	/**
	 * Setter of states.
	 *
	 * \param states completely new set of states
	 */
	void setStates ( ext::set < StateType > states ) {
		this-> template accessComponent < States > ( ).set ( std::move ( states ) );
	}

	/**
	 * Remover of a state.
	 *
	 * \param state a state to be removed from a set of states
	 *
	 * \returns true if the state was indeed removed
	 */
	void removeState ( const StateType & state ) {
		this-> template accessComponent < States > ( ).remove ( state );
	}

	/**
	 * Getter of final states.
	 *
	 * \returns the final states of the automaton
	 */
	const ext::set < StateType > & getFinalStates ( ) const & {
		return this-> template accessComponent < FinalStates > ( ).get ( );
	}

	/**
	 * Getter of final states.
	 *
	 * \returns the final states of the automaton
	 */
	ext::set < StateType > && getFinalStates ( ) && {
		return std::move ( this-> template accessComponent < FinalStates > ( ).get ( ) );
	}

	/**
	 * Adder of a final state.
	 *
	 * \param state the new state to be added to a set of final states
	 *
	 * \returns true if the state was indeed added
	 */
	bool addFinalState ( StateType state ) {
		return this-> template accessComponent < FinalStates > ( ).add ( std::move ( state ) );
	}

	/**
	 * Setter of final states.
	 *
	 * \param states completely new set of final states
	 */
	void setFinalStates ( ext::set < StateType > states ) {
		this-> template accessComponent < FinalStates > ( ).set ( std::move ( states ) );
	}

	/**
	 * Remover of a final state.
	 *
	 * \param state a state to be removed from a set of final states
	 *
	 * \returns true if the state was indeed removed
	 */
	void removeFinalState ( const StateType & state ) {
		this-> template accessComponent < FinalStates > ( ).remove ( state );
	}

	/**
	 * Getter of the pushdown store alphabet.
	 *
	 * \returns the pushdown store alphabet of the automaton
	 */
	const ext::set < PushdownStoreSymbolType > & getPushdownStoreAlphabet ( ) const & {
		return this->template accessComponent < PushdownStoreAlphabet > ( ).get ( );
	}

	/**
	 * Getter of the pushdown store alphabet.
	 *
	 * \returns the pushdown store alphabet of the automaton
	 */
	ext::set < PushdownStoreSymbolType > && getPushdownStoreAlphabet ( ) && {
		return std::move ( this->template accessComponent < PushdownStoreAlphabet > ( ).get ( ) );
	}

	/**
	 * Adder of a pushdown store symbol.
	 *
	 * \param symbol the new symbol to be added to a pushdown store alphabet
	 *
	 * \returns true if the symbol was indeed added
	 */
	bool addPushdownStoreSymbol ( PushdownStoreSymbolType symbol ) {
		return this->template accessComponent < PushdownStoreAlphabet > ( ).add ( std::move ( symbol ) );
	}

	/**
	 * Adder of pushdown store symbols.
	 *
	 * \param symbols new symbols to be added to a pushdown store alphabet
	 */
	void addPushdownStoreSymbols ( ext::set < PushdownStoreSymbolType > symbols ) {
		this->template accessComponent < PushdownStoreAlphabet > ( ).add ( std::move ( symbols ) );
	}

	/**
	 * Setter of a pushdown store alphabet.
	 *
	 * \param symbols completely new pushdown store alphabet
	 */
	void setPushdownStoreAlphabet ( ext::set < PushdownStoreSymbolType > symbols ) {
		this->template accessComponent < PushdownStoreAlphabet > ( ).set ( std::move ( symbols ) );
	}

	/**
	 * Remover of an pushdown store symbol.
	 *
	 * \param symbol a symbol to be removed from a pushdown store alphabet
	 *
	 * \returns true if the symbol was indeed removed
	 */
	void removePushdownStoreSymbol ( const PushdownStoreSymbolType & symbol ) {
		this->template accessComponent < PushdownStoreAlphabet > ( ).remove ( symbol );
	}

	/**
	 * Getter of the initial pushdown store symbol.
	 *
	 * \returns the initial pushdown store symbol of the automaton
	 */
	const PushdownStoreSymbolType & getInitialSymbol ( ) const & {
		return this->template accessComponent < InitialSymbol > ( ).get ( );
	}

	/**
	 * Getter of the initial pushdown store symbol.
	 *
	 * \returns the initial pushdown store symbol of the automaton
	 */
	PushdownStoreSymbolType && getInitialSymbol ( ) && {
		return std::move ( this->template accessComponent < InitialSymbol > ( ).get ( ) );
	}

	/**
	 * Setter of the initial pushdown store symbol.
	 *
	 * \param symbol new initial pushdown store symbol of the automaton
	 *
	 * \returns true if the initial pushdown store symbol was indeed changed
	 */
	bool setInitialSymbol ( PushdownStoreSymbolType symbol ) {
		return this->template accessComponent < InitialSymbol > ( ).set ( std::move ( symbol ) );
	}

	/**
	 * Getter of the input alphabet.
	 *
	 * \returns the input alphabet of the automaton
	 */
	const ext::set < InputSymbolType > & getInputAlphabet ( ) const & {
		return this-> template accessComponent < InputAlphabet > ( ).get ( );
	}

	/**
	 * Getter of the input alphabet.
	 *
	 * \returns the input alphabet of the automaton
	 */
	ext::set < InputSymbolType > && getInputAlphabet ( ) && {
		return std::move ( this-> template accessComponent < InputAlphabet > ( ).get ( ) );
	}

	/**
	 * Adder of a input symbol.
	 *
	 * \param symbol the new symbol to be added to an input alphabet
	 *
	 * \returns true if the symbol was indeed added
	 */
	bool addInputSymbol ( InputSymbolType symbol ) {
		return this-> template accessComponent < InputAlphabet > ( ).add ( std::move ( symbol ) );
	}

	/**
	 * Adder to an input symbols.
	 *
	 * \param symbols new symbols to be added to an input alphabet
	 */
	void addInputSymbols ( ext::set < InputSymbolType > symbols ) {
		this-> template accessComponent < InputAlphabet > ( ).add ( std::move ( symbols ) );
	}

	/**
	 * Setter of input alphabet.
	 *
	 * \param symbols completely new input alphabet
	 */
	void setInputAlphabet ( ext::set < InputSymbolType > symbols ) {
		this-> template accessComponent < InputAlphabet > ( ).set ( std::move ( symbols ) );
	}

	/**
	 * Remover of an input symbol.
	 *
	 * \param symbol a symbol to be removed from an input alphabet
	 *
	 * \returns true if the symbol was indeed removed
	 */
	void removeInputSymbol ( const InputSymbolType & symbol ) {
		this-> template accessComponent < InputAlphabet > ( ).remove ( symbol );
	}

	/**
	 * Getter of the input alphabet.
	 *
	 * \returns the input alphabet of the automaton
	 */
	const ext::set < OutputSymbolType > & getOutputAlphabet ( ) const & {
		return this->template accessComponent < OutputAlphabet > ( ).get ( );
	}

	/**
	 * Getter of the input alphabet.
	 *
	 * \returns the input alphabet of the automaton
	 */
	ext::set < OutputSymbolType > && getOutputAlphabet ( ) && {
		return std::move ( this->template accessComponent < OutputAlphabet > ( ).get ( ) );
	}

	/**
	 * Adder to an output symbol.
	 *
	 * \param symbol the new symbol to be added to an output alphabet
	 *
	 * \returns true if the symbol was indeed added
	 */
	bool addOutputSymbol ( OutputSymbolType symbol ) {
		return this->template accessComponent < OutputAlphabet > ( ).add ( std::move ( symbol ) );
	}

	/**
	 * Adder to an output symbols.
	 *
	 * \param symbols new symbols to be added to an output alphabet
	 */
	void addOutputSymbols ( ext::set < OutputSymbolType > symbols ) {
		this->template accessComponent < OutputAlphabet > ( ).add ( std::move ( symbols ) );
	}

	/**
	 * Setter of an output alphabet.
	 *
	 * \param symbols completely new output alphabet
	 */
	void setOutputAlphabet ( ext::set < OutputSymbolType > symbols ) {
		this->template accessComponent < OutputAlphabet > ( ).set ( std::move ( symbols ) );
	}

	/**
	 * Remover of an output symbol.
	 *
	 * \param symbol a symbol to be removed from an output alphabet
	 *
	 * \returns true if the symbol was indeed removed
	 */
	void removeOutputSymbol ( const OutputSymbolType & symbol ) {
		this->template accessComponent < OutputAlphabet > ( ).remove ( symbol );
	}

	/**
	 * \brief Adds a transition to the automaton.
	 *
	 * \details The transition is in a form A \times a \times \alpha -> B \times \beta \times \gamma, where A, B \in Q, a \in T \cup { \eps }, \alpha, \beta \in G*, and \gamma in D*
	 *
	 * \param from the source state (A)
	 * \param input the input symbol or epsilon (a)
	 * \param pop symbols to be poped from pushdown store on the transition use (\alpha)
	 * \param to the target state (B)
	 * \param push symbols to be pushed to the pushdown store on the transition use (\beta)
	 * \param output resulting symbols of the transition when used (\gamma)
	 *
	 * \throws AutomatonException when transition contains state or symbol not present in the automaton components
	 *
	 * \returns true if the transition was indeed added
	 */
	bool addTransition ( StateType from, common::symbol_or_epsilon < InputSymbolType > input, ext::vector < PushdownStoreSymbolType > pop, StateType to, ext::vector < PushdownStoreSymbolType > push, ext::vector < OutputSymbolType > output );

	/**
	 * \brief Adds a transition to the automaton.
	 *
	 * \details The transition is in a form A \times a \times \alpha -> B \times \beta \times \gamma, where A, B \in Q, a \in T, \alpha, \beta \in G*, and \gamma in D*
	 *
	 * \param from the source state (A)
	 * \param input the input symbol (a)
	 * \param pop symbols to be poped from pushdown store on the transition use (\alpha)
	 * \param to the target state (B)
	 * \param push symbols to be pushed to the pushdown store on the transition use (\beta)
	 * \param output resulting symbols of the transition when used (\gamma)
	 *
	 * \throws AutomatonException when transition contains state or symbol not present in the automaton components
	 *
	 * \returns true if the transition was indeed added
	 */
	bool addTransition ( StateType from, InputSymbolType input, ext::vector < PushdownStoreSymbolType > pop, StateType to, ext::vector < PushdownStoreSymbolType > push, ext::vector < OutputSymbolType > output );

	/**
	 * \brief Adds a transition to the automaton.
	 *
	 * \details The transition is in a form A \times \eps \times \alpha -> B \times \beta \times \gamma, where A, B \in Q, \alpha, \beta \in G*, and \gamma in D*
	 *
	 * \param from the source state (A)
	 * \param pop symbols to be poped from pushdown store on the transition use (\alpha)
	 * \param to the target state (B)
	 * \param push symbols to be pushed to the pushdown store on the transition use (\beta)
	 * \param output resulting symbols of the transition when used (\gamma)
	 *
	 * \throws AutomatonException when transition contains state or symbol not present in the automaton components
	 *
	 * \returns true if the transition was indeed added
	 */
	bool addTransition ( StateType from, ext::vector < PushdownStoreSymbolType > pop, StateType to, ext::vector < PushdownStoreSymbolType > push, ext::vector < OutputSymbolType > output );

	/**
	 * \brief Removes a transition from the automaton.
	 *
	 * \details The transition is in a form A \times a \times \alpha -> B \times \beta \times \gamma, where A, B \in Q, a \in T \cup { \eps }, \alpha, \beta \in G*, and \gamma \in D*
	 *
	 * \param from the source state (A)
	 * \param input the input symbol or epsilon (a)
	 * \param pop symbols poped from pushdown store on the transition use (\alpha)
	 * \param to the target state (B)
	 * \param push symbols pushed to the pushdown store on the transition use (\beta)
	 * \param output resulting symbols of the transition when used (\gamma)
	 *
	 * \throws AutomatonException when removed transition left hand side was found but the right hand side did not match.
	 *
	 * \returns true if the transition was indeed removed
	 */
	bool removeTransition ( const StateType & from, const common::symbol_or_epsilon < InputSymbolType > & input, const ext::vector < PushdownStoreSymbolType > & pop, const StateType & to, const ext::vector < PushdownStoreSymbolType > & push, const ext::vector < OutputSymbolType > & output );

	/**
	 * \brief Removes a transition from the automaton.
	 *
	 * \details The transition is in a form A \times a \times \alpha -> B \times \beta \times \gamma, where A, B \in Q, a \in T, \alpha, \beta \in G*, and \gamma \in D*
	 *
	 * \param from the source state (A)
	 * \param input the input symbol (a)
	 * \param pop symbols poped from pushdown store on the transition use (\alpha)
	 * \param to the target state (B)
	 * \param push symbols pushed to the pushdown store on the transition use (\beta)
	 * \param output resulting symbols of the transition when used (\gamma)
	 *
	 * \throws AutomatonException when removed transition left hand side was found but the right hand side did not match.
	 *
	 * \returns true if the transition was indeed removed
	 */
	bool removeTransition ( const StateType & from, const InputSymbolType & input, const ext::vector < PushdownStoreSymbolType > & pop, const StateType & to, const ext::vector < PushdownStoreSymbolType > & push, const ext::vector < OutputSymbolType > & output );

	/**
	 * \brief Removes a transition from the automaton.
	 *
	 * \details The transition is in a form A \times \eps \times \alpha -> B \times \beta \times \gamma, where A, B \in Q, a \in T, \alpha, \beta \in G*, and \gamma \in D*
	 *
	 * \param from the source state (A)
	 * \param pop symbols poped from pushdown store on the transition use (\alpha)
	 * \param to the target state (B)
	 * \param push symbols pushed to the pushdown store on the transition use (\beta)
	 * \param output resulting symbols of the transition when used (\gamma)
	 *
	 * \throws AutomatonException when removed transition left hand side was found but the right hand side did not match.
	 *
	 * \returns true if the transition was indeed removed
	 */
	bool removeTransition ( const StateType & from, const ext::vector < PushdownStoreSymbolType > & pop, const StateType & to, const ext::vector < PushdownStoreSymbolType > & push, const ext::vector < OutputSymbolType > & output );

	/**
	 * Get the transition function of the automaton in its natural form.
	 *
	 * \returns transition function of the automaton
	 */
	const ext::multimap < ext::tuple < StateType, common::symbol_or_epsilon < InputSymbolType >, ext::vector < PushdownStoreSymbolType > >, ext::tuple < StateType, ext::vector < PushdownStoreSymbolType >, ext::vector < OutputSymbolType > > > & getTransitions ( ) const &;

	/**
	 * Get the transition function of the automaton in its natural form.
	 *
	 * \returns transition function of the automaton
	 */
	ext::multimap < ext::tuple < StateType, common::symbol_or_epsilon < InputSymbolType >, ext::vector < PushdownStoreSymbolType > >, ext::tuple < StateType, ext::vector < PushdownStoreSymbolType >, ext::vector < OutputSymbolType > > > && getTransitions ( ) &&;

	/**
	 * Get a subset of the transition function of the automaton, with the source state fixed in the transitions natural representation.
	 *
	 * \param from filter the transition function based on this state as a source state
	 *
	 * \returns a subset of the transition function of the automaton with the source state fixed
	 */
	ext::multimap < ext::tuple < StateType, common::symbol_or_epsilon < InputSymbolType >, ext::vector < PushdownStoreSymbolType > >, ext::tuple < StateType, ext::vector < PushdownStoreSymbolType >, ext::vector < OutputSymbolType > > > getTransitionsFromState ( const StateType & from ) const;

	/**
	 * The three way comparison implementation
	 *
	 * \param other the other instance
	 *
	 * \returns the ordering between this object and the @p other.
	 */
	auto operator <=> ( const NPDTA & other ) const {
		return std::tie(getStates(), getInputAlphabet(), getOutputAlphabet(), getInitialState(), getFinalStates(), getPushdownStoreAlphabet(), getInitialSymbol(), transitions) <=> std::tie(other.getStates(), other.getInputAlphabet(), other.getOutputAlphabet(), other.getInitialState(), other.getFinalStates(), other.getPushdownStoreAlphabet(), other.getInitialSymbol(), other.transitions);
	}

	/**
	 * The equality comparison implementation.
	 *
	 * \param other the other object to compare with.
	 *
	 * \returns true if this and other objects are equal, false othervise
	 */
	bool operator == ( const NPDTA & other ) const {
		return std::tie(getStates(), getInputAlphabet(), getOutputAlphabet(), getInitialState(), getFinalStates(), getPushdownStoreAlphabet(), getInitialSymbol(), transitions) == std::tie(other.getStates(), other.getInputAlphabet(), other.getOutputAlphabet(), other.getInitialState(), other.getFinalStates(), other.getPushdownStoreAlphabet(), other.getInitialSymbol(), other.transitions);
	}

	/**
	 * Print this object as raw representation to ostream.
	 *
	 * \param out ostream where to print
	 * \param instance object to print
	 *
	 * \returns modified output stream
	 */
	friend std::ostream & operator << ( std::ostream & out, const NPDTA & instance ) {
		return out << "(NPDTA "
			   << "states = " << instance.getStates ( )
			   << "inputAlphabet = " << instance.getInputAlphabet ( )
			   << "outputAlphabet = " << instance.getOutputAlphabet ( )
			   << "initialState = " << instance.getInitialState ( )
			   << "finalStates = " << instance.getFinalStates ( )
			   << "pushdownStoreAlphabet = " << instance.getPushdownStoreAlphabet ( )
			   << "initialSymbol = " << instance.getInitialSymbol ( )
			   << "transitions = " << instance.getTransitions ( )
			   << ")";
	}

	/**
	 * Casts this instance to as compact as possible string representation.
	 *
	 * \returns string representation of the object
	 */
	explicit operator std::string ( ) const;
};

template < class InputSymbolType, class OutputSymbolType, class PushdownStoreSymbolType, class StateType >
NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType >::NPDTA ( ext::set < StateType > states, ext::set < InputSymbolType > inputAlphabet, ext::set < OutputSymbolType > outputAlphabet, ext::set < PushdownStoreSymbolType > pushdownStoreAlphabet, StateType initialState, PushdownStoreSymbolType initialSymbol, ext::set < StateType > finalStates ) : core::Components < NPDTA, ext::set < InputSymbolType >, component::Set, InputAlphabet, ext::set < OutputSymbolType >, component::Set, OutputAlphabet, ext::set < PushdownStoreSymbolType >, component::Set, PushdownStoreAlphabet, PushdownStoreSymbolType, component::Value, InitialSymbol, ext::set < StateType >, component::Set, std::tuple < States, FinalStates >, StateType, component::Value, InitialState > ( std::move ( inputAlphabet ), std::move ( outputAlphabet ), std::move ( pushdownStoreAlphabet ), std::move ( initialSymbol ), std::move ( states ), std::move ( finalStates ), std::move ( initialState ) ) {
}

template < class InputSymbolType, class OutputSymbolType, class PushdownStoreSymbolType, class StateType >
NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType >::NPDTA(StateType initialState, PushdownStoreSymbolType initialPushdownSymbol) : NPDTA ( ext::set < StateType > { initialState }, ext::set < InputSymbolType > { }, ext::set < OutputSymbolType > { }, ext::set < PushdownStoreSymbolType > { initialPushdownSymbol }, initialState, initialPushdownSymbol, ext::set < StateType > { }) {
}

template < class InputSymbolType, class OutputSymbolType, class PushdownStoreSymbolType, class StateType >
bool NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType >::addTransition(StateType from, common::symbol_or_epsilon < InputSymbolType > input, ext::vector<PushdownStoreSymbolType> pop, StateType to, ext::vector<PushdownStoreSymbolType> push, ext::vector < OutputSymbolType > output) {
	if (!getStates().count(from)) {
		throw AutomatonException("State \"" + ext::to_string ( from ) + "\" doesn't exist.");
	}

	if ( ! input.is_epsilon ( ) && !getInputAlphabet().count(input.getSymbol ( ) ) ) {
		throw AutomatonException("Input symbol \"" + ext::to_string ( input ) + "\" doesn't exist.");
	}

	if (!getStates().count(to)) {
		throw AutomatonException("State \"" + ext::to_string ( to ) + "\" doesn't exist.");
	}

	for(const PushdownStoreSymbolType& popSymbol : pop) {
		if (!getPushdownStoreAlphabet().count(popSymbol)) {
			throw AutomatonException("Pushdown store symbol \"" + ext::to_string ( popSymbol ) + "\" doesn't exist.");
		}
	}

	for(const PushdownStoreSymbolType& pushSymbol : push) {
		if (!getPushdownStoreAlphabet().count(pushSymbol)) {
			throw AutomatonException("Pushdown store symbol \"" + ext::to_string ( pushSymbol ) + "\" doesn't exist.");
		}
	}

	for(const OutputSymbolType& outputSymbol : output) {
		if (!getOutputAlphabet().count(outputSymbol)) {
			throw AutomatonException("Output symbol \"" + ext::to_string ( outputSymbol ) + "\" doesn't exist.");
		}
	}

	auto upper_bound = transitions.upper_bound ( ext::tie ( from, input, pop ) );
	auto lower_bound = transitions.lower_bound ( ext::tie ( from, input, pop ) );

	ext::tuple < StateType, ext::vector < PushdownStoreSymbolType >, ext::vector < OutputSymbolType > > value ( std::move ( to ), std::move ( push ), std::move ( output ) );

	auto iter = std::lower_bound ( lower_bound, upper_bound, value, [ ] ( const auto & transition, const auto & target ) { return transition.second < target; } );
	if ( iter != upper_bound && value >= iter->second )
		return false;

	ext::tuple < StateType, common::symbol_or_epsilon < InputSymbolType >, ext::vector < PushdownStoreSymbolType > > key ( std::move ( from ), std::move ( input ), std::move ( pop ) );
	transitions.insert ( iter, std::make_pair ( std::move ( key ), std::move ( value ) ) );
	return true;
}

template < class InputSymbolType, class OutputSymbolType, class PushdownStoreSymbolType, class StateType >
bool NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType >::addTransition(StateType from, InputSymbolType input, ext::vector<PushdownStoreSymbolType> pop, StateType to, ext::vector<PushdownStoreSymbolType> push, ext::vector < OutputSymbolType > output) {
	common::symbol_or_epsilon < InputSymbolType > inputVariant(std::move(input));
	return addTransition(std::move(from), std::move(inputVariant), std::move(pop), std::move(to), std::move(push), std::move(output));
}

template < class InputSymbolType, class OutputSymbolType, class PushdownStoreSymbolType, class StateType >
bool NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType >::addTransition(StateType from, ext::vector<PushdownStoreSymbolType> pop, StateType to, ext::vector<PushdownStoreSymbolType> push, ext::vector < OutputSymbolType > output) {
	auto inputVariant = common::symbol_or_epsilon < InputSymbolType > ( );
	return addTransition(std::move(from), std::move(inputVariant), std::move(pop), std::move(to), std::move(push), std::move(output));
}

template < class InputSymbolType, class OutputSymbolType, class PushdownStoreSymbolType, class StateType >
bool NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType >::removeTransition(const StateType& from, const common::symbol_or_epsilon < InputSymbolType >& input, const ext::vector < PushdownStoreSymbolType > & pop, const StateType& to, const ext::vector<PushdownStoreSymbolType>& push, const ext::vector < OutputSymbolType > & output) {
	auto upper_bound = transitions.upper_bound ( ext::tie ( from, input, pop ) );
	auto lower_bound = transitions.lower_bound ( ext::tie ( from, input, pop ) );
	auto iter = std::find_if ( lower_bound, upper_bound, [ & ] ( const auto & transition ) { return std::get < 0 > ( transition.second ) == to && std::get < 1 > ( transition.second ) == push && std::get < 2 > ( transition.second ) == output; } );
	if ( iter == upper_bound )
		return false;

	transitions.erase ( iter );
	return true;
}

template < class InputSymbolType, class OutputSymbolType, class PushdownStoreSymbolType, class StateType >
bool NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType >::removeTransition(const StateType& from, const InputSymbolType& input, const ext::vector<PushdownStoreSymbolType>& pop, const StateType& to, const ext::vector<PushdownStoreSymbolType>& push, const ext::vector < OutputSymbolType > & output) {
	common::symbol_or_epsilon < InputSymbolType > inputVariant(input);
	return removeTransition(from, inputVariant, pop, to, push, output);
}

template < class InputSymbolType, class OutputSymbolType, class PushdownStoreSymbolType, class StateType >
bool NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType >::removeTransition(const StateType& from, const ext::vector<PushdownStoreSymbolType>& pop, const StateType& to, const ext::vector<PushdownStoreSymbolType>& push, const ext::vector < OutputSymbolType > & output) {
	auto inputVariant = common::symbol_or_epsilon < InputSymbolType > ( );
	return removeTransition(from, inputVariant, pop, to, push, output);
}

template < class InputSymbolType, class OutputSymbolType, class PushdownStoreSymbolType, class StateType >
const ext::multimap<ext::tuple<StateType, common::symbol_or_epsilon < InputSymbolType >, ext::vector<PushdownStoreSymbolType> >, ext::tuple<StateType, ext::vector<PushdownStoreSymbolType>, ext::vector < OutputSymbolType > > > & NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType >::getTransitions() const & {
	return transitions;
}

template < class InputSymbolType, class OutputSymbolType, class PushdownStoreSymbolType, class StateType >
ext::multimap<ext::tuple<StateType, common::symbol_or_epsilon < InputSymbolType >, ext::vector<PushdownStoreSymbolType> >, ext::tuple<StateType, ext::vector<PushdownStoreSymbolType>, ext::vector < OutputSymbolType > > > && NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType >::getTransitions() && {
	return std::move ( transitions );
}

template < class InputSymbolType, class OutputSymbolType, class PushdownStoreSymbolType, class StateType >
ext::multimap < ext::tuple < StateType, common::symbol_or_epsilon < InputSymbolType >, ext::vector < PushdownStoreSymbolType > >, ext::tuple < StateType, ext::vector < PushdownStoreSymbolType >, ext::vector < OutputSymbolType > > > NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType >::getTransitionsFromState ( const StateType & from ) const {
	if( !getStates().count(from))
		throw AutomatonException("State \"" + ext::to_string ( from ) + "\" doesn't exist");

	ext::multimap < ext::tuple < StateType, common::symbol_or_epsilon < InputSymbolType >, ext::vector < PushdownStoreSymbolType > >, ext::tuple < StateType, ext::vector < PushdownStoreSymbolType >, ext::vector < OutputSymbolType > > > transitionsFromState;
	for ( const auto & transition: transitions ) {
		if ( std::get<0>(transition.first) == from ) {
			transitionsFromState.insert ( transition );
		}
	}

	return transitionsFromState;
}

template < class InputSymbolType, class OutputSymbolType, class PushdownStoreSymbolType, class StateType >
NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType >::operator std::string ( ) const {
	std::stringstream ss;
	ss << *this;
	return ss.str();
}

} /* namespace automaton */

namespace core {

/**
 * Helper class specifying constraints for the automaton's internal input alphabet component.
 *
 * \tparam InputSymbolType used for the terminal alphabet of the automaton.
 * \tparam OutputSymbolType used for the output alphabet of the automaton.
 * \tparam PushdownSymbolType used for the pushdown store alphabet of the automaton.
 * \tparam StateType used for the terminal alphabet of the automaton.
 */
template < class InputSymbolType, class OutputSymbolType, class PushdownStoreSymbolType, class StateType >
class SetConstraint< automaton::NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType >, InputSymbolType, automaton::InputAlphabet > {
public:
	/**
	 * Returns true if the symbol is still used in some transition of the automaton.
	 *
	 * \param automaton the tested automaton
	 * \param symbol the tested symbol
	 *
	 * \returns true if the symbol is used, false othervise
	 */
	static bool used ( const automaton::NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType > & automaton, const InputSymbolType & symbol ) {
		for ( const std::pair<const ext::tuple<StateType, common::symbol_or_epsilon < InputSymbolType >, ext::vector<PushdownStoreSymbolType> >, ext::tuple<StateType, ext::vector<PushdownStoreSymbolType>, ext::vector < OutputSymbolType > > > & transition : automaton.getTransitions())
			if ( ! std::get<1>(transition.first).is_epsilon ( ) && symbol == std::get<1>(transition.first).getSymbol ( ) )
				return true;

		return false;
	}

	/**
	 * Returns true as all symbols are possibly available to be elements of the input alphabet.
	 *
	 * \param automaton the tested automaton
	 * \param symbol the tested symbol
	 *
	 * \returns true
	 */
	static bool available ( const automaton::NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType > &, const InputSymbolType & ) {
		return true;
	}

	/**
	 * All symbols are valid as input symbols.
	 *
	 * \param automaton the tested automaton
	 * \param symbol the tested symbol
	 */
	static void valid ( const automaton::NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType > &, const InputSymbolType & ) {
	}
};

/**
 * Helper class specifying constraints for the automaton's internal pushdown store alphabet component.
 *
 * \tparam InputSymbolType used for the terminal alphabet of the automaton.
 * \tparam OutputSymbolType used for the output alphabet of the automaton.
 * \tparam PushdownSymbolType used for the pushdown store alphabet of the automaton.
 * \tparam StateType used for the terminal alphabet of the automaton.
 */
template < class InputSymbolType, class OutputSymbolType, class PushdownStoreSymbolType, class StateType >
class SetConstraint< automaton::NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType >, OutputSymbolType, automaton::OutputAlphabet > {
public:
	/**
	 * Returns true if the symbol is still used in some transition of the automaton.
	 *
	 * \param automaton the tested automaton
	 * \param symbol the tested symbol
	 *
	 * \returns true if the symbol is used, false othervise
	 */
	static bool used ( const automaton::NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType > & automaton, const OutputSymbolType & symbol ) {
		for ( const std::pair<const ext::tuple<StateType, common::symbol_or_epsilon < InputSymbolType >, ext::vector<PushdownStoreSymbolType> >, ext::tuple<StateType, ext::vector<PushdownStoreSymbolType>, ext::vector < OutputSymbolType > > > & transition : automaton.getTransitions())
			if (std::find(std::get<2>(transition.second).begin(), std::get<2>(transition.second).end(), symbol) != std::get<2>(transition.second).end())
				return true;

		return false;
	}

	/**
	 * Returns true as all symbols are possibly available to be elements of the output alphabet.
	 *
	 * \param automaton the tested automaton
	 * \param symbol the tested symbol
	 *
	 * \returns true
	 */
	static bool available ( const automaton::NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType > &, const OutputSymbolType & ) {
		return true;
	}

	/**
	 * All symbols are valid as output symbols.
	 *
	 * \param automaton the tested automaton
	 * \param symbol the tested symbol
	 */
	static void valid ( const automaton::NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType > &, const OutputSymbolType & ) {
	}
};

/**
 * Helper class specifying constraints for the automaton's internal pushdown store alphabet component.
 *
 * \tparam InputSymbolType used for the terminal alphabet of the automaton.
 * \tparam OutputSymbolType used for the output alphabet of the automaton.
 * \tparam PushdownSymbolType used for the pushdown store alphabet of the automaton.
 * \tparam StateType used for the terminal alphabet of the automaton.
 */
template < class InputSymbolType, class OutputSymbolType, class PushdownStoreSymbolType, class StateType >
class SetConstraint< automaton::NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType >, PushdownStoreSymbolType, automaton::PushdownStoreAlphabet > {
public:
	/**
	 * Returns true if the symbol is still used in some transition of the automaton.
	 *
	 * \param automaton the tested automaton
	 * \param symbol the tested symbol
	 *
	 * \returns true if the symbol is used, false othervise
	 */
	static bool used ( const automaton::NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType > & automaton, const PushdownStoreSymbolType & symbol ) {
		if(automaton.getInitialSymbol() == symbol)
			return true;

		for ( const std::pair<const ext::tuple<StateType, common::symbol_or_epsilon < InputSymbolType >, ext::vector<PushdownStoreSymbolType> >, ext::tuple<StateType, ext::vector<PushdownStoreSymbolType>, ext::vector < OutputSymbolType > > > & transition : automaton.getTransitions()) {
			for (const PushdownStoreSymbolType& popSymbol : std::get<2>(transition.first))
				if (symbol == popSymbol)
					return true;

			if (std::find(std::get<1>(transition.second).begin(), std::get<1>(transition.second).end(), symbol) != std::get<1>(transition.second).end())
				return true;
		}

		return false;
	}

	/**
	 * Returns true as all symbols are possibly available to be elements of the pushdown store alphabet.
	 *
	 * \param automaton the tested automaton
	 * \param symbol the tested symbol
	 *
	 * \returns true
	 */
	static bool available ( const automaton::NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType > &, const PushdownStoreSymbolType & ) {
		return true;
	}

	/**
	 * All symbols are valid as pushdown store symbols.
	 *
	 * \param automaton the tested automaton
	 * \param symbol the tested symbol
	 */
	static void valid ( const automaton::NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType > &, const PushdownStoreSymbolType & ) {
	}
};

/**
 * Helper class specifying constraints for the automaton's internal pushdown store initial element.
 *
 * \tparam InputSymbolType used for the terminal alphabet of the automaton.
 * \tparam OutputSymbolType used for the output alphabet of the automaton.
 * \tparam PushdownSymbolType used for the pushdown store alphabet of the automaton.
 * \tparam StateType used for the terminal alphabet of the automaton.
 */
template < class InputSymbolType, class OutputSymbolType, class PushdownStoreSymbolType, class StateType >
class ElementConstraint< automaton::NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType >, PushdownStoreSymbolType, automaton::InitialSymbol > {
public:
	/**
	 * Determines whether the initial pushdown store symbol is available in the automaton's pushdown store alphabet.
	 *
	 * \param automaton the tested automaton
	 * \param symbol the tested symbol
	 *
	 * \returns true if the pushdown store symbol is already in the pushdown store alphabet of the automaton
	 */
	static bool available ( const automaton::NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType > & automaton, const PushdownStoreSymbolType & symbol ) {
		return automaton.template accessComponent < automaton::PushdownStoreAlphabet > ( ).get ( ).count ( symbol );
	}

	/**
	 * All pushdown store symbols are valid as an initial pusdown store symbol of the automaton.
	 *
	 * \param automaton the tested automaton
	 * \param symbol the tested symbol
	 */
	static void valid ( const automaton::NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType > &, const PushdownStoreSymbolType & ) {
	}
};

/**
 * Helper class specifying constraints for the automaton's internal states component.
 *
 * \tparam InputSymbolType used for the terminal alphabet of the automaton.
 * \tparam OutputSymbolType used for the output alphabet of the automaton.
 * \tparam PushdownSymbolType used for the pushdown store alphabet of the automaton.
 * \tparam StateType used for the terminal alphabet of the automaton.
 */
template < class InputSymbolType, class OutputSymbolType, class PushdownStoreSymbolType, class StateType >
class SetConstraint< automaton::NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType >, StateType, automaton::States > {
public:
	/**
	 * Returns true if the state is still used in some transition of the automaton.
	 *
	 * \param automaton the tested automaton
	 * \param state the tested state
	 *
	 * \returns true if the state is used, false othervise
	 */
	static bool used ( const automaton::NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType > & automaton, const StateType & state ) {
		if ( automaton.getInitialState ( ) == state )
			return true;

		if ( automaton.getFinalStates ( ).count ( state ) )
			return true;

		for ( const std::pair<const ext::tuple<StateType, common::symbol_or_epsilon < InputSymbolType >, ext::vector<PushdownStoreSymbolType> >, ext::tuple<StateType, ext::vector<PushdownStoreSymbolType>, ext::vector < OutputSymbolType > > > & transition : automaton.getTransitions()) {
			if (state == std::get<0>(transition.first))
				return true;

			if(std::get<0>(transition.second) == state)
				return true;
		}

		return false;
	}

	/**
	 * Returns true as all states are possibly available to be elements of the states.
	 *
	 * \param automaton the tested automaton
	 * \param state the tested state
	 *
	 * \returns true
	 */
	static bool available ( const automaton::NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType > &, const StateType & ) {
		return true;
	}

	/**
	 * All states are valid as a state of the automaton.
	 *
	 * \param automaton the tested automaton
	 * \param state the tested state
	 */
	static void valid ( const automaton::NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType > &, const StateType & ) {
	}
};

/**
 * Helper class specifying constraints for the automaton's internal final states component.
 *
 * \tparam InputSymbolType used for the terminal alphabet of the automaton.
 * \tparam OutputSymbolType used for the output alphabet of the automaton.
 * \tparam PushdownSymbolType used for the pushdown store alphabet of the automaton.
 * \tparam StateType used for the terminal alphabet of the automaton.
 */
template < class InputSymbolType, class OutputSymbolType, class PushdownStoreSymbolType, class StateType >
class SetConstraint< automaton::NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType >, StateType, automaton::FinalStates > {
public:
	/**
	 * Returns false. Final state is only a mark that the automaton itself does require further.
	 *
	 * \param automaton the tested automaton
	 * \param state the tested state
	 *
	 * \returns false
	 */
	static bool used ( const automaton::NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType > &, const StateType & ) {
		return false;
	}

	/**
	 * Determines whether the state is available in the automaton's states set.
	 *
	 * \param automaton the tested automaton
	 * \param state the tested state
	 *
	 * \returns true if the state is already in the set of states of the automaton
	 */
	static bool available ( const automaton::NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType > & automaton, const StateType & state ) {
		return automaton.template accessComponent < automaton::States > ( ).get ( ).count ( state );
	}

	/**
	 * All states are valid as a final state of the automaton.
	 *
	 * \param automaton the tested automaton
	 * \param state the tested state
	 */
	static void valid ( const automaton::NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType > &, const StateType & ) {
	}
};

/**
 * Helper class specifying constraints for the automaton's internal initial state element.
 *
 * \tparam InputSymbolType used for the terminal alphabet of the automaton.
 * \tparam OutputSymbolType used for the output alphabet of the automaton.
 * \tparam PushdownSymbolType used for the pushdown store alphabet of the automaton.
 * \tparam StateType used for the terminal alphabet of the automaton.
 */
template < class InputSymbolType, class OutputSymbolType, class PushdownStoreSymbolType, class StateType >
class ElementConstraint< automaton::NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType >, StateType, automaton::InitialState > {
public:
	/**
	 * Determines whether the state is available in the automaton's states set.
	 *
	 * \param automaton the tested automaton
	 * \param state the tested state
	 *
	 * \returns true if the state is already in the set of states of the automaton
	 */
	static bool available ( const automaton::NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType > & automaton, const StateType & state ) {
		return automaton.template accessComponent < automaton::States > ( ).get ( ).count ( state );
	}

	/**
	 * All states are valid as an initial state of the automaton.
	 *
	 * \param automaton the tested automaton
	 * \param state the tested state
	 */
	static void valid ( const automaton::NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType > &, const StateType & ) {
	}
};

/**
 * Helper for normalisation of types specified by templates used as internal datatypes of symbols and states.
 *
 * \returns new instance of the automaton with default template parameters or unmodified instance if the template parameters were already the default ones
 */
template < class InputSymbolType, class OutputSymbolType, class PushdownStoreSymbolType, class StateType >
struct normalize < automaton::NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType > > {
	static automaton::NPDTA < > eval ( automaton::NPDTA < InputSymbolType, OutputSymbolType, PushdownStoreSymbolType, StateType > && value ) {
		ext::set < DefaultSymbolType > alphabet = alphabet::SymbolNormalize::normalizeAlphabet ( std::move ( value ).getInputAlphabet ( ) );
		ext::set < DefaultSymbolType > outputAlphabet = alphabet::SymbolNormalize::normalizeAlphabet ( std::move ( value ).getOutputAlphabet ( ) );
		ext::set < DefaultSymbolType > pushdownAlphabet = alphabet::SymbolNormalize::normalizeAlphabet ( std::move ( value ).getPushdownStoreAlphabet ( ) );
		DefaultSymbolType initialSymbol = alphabet::SymbolNormalize::normalizeSymbol ( std::move ( value ).getInitialSymbol ( ) );
		ext::set < DefaultStateType > states = automaton::AutomatonNormalize::normalizeStates ( std::move ( value ).getStates ( ) );
		DefaultStateType initialState = automaton::AutomatonNormalize::normalizeState ( std::move ( value ).getInitialState ( ) );
		ext::set < DefaultStateType > finalStates = automaton::AutomatonNormalize::normalizeStates ( std::move ( value ).getFinalStates ( ) );

		automaton::NPDTA < > res ( std::move ( states ), std::move ( alphabet ), std::move ( pushdownAlphabet ), std::move ( outputAlphabet ), std::move ( initialState ), std::move ( initialSymbol ), std::move ( finalStates ) );

		for ( std::pair < ext::tuple < StateType, common::symbol_or_epsilon < InputSymbolType >, ext::vector < PushdownStoreSymbolType > >, ext::tuple < StateType, ext::vector < PushdownStoreSymbolType >, ext::vector < OutputSymbolType > > > && transition : ext::make_mover ( std::move ( value ).getTransitions ( ) ) ) {
			DefaultStateType to = automaton::AutomatonNormalize::normalizeState ( std::move ( std::get < 0 > ( transition.second ) ) );
			ext::vector < DefaultSymbolType > push = alphabet::SymbolNormalize::normalizeSymbols ( std::move ( std::get < 1 > ( transition.second ) ) );

			ext::vector < DefaultSymbolType > output = alphabet::SymbolNormalize::normalizeSymbols ( std::move ( std::get < 2 > ( transition.second ) ) );

			ext::vector < DefaultSymbolType > pop = alphabet::SymbolNormalize::normalizeSymbols ( std::move ( std::get < 2 > ( transition.first ) ) );
			DefaultStateType from = automaton::AutomatonNormalize::normalizeState ( std::move ( std::get < 0 > ( transition.first ) ) );
			common::symbol_or_epsilon < DefaultSymbolType > input = automaton::AutomatonNormalize::normalizeSymbolEpsilon ( std::move ( std::get < 1 > ( transition.first ) ) );

			res.addTransition ( std::move ( from ), std::move ( input ), std::move ( pop ), std::move ( to ), std::move ( push ), std::move ( output ) );
		}

		return res;
	}
};

} /* namespace core */

extern template class automaton::NPDTA < >;

