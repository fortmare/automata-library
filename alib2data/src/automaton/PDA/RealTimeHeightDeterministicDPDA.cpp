/*
 * RealTimeHeightDeterministicDPDA.cpp
 *
 *  Created on: Apr 10, 2013
 *      Author: Jan Travnicek
 */

#include "RealTimeHeightDeterministicDPDA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::RealTimeHeightDeterministicDPDA < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::RealTimeHeightDeterministicDPDA < > > ( );

} /* namespace */
