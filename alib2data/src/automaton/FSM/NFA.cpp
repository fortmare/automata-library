/*
 * NFA.cpp
 *
 *  Created on: Mar 25, 2013
 *      Author: Jan Travnicek
 */

#include "NFA.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class automaton::NFA < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::NFA < > > ( );

auto NFAFromDFA = registration::CastRegister < automaton::NFA < >, automaton::DFA < > > ( );

} /* namespace */
