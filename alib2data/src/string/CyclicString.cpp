/*
 * CyclicString.cpp
 *
 *  Created on: Nov 23, 2013
 *	  Author: Jan Travnicek
 */

#include "CyclicString.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class string::CyclicString < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < string::CyclicString < > > ( );

} /* namespace */
