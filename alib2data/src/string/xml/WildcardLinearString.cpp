/*
 * WildcardLinearString.cpp
 *
 *  Created on: Apr 12, 2018
 *      Author: Jan Travnicek
 */

#include "WildcardLinearString.h"

#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < string::WildcardLinearString < > > ( );
auto xmlRead = registration::XmlReaderRegister < string::WildcardLinearString < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, string::WildcardLinearString < > > ( );

} /* namespace */
