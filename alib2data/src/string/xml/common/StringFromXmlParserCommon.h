/*
 * StringFromXmlParserCommon.h
 *
 *  Created on: Nov 23, 2013
 *      Author: Jan Travnicek
 */

#pragma once

#include <alib/vector>
#include <alib/set>
#include <alib/deque>
#include <sax/Token.h>
#include <sax/FromXMLParserHelper.h>
#include <core/xmlApi.hpp>

namespace string {

/**
 * Parser used to get String from Xml parsed into list of tokens.
 */
class StringFromXmlParserCommon {
public:
	template < class SymbolType >
	static ext::vector < SymbolType > parseContent ( ext::deque < sax::Token >::iterator & input );
	template < class SymbolType >
	static ext::set < SymbolType > parseAlphabet ( ext::deque < sax::Token >::iterator & input );
	template < class SymbolType >
	static SymbolType parseWildcard ( ext::deque < sax::Token >::iterator & input );
};

template < class SymbolType >
ext::set < SymbolType > StringFromXmlParserCommon::parseAlphabet ( ext::deque < sax::Token >::iterator & input ) {
	ext::set < SymbolType > alphabet;

	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, "alphabet" );

	while ( sax::FromXMLParserHelper::isTokenType ( input, sax::Token::TokenType::START_ELEMENT ) )
		alphabet.insert ( core::xmlApi < SymbolType >::parse ( input ) );

	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, "alphabet" );

	return alphabet;
}

template < class SymbolType >
ext::vector < SymbolType > StringFromXmlParserCommon::parseContent ( ext::deque < sax::Token >::iterator & input ) {
	ext::vector < SymbolType > data;

	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, "content" );

	while ( sax::FromXMLParserHelper::isTokenType ( input, sax::Token::TokenType::START_ELEMENT ) )
		data.push_back ( core::xmlApi < SymbolType >::parse ( input ) );

	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, "content" );

	return data;
}

template < class SymbolType >
SymbolType StringFromXmlParserCommon::parseWildcard ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, "wildcard" );

	SymbolType wildcard = core::xmlApi < SymbolType >::parse ( input );

	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, "wildcard" );

	return wildcard;
}

} /* namespace string */

