/*
 * FormalRTEEmpty.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Feb 20, 2016
 *      Author: Tomas Pecka
 */

#pragma once

#include "FormalRTEElement.h"
#include <sstream>

namespace rte {

/**
 * \brief Represents the empty expression in the regular tree expression. The node can't have any children.
 *
 * The structure is derived from NullaryNode disallowing adding any child.
 *
 * The node can be visited by the FormalRTEElement < SymbolType >::Visitor
 *
 * \tparam SymbolType used for the symbol part of the ranked symbol
 */
template < class SymbolType >
class FormalRTEEmpty : public ext::NullaryNode < FormalRTEElement < SymbolType > > {
	/**
	 * @copydoc rte::FormalRTEElement < SymbolType >::accept ( ) const
	 */
	void accept ( typename FormalRTEElement < SymbolType >::ConstVisitor & visitor ) const override {
		visitor.visit ( * this );
	}

	/**
	 * @copydoc rte::FormalRTEElement < SymbolType >::accept ( ) const
	 */
	void accept ( typename FormalRTEElement < SymbolType >::Visitor & visitor ) override {
		visitor.visit ( * this );
	}

public:
	/**
	 * \brief Creates a new instance of the empty node.
	 */
	explicit FormalRTEEmpty ( ) = default;

	/**
	 * @copydoc FormalRTEElement < SymbolType >::clone ( ) const &
	 */
	FormalRTEEmpty < SymbolType > * clone ( ) const & override;

	/**
	 * @copydoc FormalRTEElement < SymbolType >::clone ( ) &&
	 */
	FormalRTEEmpty < SymbolType > * clone ( ) && override;

	/**
	 * @copydoc FormalRTEElement < SymbolType >::testSymbol ( const common::ranked_symbol < SymbolType > & ) const
	 */
	bool testSymbol ( const common::ranked_symbol < SymbolType > & symbol ) const override;

	/**
	 * @copydoc FormalRTEElement < SymbolType >::computeMinimalAlphabet ( ext::set < common::ranked_symbol < SymbolType > > &, ext::set < common::ranked_symbol < SymbolType > > & ) const
	 */
	void computeMinimalAlphabet ( ext::set < common::ranked_symbol < SymbolType > > & alphabetF, ext::set < common::ranked_symbol < SymbolType > > & alphabetK ) const override;

	/**
	 * @copydoc FormalRTEElement < SymbolType >::checkAlphabet ( const ext::set < common::ranked_symbol < SymbolType > > &, const ext::set < common::ranked_symbol < SymbolType > > & ) const
	 */
	bool checkAlphabet ( const ext::set < common::ranked_symbol < SymbolType > > & alphabetF, const ext::set < common::ranked_symbol < SymbolType > > & alphabetK ) const override;

	/**
	 * @copydoc FormalRTEElement < SymbolType >::operator <=> ( const FormalRTEElement < SymbolType > & other ) const;
	 */
	std::strong_ordering operator <=> ( const FormalRTEElement < SymbolType > & other ) const override {
		if ( ext::type_index ( typeid ( * this ) ) == ext::type_index ( typeid ( other ) ) ) return * this <=> ( decltype ( * this ) ) other;

		return ext::type_index ( typeid ( * this ) ) <=> ext::type_index ( typeid ( other ) );
	}

	/**
	 * The three way comparison implementation
	 *
	 * \param other the other instance
	 *
	 * \returns the ordering between this object and the @p other.
	 */
	std::strong_ordering operator <=> ( const FormalRTEEmpty < SymbolType > & ) const;

	/**
	 * @copydoc FormalRTEElement < SymbolType >::operator == ( const FormalRTEElement < SymbolType > & other ) const;
	 */
	bool operator == ( const FormalRTEElement < SymbolType > & other ) const override {
		if ( ext::type_index ( typeid ( * this ) ) == ext::type_index ( typeid ( other ) ) ) return * this == ( decltype ( * this ) ) other;

		return false;
	}

	/**
	 * The equality comparison implementation.
	 *
	 * \param other the other object to compare with.
	 *
	 * \returns true if this and other objects are equal, false othervise
	 */
	bool operator == ( const FormalRTEEmpty < SymbolType > & ) const;

	/**
	 * @copydoc base::CommonBase < FormalRTEElement < SymbolType > >::operator >> ( std::ostream & ) const
	 */
	void operator >>( std::ostream & out ) const override;

	/**
	 * @copydoc base::CommonBase < FormalRTEElement < SymbolType > >::operator std::string ( ) const
	 */
	explicit operator std::string ( ) const override;

	/**
	 * @copydoc FormalRTEElement::normalize ( ) &&
	 */
	ext::smart_ptr < FormalRTEElement < DefaultSymbolType > > normalize ( ) && override {
		return ext::smart_ptr < FormalRTEElement < DefaultSymbolType > > ( new FormalRTEEmpty < DefaultSymbolType > ( ) );
	}
};

template < class SymbolType >
FormalRTEEmpty < SymbolType > * FormalRTEEmpty < SymbolType >::clone ( ) const & {
	return new FormalRTEEmpty ( * this );
}

template < class SymbolType >
FormalRTEEmpty < SymbolType > * FormalRTEEmpty < SymbolType >::clone ( ) && {
	return new FormalRTEEmpty ( std::move ( * this ) );
}

template < class SymbolType >
std::strong_ordering FormalRTEEmpty < SymbolType >::operator <=> ( const FormalRTEEmpty < SymbolType > & ) const {
	return std::strong_ordering::equal;
}

template < class SymbolType >
bool FormalRTEEmpty < SymbolType >::operator == ( const FormalRTEEmpty < SymbolType > & ) const {
	return true;
}

template < class SymbolType >
void FormalRTEEmpty < SymbolType >::operator >>( std::ostream & out ) const {
	out << "(FormalRTEEmpty)";
}

template < class SymbolType >
bool FormalRTEEmpty < SymbolType >::testSymbol ( const common::ranked_symbol < SymbolType > & ) const {
	return false;
}

template < class SymbolType >
void FormalRTEEmpty < SymbolType >::computeMinimalAlphabet ( ext::set < common::ranked_symbol < SymbolType > > & /*alphabetF*/, ext::set < common::ranked_symbol < SymbolType > > & /*alphabetK*/ ) const {
}

template < class SymbolType >
bool FormalRTEEmpty < SymbolType >::checkAlphabet ( const ext::set < common::ranked_symbol < SymbolType > > & /* alphabetF */, const ext::set < common::ranked_symbol < SymbolType > > & /* alphabetK */ ) const {
	return true;
}

template < class SymbolType >
FormalRTEEmpty < SymbolType >::operator std::string ( ) const {
	std::stringstream ss;
	ss << * this;
	return ss.str ( );
}

} /* namespace rte */

extern template class rte::FormalRTEEmpty < DefaultSymbolType >;

