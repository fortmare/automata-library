/*
 * FormalRTEStructure.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Oct 20, 2016
 *      Author: Jan Travnicek
 */

#pragma once

#include <rte/formal/FormalRTEStructure.h>
#include <rte/xml/common/RTEFromXmlParser.h>
#include <rte/xml/common/RTEToXmlComposer.h>

namespace core {

/**
 * \brief Specialisation of the xmlApi class for formal regular expression
 *
 * The class provides the usual intarface of the xmlApi including parse and compose
 *
 * \tparam SymbolType used for the symbol part of the ranked symbol
 * \tparam RankType used for the rank part of the ranked symbol
 */
template < class SymbolType >
struct xmlApi < rte::FormalRTEStructure < SymbolType > > {
	/**
	 * Parsing from a sequence of xml tokens helper.
	 *
	 * \params input the iterator to sequence of xml tokens to parse from
	 *
	 * \returns the new instance of the rte
	 */
	static rte::FormalRTEStructure < SymbolType > parse ( ext::deque < sax::Token >::iterator & input );

	/**
	 * Composing to a sequence of xml tokens helper.
	 *
	 * \param output the sink for new xml tokens representing the automaton
	 * \param input the automaton to compose
	 */
	static void compose ( ext::deque < sax::Token > & output, const rte::FormalRTEStructure < SymbolType > & input );
};

template < class SymbolType >
rte::FormalRTEStructure < SymbolType > xmlApi < rte::FormalRTEStructure < SymbolType > >::parse ( ext::deque < sax::Token >::iterator & input ) {
	return rte::FormalRTEStructure < SymbolType > ( rte::RTEFromXmlParser::parseFormalRTEElement < SymbolType > ( input ) );
}

template < class SymbolType >
void xmlApi < rte::FormalRTEStructure < SymbolType > >::compose ( ext::deque < sax::Token > & output, const rte::FormalRTEStructure < SymbolType > & input ) {
	input.getStructure ( ).template accept < void, rte::RTEToXmlComposer::Formal > ( output );
}

} /* namespace core */

