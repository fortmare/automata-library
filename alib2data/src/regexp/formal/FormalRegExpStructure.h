/*
 * FormalRegExpStructure.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Oct 20, 2016
 *      Author: Jan Travnicek
 */

#pragma once

namespace regexp {

template < class SymbolType >
class UnboundedRegExpStructure;

} /* namespace regexp */

#include <exception/CommonException.h>

#include "FormalRegExpEmpty.h"

namespace regexp {

/**
 * \brief Represents formal regular expression structure. Regular expression is stored as a tree of FormalRegExpElements.
 *
 * \tparam SymbolType used for the terminal alphabet
 */
template < class SymbolType >
class FormalRegExpStructure final {
	/**
	 * The root of the regular expression tree
	 */
	ext::smart_ptr < FormalRegExpElement < SymbolType > > m_structure;

public:
	/**
	 * \brief Creates a new instance of the expression structure. Defaultly created structure is empty expression
	 */
	explicit FormalRegExpStructure ( );

	/**
	 * \brief Creates a new instance of the expression structure. The new instance contains structure as provided by the parameter.
	 *
	 * \param structure the expression structure.
	 */
	explicit FormalRegExpStructure ( FormalRegExpElement < SymbolType > && structure );

	/**
	 * \brief Creates a new instance of the expression structure. The new instance contains structure as provided by the parameter.
	 *
	 * \param structure the expression structure.
	 */
	explicit FormalRegExpStructure ( const FormalRegExpElement < SymbolType > & structure );

	/**
	 * \brief Creates a new instance based on unbounded representation. The new instance contains structure converted to formal representation.
	 *
	 * \param other the structure in unbounded representation
	 */
	explicit FormalRegExpStructure ( const UnboundedRegExpStructure < SymbolType > & other );

	/**
	 * Getter of the root of the structure.
	 *
	 * \return Root node of the formal regular expression tree
	 */
	const FormalRegExpElement < SymbolType > & getStructure ( ) const;

	/**
	 * Getter of the root of the structure.
	 *
	 * \return Root node of the formal regular expression tree
	 */
	FormalRegExpElement < SymbolType > & getStructure ( );

	/**
	 * Sets the root node of the expression structure
	 *
	 * \param structure new root node of the structure
	 */
	void setStructure ( FormalRegExpElement < SymbolType > && param );

	/**
	 * Sets the root node of the expression structure
	 *
	 * \param structure new root node of the structure
	 */
	void setStructure ( const FormalRegExpElement < SymbolType > & structure );

	/**
	 * \brief Strucuture printer to the stream
	 *
	 * \param out the output stream
	 * \param structure the structure to print
	 */
	friend std::ostream & operator << ( std::ostream & out, const FormalRegExpStructure < SymbolType > & structure ) {
		out << structure.getStructure ( );
		return out;
	}

	/**
	 * \brief Three way comparison operator implementation for the expression structure.
	 *
	 * \param first the first instance
	 * \param second the second instance
	 *
	 * \return the strong ordering between the the first and the second parameter
	 */
	friend bool operator <=> ( const FormalRegExpStructure < SymbolType > & first, const FormalRegExpStructure < SymbolType > & second ) {
		return first.getStructure() <=> second.getStructure();
	}

	/**
	 * \brief Equality operator implementation for the expression structure.
	 *
	 * \param first the first instance
	 * \param second the second instance
	 *
	 * \return true if the first is equal to the second, false otherwise
	 */
	friend bool operator == ( const FormalRegExpStructure < SymbolType > & first, const FormalRegExpStructure < SymbolType > & second ) {
		return first.getStructure() == second.getStructure();
	}

	/**
	 * \brief Performs the type normalization of the regexp structure.
	 *
	 * \return the normalized structure
	 */
	FormalRegExpStructure < DefaultSymbolType > normalize ( ) && {
		return FormalRegExpStructure < DefaultSymbolType > ( std::move ( * std::move ( getStructure ( ) ).normalize ( ) ) );
	}
};

template < class SymbolType >
FormalRegExpStructure < SymbolType >::FormalRegExpStructure ( FormalRegExpElement < SymbolType > && structure ) : m_structure ( nullptr ) {
	setStructure ( std::move ( structure ) );
}

template < class SymbolType >
FormalRegExpStructure < SymbolType >::FormalRegExpStructure ( const FormalRegExpElement < SymbolType > & structure ) : FormalRegExpStructure ( ext::move_copy ( structure ) ) {
}

template < class SymbolType >
FormalRegExpStructure < SymbolType >::FormalRegExpStructure ( ) : FormalRegExpStructure ( FormalRegExpEmpty < SymbolType > ( ) ) {
}

template < class SymbolType >
FormalRegExpStructure < SymbolType >::FormalRegExpStructure ( const UnboundedRegExpStructure < SymbolType > & other ) : FormalRegExpStructure ( * other.getStructure ( ).asFormal ( ) ) {
}

template < class SymbolType >
const FormalRegExpElement < SymbolType > & FormalRegExpStructure < SymbolType >::getStructure ( ) const {
	return * m_structure;
}

template < class SymbolType >
FormalRegExpElement < SymbolType > & FormalRegExpStructure < SymbolType >::getStructure ( ) {
	return * m_structure;
}

template < class SymbolType >
void FormalRegExpStructure < SymbolType >::setStructure ( const FormalRegExpElement < SymbolType > & structure ) {
	setStructure ( ext::move_copy ( structure ) );
}

template < class SymbolType >
void FormalRegExpStructure < SymbolType >::setStructure ( FormalRegExpElement < SymbolType > && param ) {
	this->m_structure = ext::smart_ptr < FormalRegExpElement < SymbolType > > ( std::move ( param ).clone ( ) );
}

} /* namespace regexp */

extern template class regexp::FormalRegExpStructure < DefaultSymbolType >;

