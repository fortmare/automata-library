/*
 * FormalRegExpSymbol.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Nov 23, 2013
 *      Author: Martin Zak
 */

#pragma once

#include <sstream>

#include "FormalRegExpElement.h"
#include <alphabet/common/SymbolNormalize.h>

namespace regexp {

/**
 * \brief Represents the symbol in the regular expression. The can't have any children.
 *
 * The structure is derived from NullaryNode disallowing adding any child.
 *
 * The node can be visited by the FormalRegExpElement < SymbolType >::Visitor
 *
 * \tparam SymbolType used for the terminal alphabet
 */
template < class SymbolType >
class FormalRegExpSymbol : public ext::NullaryNode < FormalRegExpElement < SymbolType > > {
	/**
	 * The symbol of the node.
	 */
	SymbolType m_symbol;

	/**
	 * @copydoc regexp::FormalRegExpElement < SymbolType >::accept ( ) const
	 */
	void accept ( typename FormalRegExpElement < SymbolType >::Visitor & visitor ) const override {
		visitor.visit ( * this );
	}

public:
	/**
	 * \brief Creates a new instance of the symbol node using the actual symbol to represent.
	 *
	 * \param symbol the value of the represented symbol
	 */
	explicit FormalRegExpSymbol ( SymbolType symbol );

	/**
	 * @copydoc FormalRegExpElement::clone ( ) const &
	 */
	FormalRegExpSymbol < SymbolType > * clone ( ) const & override;

	/**
	 * @copydoc FormalRegExpElement::clone ( ) const &
	 */
	FormalRegExpSymbol < SymbolType > * clone ( ) && override;

	/**
	 * @copydoc FormalRegExpElement::clone ( ) const &
	 */
	ext::smart_ptr < UnboundedRegExpElement < SymbolType > > asUnbounded ( ) const override;

	/**
	 * @copydoc FormalRegExpElement::testSymbol() const
	 */
	bool testSymbol ( const SymbolType & symbol ) const override;

	/**
	 * @copydoc FormalRegExpElement::computeMinimalAlphabet()
	 */
	void computeMinimalAlphabet ( ext::set < SymbolType > & alphabet ) const override;

	/**
	 * @copydoc FormalRegExpElement::checkAlphabet()
	 */
	bool checkAlphabet ( const ext::set < SymbolType > & alphabet ) const override;

	/**
	 * Getter of the symbol.
	 *
	 * \return the symbol
	 */
	const SymbolType & getSymbol ( ) const &;

	/**
	 * Getter of the symbol.
	 *
	 * \return the symbol
	 */
	SymbolType && getSymbol ( ) &&;

	/**
	 * @copydoc FormalRegExpElement < SymbolType >::operator <=> ( const FormalRegExpElement < SymbolType > & other ) const;
	 */
	std::strong_ordering operator <=> ( const FormalRegExpElement < SymbolType > & other ) const override {
		if ( ext::type_index ( typeid ( * this ) ) == ext::type_index ( typeid ( other ) ) ) return * this <=> ( decltype ( * this ) ) other;

		return ext::type_index ( typeid ( * this ) ) <=> ext::type_index ( typeid ( other ) );
	}

	/**
	 * The three way comparison implementation
	 *
	 * \param other the other instance
	 *
	 * \returns the ordering between this object and the @p other.
	 */
	std::strong_ordering operator <=> ( const FormalRegExpSymbol < SymbolType > & ) const;

	/**
	 * @copydoc FormalRegExpElement < SymbolType >::operator == ( const FormalRegExpElement < SymbolType > & other ) const;
	 */
	bool operator == ( const FormalRegExpElement < SymbolType > & other ) const override {
		if ( ext::type_index ( typeid ( * this ) ) == ext::type_index ( typeid ( other ) ) ) return * this == ( decltype ( * this ) ) other;

		return false;
	}

	/**
	 * The equality comparison implementation.
	 *
	 * \param other the other object to compare with.
	 *
	 * \returns true if this and other objects are equal, false othervise
	 */
	bool operator == ( const FormalRegExpSymbol < SymbolType > & ) const;

	/**
	 * @copydoc base::CommonBase < FormalRegExpElement < SymbolType > >::operator >> ( std::ostream & )
	 */
	void operator >>( std::ostream & out ) const override;

	/**
	 * @copydoc base::CommonBase < FormalRegExpElement < SymbolType > >::operator std::string ( )
	 */
	explicit operator std::string ( ) const override;

	/**
	 * @copydoc regexp::FormalRegExpElement < SymbolType >::normalize ( ) &&
	 */
	ext::smart_ptr < FormalRegExpElement < DefaultSymbolType > > normalize ( ) && override {
		return ext::smart_ptr < FormalRegExpElement < DefaultSymbolType > > ( new FormalRegExpSymbol < DefaultSymbolType > ( alphabet::SymbolNormalize::normalizeSymbol ( std::move ( m_symbol ) ) ) );
	}
};

} /* namespace regexp */

#include "../unbounded/UnboundedRegExpSymbol.h"

namespace regexp {

template < class SymbolType >
FormalRegExpSymbol < SymbolType >::FormalRegExpSymbol ( SymbolType symbol ) : m_symbol ( std::move ( symbol ) ) {
}

template < class SymbolType >
FormalRegExpSymbol < SymbolType > * FormalRegExpSymbol < SymbolType >::clone ( ) const & {
	return new FormalRegExpSymbol ( * this );
}

template < class SymbolType >
FormalRegExpSymbol < SymbolType > * FormalRegExpSymbol < SymbolType >::clone ( ) && {
	return new FormalRegExpSymbol ( std::move ( * this ) );
}

template < class SymbolType >
ext::smart_ptr < UnboundedRegExpElement < SymbolType > > FormalRegExpSymbol < SymbolType >::asUnbounded ( ) const {
	return ext::smart_ptr < UnboundedRegExpElement < SymbolType > > ( new UnboundedRegExpSymbol < SymbolType > ( this->m_symbol ) );
}

template < class SymbolType >
std::strong_ordering FormalRegExpSymbol < SymbolType >::operator <=> ( const FormalRegExpSymbol < SymbolType > & other ) const {
	return m_symbol <=> other.m_symbol;
}

template < class SymbolType >
bool FormalRegExpSymbol < SymbolType >::operator == ( const FormalRegExpSymbol < SymbolType > & other ) const {
	return m_symbol == other.m_symbol;
}

template < class SymbolType >
void FormalRegExpSymbol < SymbolType >::operator >>( std::ostream & out ) const {
	out << "(FormalRegExpSymbol " << m_symbol << ")";
}

template < class SymbolType >
bool FormalRegExpSymbol < SymbolType >::testSymbol ( const SymbolType & symbol ) const {
	return symbol == this->m_symbol;
}

template < class SymbolType >
void FormalRegExpSymbol < SymbolType >::computeMinimalAlphabet ( ext::set < SymbolType > & alphabet ) const {
	alphabet.insert ( m_symbol );
}

template < class SymbolType >
bool FormalRegExpSymbol < SymbolType >::checkAlphabet ( const ext::set < SymbolType > & alphabet ) const {
	return alphabet.count ( m_symbol );
}

template < class SymbolType >
const SymbolType & FormalRegExpSymbol < SymbolType >::getSymbol ( ) const & {
	return this->m_symbol;
}

template < class SymbolType >
SymbolType && FormalRegExpSymbol < SymbolType >::getSymbol ( ) && {
	return std::move ( this->m_symbol );
}

template < class SymbolType >
FormalRegExpSymbol < SymbolType >::operator std::string ( ) const {
	std::stringstream ss;
	ss << * this;
	return ss.str ( );
}

} /* namespace regexp */

extern template class regexp::FormalRegExpSymbol < DefaultSymbolType >;

