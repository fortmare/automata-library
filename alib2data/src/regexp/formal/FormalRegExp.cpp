/*
 * FormalRegExp.cpp
 *
 *  Created on: Nov 23, 2013
 *      Author: Martin Zak
 */

#include "FormalRegExp.h"
#include "FormalRegExpElements.h"

#include <registration/CastRegistration.hpp>
#include <registration/ValuePrinterRegistration.hpp>

template class regexp::FormalRegExp < >;
template class regexp::FormalRegExpStructure < DefaultSymbolType >;
template class regexp::FormalRegExpElement < DefaultSymbolType >;
template class regexp::FormalRegExpAlternation < DefaultSymbolType >;
template class regexp::FormalRegExpConcatenation < DefaultSymbolType >;
template class regexp::FormalRegExpIteration < DefaultSymbolType >;
template class regexp::FormalRegExpEpsilon < DefaultSymbolType >;
template class regexp::FormalRegExpEmpty < DefaultSymbolType >;
template class regexp::FormalRegExpSymbol < DefaultSymbolType >;

namespace {

auto formalRegExpFromUnboundedRegExp = registration::CastRegister < regexp::FormalRegExp < >, regexp::UnboundedRegExp < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < regexp::FormalRegExp < > > ( );

} /* namespace */
