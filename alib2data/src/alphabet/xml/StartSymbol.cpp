/*
 * StartSymbol.cpp
 *
 *  Created on: Jun 19, 2014
 *      Author: Jan Travnicek
 */

#include "StartSymbol.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace core {

alphabet::StartSymbol xmlApi < alphabet::StartSymbol >::parse ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, xmlTagName ( ) );
	return alphabet::StartSymbol ( );
}

bool xmlApi < alphabet::StartSymbol >::first ( const ext::deque < sax::Token >::const_iterator & input ) {
	return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
}

std::string xmlApi < alphabet::StartSymbol >::xmlTagName ( ) {
	return "StartSymbol";
}

void xmlApi < alphabet::StartSymbol >::compose ( ext::deque < sax::Token > & output, const alphabet::StartSymbol & ) {
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::START_ELEMENT );
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::END_ELEMENT );
}

} /* namespace core */

namespace {

auto xmlWrite = registration::XmlWriterRegister < alphabet::StartSymbol > ( );
auto xmlRead = registration::XmlReaderRegister < alphabet::StartSymbol > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, alphabet::StartSymbol > ( );

} /* namespace */
