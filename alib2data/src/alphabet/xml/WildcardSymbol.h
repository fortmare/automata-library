/*
 * WildcardSymbol.h
 *
 *  Created on: Apr 10, 2013
 *      Author: Jan Travnicek
 */

#pragma once

#include <alphabet/WildcardSymbol.h>
#include <core/xmlApi.hpp>

namespace core {

template < >
struct xmlApi < alphabet::WildcardSymbol > {
	static alphabet::WildcardSymbol parse ( ext::deque < sax::Token >::iterator & input );
	static bool first ( const ext::deque < sax::Token >::const_iterator & input );
	static std::string xmlTagName ( );
	static void compose ( ext::deque < sax::Token > & output, const alphabet::WildcardSymbol & data );
};

} /* namespace core */

