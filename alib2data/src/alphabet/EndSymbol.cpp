/*
 * EndSymbol.cpp
 *
 *  Created on: Jun 19, 2014
 *      Author: Jan Travnicek
 */

#include "EndSymbol.h"

#include <object/Object.h>

#include <registration/ValuePrinterRegistration.hpp>

namespace alphabet {

EndSymbol::EndSymbol() = default;

std::ostream & operator << ( std::ostream & out, const EndSymbol & ) {
	return out << "(EndSymbol)";
}

EndSymbol::operator std::string ( ) const {
	return EndSymbol::instance < std::string > ( );
}

} /* namespace alphabet */

namespace {

auto valuePrinter = registration::ValuePrinterRegister < alphabet::EndSymbol > ( );

} /* namespace */
