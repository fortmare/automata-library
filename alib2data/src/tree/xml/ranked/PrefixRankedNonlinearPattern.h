/*
 * PrefixRankedNonlinearPattern.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Jul 20, 2016
 *      Author: Jan Travnicek
 */

#pragma once

#include <tree/ranked/PrefixRankedNonlinearPattern.h>
#include <sax/FromXMLParserHelper.h>
#include <alphabet/xml/RankedSymbol.h>
#include "../common/TreeFromXMLParser.h"
#include "../common/TreeToXMLComposer.h"

namespace core {

template < class SymbolType >
struct xmlApi < tree::PrefixRankedNonlinearPattern < SymbolType > > {
	/**
	 * \brief The XML tag name of class.
	 *
	 * \details Intentionaly a static member function to be safe in the initialisation before the main function starts.
	 *
	 * \returns string representing the XML tag name of the class
	 */
	static std::string xmlTagName ( ) {
		return "PrefixRankedNonlinearPattern";
	}

	/**
	 * \brief Tests whether the token stream starts with this type
	 *
	 * \params input the iterator to sequence of xml tokens to test
	 *
	 * \returns true if the token stream iterator points to opening tag named with xml tag name of this type, false otherwise.
	 */
	static bool first ( const ext::deque < sax::Token >::const_iterator & input ) {
		return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
	}

	/**
	 * Parsing from a sequence of xml tokens helper.
	 *
	 * \params input the iterator to sequence of xml tokens to parse from
	 *
	 * \returns the new instance of the pattern
	 */
	static tree::PrefixRankedNonlinearPattern < SymbolType > parse ( ext::deque < sax::Token >::iterator & input );

	/**
	 * Composing to a sequence of xml tokens helper.
	 *
	 * \param out the sink for new xml tokens representing the pattern
	 * \param pattern the pattern to compose
	 */
	static void compose ( ext::deque < sax::Token > & out, const tree::PrefixRankedNonlinearPattern < SymbolType > & pattern );
};

template < class SymbolType >
tree::PrefixRankedNonlinearPattern < SymbolType > xmlApi < tree::PrefixRankedNonlinearPattern < SymbolType > >::parse ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
	common::ranked_symbol < SymbolType > subtreeWildcardSymbol = tree::TreeFromXMLParser::parseWildcardSymbol < common::ranked_symbol < SymbolType > > ( input );
	ext::set < common::ranked_symbol < SymbolType > > nonlinearVariables = tree::TreeFromXMLParser::parseNonlinearVariables < common::ranked_symbol < SymbolType > > ( input );
	ext::set < common::ranked_symbol < SymbolType > > rankedAlphabet = tree::TreeFromXMLParser::parseAlphabet < common::ranked_symbol < SymbolType > > ( input );
	ext::vector < common::ranked_symbol < SymbolType > > data = tree::TreeFromXMLParser::parseLinearContent < common::ranked_symbol < SymbolType > > ( input );
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, xmlTagName ( ) );

	return tree::PrefixRankedNonlinearPattern < SymbolType > ( std::move ( subtreeWildcardSymbol ), std::move ( nonlinearVariables ), std::move ( rankedAlphabet ), std::move ( data ) );
}

template < class SymbolType >
void xmlApi < tree::PrefixRankedNonlinearPattern < SymbolType > >::compose ( ext::deque < sax::Token > & out, const tree::PrefixRankedNonlinearPattern < SymbolType > & pattern ) {
	out.emplace_back ( xmlTagName ( ), sax::Token::TokenType::START_ELEMENT );
	tree::TreeToXMLComposer::composeSubtreeWildcard ( out, pattern.getSubtreeWildcard ( ) );
	tree::TreeToXMLComposer::composeNonlinearVariables ( out, pattern.getNonlinearVariables ( ) );
	tree::TreeToXMLComposer::composeAlphabet ( out, pattern.getAlphabet ( ) );
	tree::TreeToXMLComposer::composeContent ( out, pattern.getContent ( ) );
	out.emplace_back ( xmlTagName ( ), sax::Token::TokenType::END_ELEMENT );
}

} /* namespace core */

