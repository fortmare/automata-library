/*
 * PrefixRankedBarTree.cpp
 *
 *  Created on: Nov 23, 2013
 *      Author: Jan Travnicek
 */

#include "PrefixRankedBarTree.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < tree::PrefixRankedBarTree < > > ( );
auto xmlRead = registration::XmlReaderRegister < tree::PrefixRankedBarTree < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, tree::PrefixRankedBarTree < > > ( );

} /* namespace */
