/*
 * PrefixBarTree.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Nov 23, 2013
 *      Author: Jan Travnicek
 */

#pragma once

#include <tree/unranked/PrefixBarTree.h>
#include <sax/FromXMLParserHelper.h>
#include "../common/TreeFromXMLParser.h"
#include "../common/TreeToXMLComposer.h"

namespace core {

template < class SymbolType >
struct xmlApi < tree::PrefixBarTree < SymbolType > > {
	/**
	 * \brief The XML tag name of class.
	 *
	 * \details Intentionaly a static member function to be safe in the initialisation before the main function starts.
	 *
	 * \returns string representing the XML tag name of the class
	 */
	static std::string xmlTagName() {
		return "PrefixBarTree";
	}

	/**
	 * \brief Tests whether the token stream starts with this type
	 *
	 * \params input the iterator to sequence of xml tokens to test
	 *
	 * \returns true if the token stream iterator points to opening tag named with xml tag name of this type, false otherwise.
	 */
	static bool first ( const ext::deque < sax::Token >::const_iterator & input ) {
		return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
	}

	/**
	 * Parsing from a sequence of xml tokens helper.
	 *
	 * \params input the iterator to sequence of xml tokens to parse from
	 *
	 * \returns the new instance of the tree
	 */
	static tree::PrefixBarTree < SymbolType > parse ( ext::deque < sax::Token >::iterator & input );

	/**
	 * Composing to a sequence of xml tokens helper.
	 *
	 * \param out the sink for new xml tokens representing the tree
	 * \param tree the tree to compose
	 */
	static void compose ( ext::deque < sax::Token > & out, const tree::PrefixBarTree < SymbolType > & tree );
};

template < class SymbolType >
tree::PrefixBarTree < SymbolType > xmlApi < tree::PrefixBarTree < SymbolType > >::parse ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
	SymbolType bar = tree::TreeFromXMLParser::parseBar < SymbolType > ( input );
	ext::set < SymbolType > unrankedAlphabet = tree::TreeFromXMLParser::parseAlphabet < SymbolType > ( input );
	ext::vector < SymbolType > data = tree::TreeFromXMLParser::parseLinearContent < SymbolType > ( input );
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, xmlTagName ( ) );

	return tree::PrefixBarTree < SymbolType > ( std::move ( bar ), std::move ( unrankedAlphabet ), std::move ( data ) );
}

template < class SymbolType >
void xmlApi < tree::PrefixBarTree < SymbolType > >::compose ( ext::deque < sax::Token > & out, const tree::PrefixBarTree < SymbolType > & tree ) {
	out.emplace_back ( xmlTagName ( ), sax::Token::TokenType::START_ELEMENT );
	tree::TreeToXMLComposer::composeBar ( out, tree.getBar ( ) );
	tree::TreeToXMLComposer::composeAlphabet ( out, tree.getAlphabet ( ) );
	tree::TreeToXMLComposer::composeContent ( out, tree.getContent ( ) );
	out.emplace_back ( xmlTagName ( ), sax::Token::TokenType::END_ELEMENT );
}

} /* namespace core */

