/*
 * UnrankedPattern.cpp
 *
 *  Created on: Nov 23, 2013
 *      Author: Jan Travnicek
 */

#include "UnrankedPattern.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < tree::UnrankedPattern < > > ( );
auto xmlRead = registration::XmlReaderRegister < tree::UnrankedPattern < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, tree::UnrankedPattern < > > ( );

} /* namespace */
