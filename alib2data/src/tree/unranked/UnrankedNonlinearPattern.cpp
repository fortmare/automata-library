/*
 * UnrankedNonlinearPattern.cpp
 *
 *  Created on: Nov 23, 2013
 *      Author: Jan Travnicek
 */

#include "UnrankedNonlinearPattern.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>

template class tree::UnrankedNonlinearPattern < >;

namespace {

auto components = registration::ComponentRegister < tree::UnrankedNonlinearPattern < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < tree::UnrankedNonlinearPattern < > > ( );

} /* namespace */
