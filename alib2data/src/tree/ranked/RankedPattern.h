/*
 * RankedPattern.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Nov 23, 2013
 *      Author: Jan Travnicek
 */

#pragma once

#include <common/DefaultSymbolType.h>

namespace tree {

template < class SymbolType = DefaultSymbolType >
class RankedPattern;

} /* namespace tree */

#include <sstream>

#include <alib/string>
#include <alib/set>
#include <alib/tree>
#include <alib/iostream>
#include <alib/algorithm>

#include <core/components.hpp>
#include <common/ranked_symbol.hpp>

#include <tree/TreeException.h>
#include <tree/common/TreeAuxiliary.h>

#include <core/normalize.hpp>
#include <tree/common/TreeNormalize.h>

#include "../unranked/UnrankedPattern.h"

namespace tree {

class GeneralAlphabet;
class SubtreeWildcard;

/**
 * \brief
 * Tree pattern represented in its natural representation. The representation is so called ranked, therefore it consists of ranked symbols. The rank of the ranked symbol needs to be compatible with unsigned integer. Additionally the pattern contains a special wildcard symbol representing any subtree.
 *
 * \details
 * T = (A, C, W \in ( A \minus B ) ),
 * A (Alphabet) = finite set of ranked symbols,
 * C (Content) = pattern in its natural representation
 * W (Wildcard) = special symbol representing any subtree
 *
 * \tparam SymbolType used for the symbol part of the ranked symbol
 */
template < class SymbolType >
class RankedPattern final : public core::Components < RankedPattern < SymbolType >, ext::set < common::ranked_symbol < SymbolType > >, component::Set, GeneralAlphabet, common::ranked_symbol < SymbolType >, component::Value, SubtreeWildcard > {
	/**
	 * Natural representation of the pattern content.
	 */
	ext::tree < common::ranked_symbol < SymbolType > > m_content;

	/**
	 * Checks that symbols of the pattern are present in the alphabet.
	 *
	 * \throws TreeException when some symbols of the pattern representation are not present in the alphabet
	 *
	 * \param data the pattern in its natural representation
	 */
	void checkAlphabet ( const ext::tree < common::ranked_symbol < SymbolType > > & data ) const;

	/**
	 * Checks that the rank of each symbol of a pattern node corresponds to the number of child nodes of that same node.
	 *
	 * \throws TreeException when some nodes of a pattern have different number of children than the rank of their label states
	 *
	 * \param data the pattern in its natural representation
	 */
	void checkArities ( const ext::tree < common::ranked_symbol < SymbolType > > & data ) const;

public:
	/**
	 * \brief Creates a new instance of the pattern with concrete alphabet, content, and wildcard.
	 *
	 * \param subtreeWildcard the wildcard symbol
	 * \param alphabet the initial alphabet of the pattern
	 * \param pattern the initial content in it's natural representation
	 */
	explicit RankedPattern ( common::ranked_symbol < SymbolType > subtreeWildcard, ext::set < common::ranked_symbol < SymbolType > > alphabet, ext::tree < common::ranked_symbol < SymbolType > > pattern );

	/**
	 * \brief Creates a new instance of the pattern with concrete content and wildcard. The alphabet is deduced from the content.
	 *
	 * \param subtreeWildcard the wildcard symbol
	 * \param pattern the initial content in it's natural representation
	 */
	explicit RankedPattern ( common::ranked_symbol < SymbolType > subtreeWildcard, ext::tree < common::ranked_symbol < SymbolType > > pattern );

	/**
	 * \brief Creates a new instance of the pattern based on UnrankedPattern, the alphabet is created from the content of the UnrankedPattern. The alphabet is created since ranks cannot be deduced.
	 *
	 * \param other the pattern represented as UnrankedNonlinearPattern
	 */
	explicit RankedPattern ( const UnrankedPattern < SymbolType > & other );

	/**
	 * Getter of the alphabet.
	 *
	 * \returns the alphabet of the pattern
	 */
	const ext::set < common::ranked_symbol < SymbolType > > & getAlphabet ( ) const & {
		return this->template accessComponent < GeneralAlphabet > ( ).get ( );
	}

	/**
	 * Getter of the alphabet.
	 *
	 * \returns the alphabet of the pattern
	 */
	ext::set < common::ranked_symbol < SymbolType > > && getAlphabet ( ) && {
		return std::move ( this->template accessComponent < GeneralAlphabet > ( ).get ( ) );
	}

	/**
	 * Adder of an alphabet symbols.
	 *
	 * \param symbols the new symbols to be added to the alphabet
	 */
	void extendAlphabet ( const ext::set < common::ranked_symbol < SymbolType > > & symbols ) {
		this->template accessComponent < GeneralAlphabet > ( ).add ( symbols );
	}

	/**
	 * Getter of the wildcard symbol.
	 *
	 * \returns the wildcard symbol of the pattern
	 */
	const common::ranked_symbol < SymbolType > & getSubtreeWildcard ( ) const & {
		return this->template accessComponent < SubtreeWildcard > ( ).get ( );
	}

	/**
	 * Getter of the wildcard symbol.
	 *
	 * \returns the wildcard symbol of the pattern
	 */
	common::ranked_symbol < SymbolType > && getSubtreeWildcard ( ) && {
		return std::move ( this->template accessComponent < SubtreeWildcard > ( ).get ( ) );
	}

	/**
	 * Getter of the pattern representation.
	 *
	 * \return the natural representation of the pattern.
	 */
	const ext::tree < common::ranked_symbol < SymbolType > > & getContent ( ) const &;

	/**
	 * Getter of the pattern representation.
	 *
	 * \return the natural representation of the pattern.
	 */
	ext::tree < common::ranked_symbol < SymbolType > > && getContent ( ) &&;

	/**
	 * Setter of the representation of the pattern.
	 *
	 * \throws TreeException in same situations as checkAlphabet and checkArities
	 *
	 * \param pattern new representation of the pattern.
	 */
	void setTree ( ext::tree < common::ranked_symbol < SymbolType > > pattern );

	/**
	 * The three way comparison implementation
	 *
	 * \param other the other instance
	 *
	 * \returns the ordering between this object and the @p other.
	 */
	auto operator <=> ( const RankedPattern & other ) const {
		return std::tie ( m_content, getAlphabet(), getSubtreeWildcard() ) <=> std::tie ( other.m_content, other.getAlphabet(), other.getSubtreeWildcard() );
	}

	/**
	 * The equality comparison implementation.
	 *
	 * \param other the other object to compare with.
	 *
	 * \returns true if this and other objects are equal, false othervise
	 */
	bool operator == ( const RankedPattern & other ) const {
		return std::tie ( m_content, getAlphabet(), getSubtreeWildcard() ) == std::tie ( other.m_content, other.getAlphabet(), other.getSubtreeWildcard() );
	}

	/**
	 * Print this object as raw representation to ostream.
	 *
	 * \param out ostream where to print
	 * \param instance object to print
	 *
	 * \returns modified output stream
	 */
	friend std::ostream & operator << ( std::ostream & out, const RankedPattern & instance ) {
		out << "(RankedPattern ";
		out << "alphabet = " << instance.getAlphabet ( );
		out << "content = " << instance.getContent ( );
		out << "subtreeWildcard = " << instance.getSubtreeWildcard ( );
		out << ")";
		return out;
	}

	/**
	 * Casts this instance to as compact as possible string representation.
	 *
	 * \returns string representation of the object
	 */
	explicit operator std::string ( ) const;

	/**
	 * Nice printer of the tree natural representation
	 *
	 * \param os the output stream to print to
	 */
	void nicePrint ( std::ostream & os ) const;
};

template < class SymbolType >
RankedPattern < SymbolType >::RankedPattern ( common::ranked_symbol < SymbolType > subtreeWildcard, ext::set < common::ranked_symbol < SymbolType > > alphabet, ext::tree < common::ranked_symbol < SymbolType > > pattern ) : core::Components < RankedPattern, ext::set < common::ranked_symbol < SymbolType > >, component::Set, GeneralAlphabet, common::ranked_symbol < SymbolType >, component::Value, SubtreeWildcard > ( std::move ( alphabet ), std::move ( subtreeWildcard ) ), m_content ( std::move ( pattern ) ) {
	checkAlphabet ( m_content );
	checkArities ( m_content );
}

template < class SymbolType >
RankedPattern < SymbolType >::RankedPattern ( common::ranked_symbol < SymbolType > subtreeWildcard, ext::tree < common::ranked_symbol < SymbolType > > pattern ) : RankedPattern ( subtreeWildcard, ext::set < common::ranked_symbol < SymbolType > > ( pattern.prefix_begin ( ), pattern.prefix_end ( ) ) + ext::set < common::ranked_symbol < SymbolType > > { subtreeWildcard }, pattern ) {
}

template < class SymbolType >
RankedPattern < SymbolType >::RankedPattern ( const UnrankedPattern < SymbolType > & other ) : RankedPattern ( common::ranked_symbol < SymbolType > ( other.getSubtreeWildcard ( ), 0u ), TreeAuxiliary::unrankedToRanked < SymbolType > ( other.getContent ( ) ) ) {
}

template < class SymbolType >
const ext::tree < common::ranked_symbol < SymbolType > > & RankedPattern < SymbolType >::getContent ( ) const & {
	return m_content;
}

template < class SymbolType >
ext::tree < common::ranked_symbol < SymbolType > > && RankedPattern < SymbolType >::getContent ( ) && {
	return std::move ( m_content );
}

template < class SymbolType >
void RankedPattern < SymbolType >::checkAlphabet ( const ext::tree < common::ranked_symbol < SymbolType > > & data ) const {
	ext::set < common::ranked_symbol < SymbolType > > minimalAlphabet ( data.prefix_begin ( ), data.prefix_end ( ) );
	ext::set < common::ranked_symbol < SymbolType > > unknownSymbols;
	std::set_difference ( minimalAlphabet.begin ( ), minimalAlphabet.end ( ), getAlphabet().begin ( ), getAlphabet().end ( ), std::inserter ( unknownSymbols, unknownSymbols.end ( ) ) );

	if ( !unknownSymbols.empty ( ) )
		throw exception::CommonException ( "Input symbols not in the alphabet." );
}

template < class SymbolType >
void RankedPattern < SymbolType >::checkArities ( const ext::tree < common::ranked_symbol < SymbolType > > & data ) const {
	if ( ( size_t ) data.getData ( ).getRank ( ) != data.getChildren ( ).size ( ) )
		throw exception::CommonException ( "Invalid rank." );

	for ( const ext::tree < common::ranked_symbol < SymbolType > > & child : data )
		checkArities ( child );
}

template < class SymbolType >
void RankedPattern < SymbolType >::setTree ( ext::tree < common::ranked_symbol < SymbolType > > pattern ) {
	checkAlphabet ( pattern );
	checkArities ( pattern );

	this->m_content = std::move ( pattern );
}

template < class SymbolType >
void RankedPattern < SymbolType >::nicePrint ( std::ostream & os ) const {
	m_content.nicePrint ( os );
}

template < class SymbolType >
RankedPattern < SymbolType >::operator std::string ( ) const {
	std::stringstream ss;
	ss << * this;
	return ss.str ( );
}

} /* namespace tree */

namespace core {

/**
 * Helper class specifying constraints for the pattern's internal alphabet component.
 *
 * \tparam SymbolType used for the symbol part of the ranked symbols of the alphabet of the pattern.
 */
template < class SymbolType >
class SetConstraint< tree::RankedPattern < SymbolType >, common::ranked_symbol < SymbolType >, tree::GeneralAlphabet > {
public:
	/**
	 * Returns true if the symbol is still used in the pattern.
	 *
	 * \param pattern the tested pattern
	 * \param symbol the tested symbol
	 *
	 * \returns true if the symbol is used, false othervise
	 */
	static bool used ( const tree::RankedPattern < SymbolType > & pattern, const common::ranked_symbol < SymbolType > & symbol ) {
		const ext::tree < common::ranked_symbol < SymbolType > > & m_content = pattern.getContent ( );
		return std::find(m_content.prefix_begin(), m_content.prefix_end(), symbol) != m_content.prefix_end() || pattern.template accessComponent < tree::SubtreeWildcard > ( ).get ( ) == symbol;
	}

	/**
	 * Returns true as all symbols are possibly available to be in an alphabet.
	 *
	 * \param pattern the tested pattern
	 * \param symbol the tested symbol
	 *
	 * \returns true
	 */
	static bool available ( const tree::RankedPattern < SymbolType > &, const common::ranked_symbol < SymbolType > & ) {
		return true;
	}

	/**
	 * All symbols are valid as symbols of an alphabet.
	 *
	 * \param pattern the tested pattern
	 * \param symbol the tested symbol
	 */
	static void valid ( const tree::RankedPattern < SymbolType > &, const common::ranked_symbol < SymbolType > & ) {
	}
};

/**
 * Helper class specifying constraints for the pattern's internal subtree wildcard element.
 *
 * \tparam SymbolType used for the symbol part of the ranked symbols of the alphabet of the pattern.
 */
template < class SymbolType >
class ElementConstraint< tree::RankedPattern < SymbolType >, common::ranked_symbol < SymbolType >, tree::SubtreeWildcard > {
public:
	/**
	 * Determines whether the symbol is available in the pattern's alphabet.
	 *
	 * \param pattern the tested pattern
	 * \param symbol the tested symbol
	 *
	 * \returns true if the symbol is already in the alphabet of the pattern
	 */
	static bool available ( const tree::RankedPattern < SymbolType > & pattern, const common::ranked_symbol < SymbolType > & symbol ) {
		return pattern.template accessComponent < tree::GeneralAlphabet > ( ).get ( ).count ( symbol );
	}

	/**
	 * Subtree wildcard needs to have zero arity and it needs to be different from any nonlinear variable of the tree.
	 *
	 * \param pattern the tested pattern
	 * \param symbol the tested symbol
	 *
	 * \throws TreeException if the symbol does not have zero arity
	 */
	static void valid ( const tree::RankedPattern < SymbolType > &, const common::ranked_symbol < SymbolType > & symbol) {
		if( ( unsigned ) symbol.getRank() != 0 )
			throw tree::TreeException ( "SubtreeWildcard symbol has nonzero arity" );
	}
};

/**
 * Helper for normalisation of types specified by templates used as internal datatypes of symbols.
 *
 * \returns new instance of the tree with default template parameters or unmodified instance if the template parameters were already default ones
 */
template < class SymbolType >
struct normalize < tree::RankedPattern < SymbolType > > {
	static tree::RankedPattern < > eval ( tree::RankedPattern < SymbolType > && value ) {
		common::ranked_symbol < DefaultSymbolType > wildcard = alphabet::SymbolNormalize::normalizeRankedSymbol ( std::move ( value ).getSubtreeWildcard ( ) );
		ext::set < common::ranked_symbol < DefaultSymbolType > > alphabet = alphabet::SymbolNormalize::normalizeRankedAlphabet ( std::move ( value ).getAlphabet ( ) );
		ext::tree < common::ranked_symbol < DefaultSymbolType > > content = tree::TreeNormalize::normalizeRankedTree ( std::move ( value ).getContent ( ) );

		return tree::RankedPattern < > ( std::move ( wildcard ), std::move ( alphabet ), std::move ( content ) );
	}
};

} /* namespace core */

extern template class tree::RankedPattern < >;

