/*
 * FailStateLabel.h
 *
 *  Created on: Apr 10, 2013
 *      Author: Jan Travnicek
 */

#include "FailStateLabel.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace core {

label::FailStateLabel xmlApi < label::FailStateLabel >::parse ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, xmlTagName ( ) );
	return label::FailStateLabel ( );
}

bool xmlApi < label::FailStateLabel >::first ( const ext::deque < sax::Token >::const_iterator & input ) {
	return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
}

std::string xmlApi < label::FailStateLabel >::xmlTagName ( ) {
	return "FailStateLabel";
}

void xmlApi < label::FailStateLabel >::compose ( ext::deque < sax::Token > & output, const label::FailStateLabel & ) {
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::START_ELEMENT );
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::END_ELEMENT );
}

} /* namespace core */

namespace {

auto xmlWrite = registration::XmlWriterRegister < label::FailStateLabel > ( );
auto xmlRead = registration::XmlReaderRegister < label::FailStateLabel > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, label::FailStateLabel > ( );

} /* namespace */
