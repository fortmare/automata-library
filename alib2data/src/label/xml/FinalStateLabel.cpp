/*
 * FinalStateLabel.h
 *
 *  Created on: Apr 10, 2013
 *      Author: Jan Travnicek
 */

#include "FinalStateLabel.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace core {

label::FinalStateLabel xmlApi < label::FinalStateLabel >::parse ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, xmlTagName ( ) );
	return label::FinalStateLabel ( );
}

bool xmlApi < label::FinalStateLabel >::first ( const ext::deque < sax::Token >::const_iterator & input ) {
	return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
}

std::string xmlApi < label::FinalStateLabel >::xmlTagName ( ) {
	return "FinalStateLabel";
}

void xmlApi < label::FinalStateLabel >::compose ( ext::deque < sax::Token > & output, const label::FinalStateLabel & ) {
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::START_ELEMENT );
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::END_ELEMENT );
}

} /* namespace core */

namespace {

auto xmlWrite = registration::XmlWriterRegister < label::FinalStateLabel > ( );
auto xmlRead = registration::XmlReaderRegister < label::FinalStateLabel > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, label::FinalStateLabel > ( );

} /* namespace */
