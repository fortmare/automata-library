/*
 * InitialStateLabel.h
 *
 *  Created on: Mar 26, 2013
 *      Author: Jan Travnicek
 */

#pragma once

#include <label/InitialStateLabel.h>
#include <core/xmlApi.hpp>

namespace core {

template < >
struct xmlApi < label::InitialStateLabel > {
	static label::InitialStateLabel parse ( ext::deque < sax::Token >::iterator & input );
	static bool first ( const ext::deque < sax::Token >::const_iterator & input );
	static std::string xmlTagName ( );
	static void compose ( ext::deque < sax::Token > & output, const label::InitialStateLabel & data );
};

} /* namespace core */

