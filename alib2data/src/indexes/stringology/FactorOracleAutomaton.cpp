/*
 * FactorOracleAutomaton.cpp
 *
 *  Created on: Jan 8, 2017
 *      Author: Jan Travnicek
 */

#include "FactorOracleAutomaton.h"

#include <registration/CastRegistration.hpp>
#include <registration/ValuePrinterRegistration.hpp>

namespace {

auto DFAFromOracleAutomaton = registration::CastRegister < automaton::DFA < DefaultSymbolType, unsigned >, indexes::stringology::FactorOracleAutomaton < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < indexes::stringology::FactorOracleAutomaton < > > ( );

} /* namespace */
