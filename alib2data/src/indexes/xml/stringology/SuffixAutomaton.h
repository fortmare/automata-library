/*
 * SuffixAutomaton.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Jan 8, 2017
 *      Author: Jan Travnicek
 */

#pragma once

#include <indexes/stringology/SuffixAutomaton.h>

#include <sax/FromXMLParserHelper.h>
#include <core/xmlApi.hpp>

#include <primitive/xml/Unsigned.h>

#include <automaton/xml/FSM/DFA.h>

namespace core {

template < class SymbolType >
struct xmlApi < indexes::stringology::SuffixAutomaton < SymbolType > > {
	static indexes::stringology::SuffixAutomaton < SymbolType > parse ( ext::deque < sax::Token >::iterator & input );
	static bool first ( const ext::deque < sax::Token >::const_iterator & input );
	static std::string xmlTagName ( );
	static void compose ( ext::deque < sax::Token > & output, const indexes::stringology::SuffixAutomaton < SymbolType > & index );
};

template < class SymbolType >
indexes::stringology::SuffixAutomaton < SymbolType > xmlApi < indexes::stringology::SuffixAutomaton < SymbolType > >::parse ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
	automaton::DFA < SymbolType, unsigned > automaton = core::xmlApi < automaton::DFA < SymbolType, unsigned > >::parse ( input );
	unsigned backboneLength = core::xmlApi < unsigned >::parse ( input );
	indexes::stringology::SuffixAutomaton < SymbolType > res ( std::move ( automaton ), backboneLength );

	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, xmlTagName ( ) );
	return res;
}

template < class SymbolType >
bool xmlApi < indexes::stringology::SuffixAutomaton < SymbolType > >::first ( const ext::deque < sax::Token >::const_iterator & input ) {
	return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
}

template < class SymbolType >
std::string xmlApi < indexes::stringology::SuffixAutomaton < SymbolType > >::xmlTagName ( ) {
	return "SuffixAutomaton";
}

template < class SymbolType >
void xmlApi < indexes::stringology::SuffixAutomaton < SymbolType > >::compose ( ext::deque < sax::Token > & output, const indexes::stringology::SuffixAutomaton < SymbolType > & index ) {
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::START_ELEMENT );
	core::xmlApi < automaton::DFA < SymbolType, unsigned > >::compose ( output, index.getAutomaton ( ) );
	core::xmlApi < unsigned >::compose ( output, index.getBackboneLength ( ) );
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::END_ELEMENT );
}

} /* namespace core */

