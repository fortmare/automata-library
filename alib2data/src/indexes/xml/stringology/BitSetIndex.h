/*
 * BitSetIndex.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Jan 8, 2017
 *      Author: Jan Travnicek
 */

#pragma once

#include <indexes/stringology/BitSetIndex.h>

#include <sax/FromXMLParserHelper.h>
#include <core/xmlApi.hpp>

#include <primitive/xml/Bool.h>
#include <container/xml/ObjectsMap.h>
#include <container/xml/ObjectsBitset.h>

#include <string/xml/LinearString.h>

namespace core {

template < class SymbolType, size_t BitmaskBitCount >
struct xmlApi < indexes::stringology::BitSetIndex < SymbolType, BitmaskBitCount > > {
	static indexes::stringology::BitSetIndex < SymbolType, BitmaskBitCount > parse ( ext::deque < sax::Token >::iterator & input );
	static bool first ( const ext::deque < sax::Token >::const_iterator & input );
	static std::string xmlTagName ( );
	static void compose ( ext::deque < sax::Token > & output, const indexes::stringology::BitSetIndex < SymbolType, BitmaskBitCount > & index );
};

template < class SymbolType, size_t BitmaskBitCount >
indexes::stringology::BitSetIndex < SymbolType, BitmaskBitCount > xmlApi < indexes::stringology::BitSetIndex < SymbolType, BitmaskBitCount > >::parse ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
	ext::map < SymbolType, ext::bitset < BitmaskBitCount > > data = core::xmlApi < ext::map < SymbolType, ext::bitset < BitmaskBitCount > > >::parse ( input );
	string::LinearString < SymbolType > string = core::xmlApi < string::LinearString < SymbolType > >::parse ( input );
	indexes::stringology::BitSetIndex < SymbolType, BitmaskBitCount > res ( std::move ( data ), std::move ( string ) );

	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, xmlTagName ( ) );
	return res;
}

template < class SymbolType, size_t BitmaskBitCount >
bool xmlApi < indexes::stringology::BitSetIndex < SymbolType, BitmaskBitCount > >::first ( const ext::deque < sax::Token >::const_iterator & input ) {
	return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
}

template < class SymbolType, size_t BitmaskBitCount >
std::string xmlApi < indexes::stringology::BitSetIndex < SymbolType, BitmaskBitCount > >::xmlTagName ( ) {
	return "BitSetIndex";
}

template < class SymbolType, size_t BitmaskBitCount >
void xmlApi < indexes::stringology::BitSetIndex < SymbolType, BitmaskBitCount > >::compose ( ext::deque < sax::Token > & output, const indexes::stringology::BitSetIndex < SymbolType, BitmaskBitCount > & index ) {
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::START_ELEMENT );
	core::xmlApi < ext::map < SymbolType, ext::bitset < BitmaskBitCount > > >::compose ( output, index.getData ( ) );
	core::xmlApi < string::LinearString < SymbolType > >::compose ( output, index.getString ( ) );
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::END_ELEMENT );
}

} /* namespace core */

