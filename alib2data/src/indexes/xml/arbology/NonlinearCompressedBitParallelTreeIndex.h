/*
 * NonlinearCompressedBitParallelTreeIndex.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: 22. 8. 2017
 *      Author: Jan Travnicek
 */

#pragma once

#include <indexes/arbology/NonlinearCompressedBitParallelTreeIndex.h>

#include <sax/FromXMLParserHelper.h>
#include <core/xmlApi.hpp>

#include <primitive/xml/Bool.h>
#include <primitive/xml/Integer.h>
#include <primitive/xml/Unsigned.h>
#include <container/xml/ObjectsSet.h>
#include <container/xml/ObjectsMap.h>
#include <container/xml/ObjectsVector.h>

#include <alphabet/xml/RankedSymbol.h>
#include <common/xml/SparseBoolVector.hpp>

namespace core {

template < class SymbolType >
struct xmlApi < indexes::arbology::NonlinearCompressedBitParallelTreeIndex < SymbolType > > {
	static indexes::arbology::NonlinearCompressedBitParallelTreeIndex < SymbolType > parse ( ext::deque < sax::Token >::iterator & input );
	static bool first ( const ext::deque < sax::Token >::const_iterator & input );
	static std::string xmlTagName ( );
	static void compose ( ext::deque < sax::Token > & output, const indexes::arbology::NonlinearCompressedBitParallelTreeIndex < SymbolType > & index );
};

template < class SymbolType >
indexes::arbology::NonlinearCompressedBitParallelTreeIndex < SymbolType > xmlApi < indexes::arbology::NonlinearCompressedBitParallelTreeIndex < SymbolType > >::parse ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
	ext::set < common::ranked_symbol < SymbolType > > alphabet = core::xmlApi < ext::set < common::ranked_symbol < SymbolType > > >::parse ( input );
	ext::map < common::ranked_symbol < SymbolType >, common::SparseBoolVector > data = core::xmlApi < ext::map < common::ranked_symbol < SymbolType >, common::SparseBoolVector > >::parse ( input );
	ext::vector < int > jumps = core::xmlApi < ext::vector < int > >::parse ( input );
	ext::vector < unsigned > repeats = core::xmlApi < ext::vector < unsigned > >::parse ( input );

	indexes::arbology::NonlinearCompressedBitParallelTreeIndex < SymbolType > res ( std::move ( alphabet ), std::move ( data ), std::move ( jumps ), std::move ( repeats ) );

	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, xmlTagName ( ) );
	return res;
}

template < class SymbolType >
bool xmlApi < indexes::arbology::NonlinearCompressedBitParallelTreeIndex < SymbolType > >::first ( const ext::deque < sax::Token >::const_iterator & input ) {
	return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
}

template < class SymbolType >
std::string xmlApi < indexes::arbology::NonlinearCompressedBitParallelTreeIndex < SymbolType > >::xmlTagName ( ) {
	return "NonlinearCompressedBitParallelTreeIndex";
}

template < class SymbolType >
void xmlApi < indexes::arbology::NonlinearCompressedBitParallelTreeIndex < SymbolType > >::compose ( ext::deque < sax::Token > & output, const indexes::arbology::NonlinearCompressedBitParallelTreeIndex < SymbolType > & index ) {
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::START_ELEMENT );
	core::xmlApi < ext::set < common::ranked_symbol < SymbolType > > >::compose ( output, index.getAlphabet ( ) );
	core::xmlApi < ext::map < common::ranked_symbol < SymbolType >, common::SparseBoolVector > >::compose ( output, index.getData ( ) );
	core::xmlApi < ext::vector < int > >::compose ( output, index.getJumps ( ) );
	core::xmlApi < ext::vector < unsigned > >::compose ( output, index.getRepeats ( ) );
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::END_ELEMENT );
}

} /* namespace core */

