/*
 * SparseBoolVector.hpp
 *
 *  Created on: 23. 2. 2017
 *	  Author: Jan Travnicek
 */

#pragma once

#include <core/xmlApi.hpp>
#include "../SparseBoolVector.hpp"

namespace core {

template < >
struct xmlApi < common::SparseBoolVector > {
	static common::SparseBoolVector parse ( ext::deque < sax::Token >::iterator & input );

	static std::string xmlTagName ( ) {
		return "SparseBoolVector";
	}

	static bool first ( const ext::deque < sax::Token >::const_iterator & input ) {
		return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
	}

	static void compose ( ext::deque < sax::Token > & output, const common::SparseBoolVector & input );
};

} /* namespace core */

