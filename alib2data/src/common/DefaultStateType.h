/*
 * DefaultStateType.h
 *
 *  Created on: Nov 29, 2016
 *      Author: Jan Travnicek
 */

#pragma once

#include <object/Object.h>

typedef object::Object DefaultStateType;

