#include <catch2/catch.hpp>

#include <alib/list>

#include "sax/SaxParseInterface.h"
#include "sax/SaxComposeInterface.h"

#include "alphabet/BlankSymbol.h"
#include "alphabet/StartSymbol.h"
#include "alphabet/EndSymbol.h"
#include "alphabet/BarSymbol.h"

#include <common/DefaultSymbolType.h>

#include "factory/XmlDataFactory.hpp"


namespace alphabet {
	template < >
		inline int BarSymbol::instance < int > ( ) {
			return -1;
		}
} /* namespace alphabet */

TEST_CASE ( "Alphabet symbols test", "[unit][data][alphabet]" ) {
	SECTION ( "Order" ) {
		common::ranked_symbol < > rs1 ( DefaultSymbolType ( alphabet::BarSymbol { } ), 0 );
		common::ranked_symbol < > rs2 ( DefaultSymbolType ( alphabet::BarSymbol { } ), 0 );

		CHECK ( rs1 == rs2 );
	}

	SECTION ( "Default values" ) {
		CHECK ( alphabet::BarSymbol::instance < int > ( ) == -1 );
		CHECK ( alphabet::BarSymbol::instance < char > ( ) == '|');
		INFO ( alphabet::BarSymbol::instance < std::string > ( ) );
		CHECK ( alphabet::BarSymbol::instance < std::string > ( ) == "|");
		CHECK ( alphabet::BarSymbol::instance < alphabet::BarSymbol > ( ) == alphabet::BarSymbol ( ) );
		CHECK ( alphabet::BarSymbol::instance < object::Object > ( ) == object::Object ( alphabet::BarSymbol ( ) ) );

	}
}
