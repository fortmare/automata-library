#pragma once
#include <memory>

#include <QtCore/QRectF>

#include <abstraction/Value.hpp>

namespace Utils
{
    QRectF pointsToRect(QPointF a, QPointF b);

    std::shared_ptr<abstraction::Value> generateRandomAutomaton();
    std::shared_ptr<abstraction::Value> generateRandomGrammar();
}
