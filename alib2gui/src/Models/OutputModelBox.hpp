#pragma once
#include <Models/ModelBox.hpp>

class OutputModelBox : public ModelBox {
public:
    explicit OutputModelBox();

    std::string getName() const override;

    bool canHaveOutput() const override { return false; }

    std::shared_ptr<abstraction::Value> evaluate() override;
};


