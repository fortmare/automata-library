/*
 * normalize.hpp
 *
 * Created on: Apr 1, 2013
 * Author: Jan Travnicek
 */

#pragma once

#include <type_traits>

namespace core {

template < typename T >
struct normalize { };

template < class ReturnType >
using normalizationResult = typename std::decay < typename std::result_of < decltype ( & core::normalize < ReturnType >::eval ) ( ReturnType && ) >::type >::type;

} /* namespace core */

