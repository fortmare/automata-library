#pragma once

#include <registry/CastRegistry.hpp>

#include <registration/NormalizationRegistration.hpp>

namespace registration {

template < class To, class From >
class CastRegister {
	registration::NormalizationRegister < To > normalize;

public:
	explicit CastRegister ( ) {
		abstraction::CastRegistry::registerCast < To, From > ( );
	}

	explicit CastRegister ( To ( * castFunction ) ( const From & ) ) {
		abstraction::CastRegistry::registerCastAlgorithm < To, From > ( castFunction );
	}

	~CastRegister ( ) {
		abstraction::CastRegistry::unregisterCast < To, From > ( );
	}
};

} /* namespace registration */

