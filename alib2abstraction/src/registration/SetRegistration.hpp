#pragma once

#include <registry/ContainerRegistry.hpp>

#include <registration/NormalizationRegistration.hpp>

namespace registration {

template < class Param >
class SetRegister {
public:
	explicit SetRegister ( ) {
		abstraction::ContainerRegistry::registerSet < Param > ( );
	}

	~SetRegister ( ) {
		abstraction::ContainerRegistry::unregisterSet < Param > ( );
	}
};

} /* namespace registration */

