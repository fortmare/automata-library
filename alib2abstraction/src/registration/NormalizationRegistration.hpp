#pragma once

#include <registry/NormalizeRegistry.hpp>

#include <alib/registration>

namespace registration {

template < class ReturnType, typename enable = void >
class NormalizationRegister {
};

template < class ReturnType >
class NormalizationRegister < ReturnType, typename std::enable_if < ! std::is_same < ReturnType, core::normalizationResult < ReturnType > >::value >::type > : public ext::Register < std::list < std::unique_ptr < abstraction::NormalizeRegistry::Entry > >::const_iterator > {
public:
	explicit NormalizationRegister ( ) : ext::Register < std::list < std::unique_ptr < abstraction::NormalizeRegistry::Entry > >::const_iterator > ( [=] ( ) {
				return abstraction::NormalizeRegistry::registerNormalize < ReturnType > ( );
			}, [=] ( std::list < std::unique_ptr < abstraction::NormalizeRegistry::Entry > >::const_iterator iter ) {
				abstraction::NormalizeRegistry::unregisterNormalize < ReturnType > ( iter );
			} ) {
	}
};

} /* namespace registration */

