/*
 * NormalizeFactory.hpp
 *
 *  Created on: Jan 1, 2014
 *      Author: Jan Travnicek
 */

#pragma once

#include <alib/type_traits>
#include <utility>

#include <core/normalize.hpp>

namespace factory {

class NormalizeFactory {
public:
	template < class SourceType >
	class normalizer {
		SourceType && m_source;

	public:
		explicit normalizer ( SourceType && source ) : m_source ( std::move ( source ) ) {
		}

		template < class TargetType >
		operator TargetType ( ) {
			if constexpr ( std::is_same < TargetType, SourceType >::value )
				return TargetType ( std::move ( m_source ) );
			else
				return core::normalize < SourceType >::eval ( std::move ( m_source ) );
		}
	};

	template < class SourceType >
	static normalizer < SourceType > normalize ( SourceType && source ) {
		return normalizer < SourceType > ( std::forward < SourceType > ( source ) );
	}

};

} /* namespace factory */

