/*
 * ValueOperationAbstraction.hpp
 *
 *  Created on: 11. 7. 2017
 *	  Author: Jan Travnicek
 */

#pragma once

#include <alib/array>
#include <alib/memory>

#include <common/TypeQualifiers.hpp>

#include <abstraction/ValueHolder.hpp>
#include <common/AbstractionHelpers.hpp>

namespace abstraction {

template < class ReturnType >
class ValueOperationAbstraction : virtual public OperationAbstraction {
public:
	template < typename ... ParamTypes, typename Callable >
	inline std::shared_ptr < abstraction::Value > run_helper ( Callable callback, const ext::array < std::shared_ptr < abstraction::Value >, sizeof ... ( ParamTypes ) > & inputs ) {
		return std::make_shared < abstraction::ValueHolder < ReturnType > > ( abstraction::apply < ParamTypes ... > ( callback, inputs ), ! std::is_lvalue_reference_v < ReturnType > );
	}

	abstraction::TypeQualifiers::TypeQualifierSet getReturnTypeQualifiers ( ) const override {
		return abstraction::TypeQualifiers::typeQualifiers < ReturnType > ( );
	}

	ext::type_index getReturnTypeIndex ( ) const override {
		return ext::type_index ( typeid ( ReturnType ) );
	}

};

template < >
class ValueOperationAbstraction < void > : virtual public OperationAbstraction {
public:
	template < typename ... ParamTypes, typename Callable >
	inline std::shared_ptr < abstraction::Value > run_helper ( Callable callback, const ext::array < std::shared_ptr < abstraction::Value >, sizeof ... ( ParamTypes ) > & inputs ) {
		abstraction::apply < ParamTypes ... > ( callback, inputs );
		return std::make_shared < abstraction::Void > ( );
	}

	ext::type_index getReturnTypeIndex ( ) const override;

	abstraction::TypeQualifiers::TypeQualifierSet getReturnTypeQualifiers ( ) const override;
};

} /* namespace abstraction */

