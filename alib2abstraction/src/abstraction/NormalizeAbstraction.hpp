/*
 * NormalizeAbstraction.hpp
 *
 *  Created on: 11. 7. 2017
 *	  Author: Jan Travnicek
 */

#pragma once

#include <abstraction/NaryOperationAbstraction.hpp>
#include <abstraction/ValueOperationAbstraction.hpp>

#include <factory/NormalizeFactory.hpp>

namespace abstraction {

template < class ReturnType, class ParamType >
class NormalizeAbstraction : virtual public NaryOperationAbstraction < ParamType && >, virtual public ValueOperationAbstraction < ReturnType > {
public:
	std::shared_ptr < abstraction::Value > run ( ) override {
		std::shared_ptr < abstraction::Value > & rawParam = std::get < 0 > ( this->getParams ( ) );
		ParamType && param = retrieveValue < ParamType && > ( rawParam );

		return std::make_shared < abstraction::ValueHolder < ReturnType > > ( factory::NormalizeFactory::normalize ( std::move ( param ) ), true );
	}

};

} /* namespace abstraction */

