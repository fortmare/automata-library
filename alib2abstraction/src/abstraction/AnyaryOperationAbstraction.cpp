/*
 * AnyaryOperationAbstraction.hpp
 *
 *  Created on: 20. 8. 2017
 *	  Author: Jan Travnicek
 */

#include "AnyaryOperationAbstraction.hpp"

namespace abstraction {

void AnyaryOperationAbstractionImpl::attachInput ( const std::shared_ptr < abstraction::Value > & input, size_t index ) {
	if ( m_params.size ( ) < index + 1 )
		m_params.resize ( index + 1 );

	m_params [ index ] = input;
}

void AnyaryOperationAbstractionImpl::detachInput ( size_t index ) {
	if ( index >= m_params.size ( ) )
		throw std::invalid_argument ( "Parameter index " + ext::to_string ( index ) + " out of bounds.");

	m_params [ index ] = nullptr;
}

bool AnyaryOperationAbstractionImpl::inputsAttached ( ) const {
	auto attached_lambda = [ ] ( const std::shared_ptr < abstraction::Value > & param ) {
		return ( bool ) param;
	};

	return std::all_of ( m_params.begin ( ), m_params.end ( ), attached_lambda );
}

std::shared_ptr < abstraction::Value > AnyaryOperationAbstractionImpl::eval ( ) {
	if ( ! inputsAttached ( ) )
		return nullptr;

	return this->run ( );
}

} /* namespace abstraction */
