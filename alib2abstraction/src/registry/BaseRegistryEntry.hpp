/*
 * BaseRegistryEntry.hpp
 *
 *  Created on: 11. 7. 2017
 *	  Author: Jan Travnicek
 */

#pragma once

#include <abstraction/OperationAbstraction.hpp>

namespace abstraction {

class BaseRegistryEntry {
public:
	virtual ~BaseRegistryEntry ( ) = default;

	virtual std::shared_ptr < abstraction::OperationAbstraction > getAbstraction ( ) const = 0;
};

} /* namespace abstraction */

