/*
 * Registry.h
 *
 *  Created on: 10. 7. 2017
 *	  Author: Jan Travnicek
 */

#pragma once

#include <abstraction/OperationAbstraction.hpp>
#include <common/TypeQualifiers.hpp>
#include <common/AlgorithmCategories.hpp>
#include <common/Operators.hpp>
#include "AlgorithmRegistryInfo.hpp"

#include <alib/pair>
#include <alib/tuple>
#include <alib/list>

namespace abstraction {

class Registry {
public:
	static ext::set < ext::pair < std::string, ext::vector < std::string > > > listAlgorithmGroup ( const std::string & group );
	static ext::set < ext::pair < std::string, ext::vector < std::string > > > listAlgorithms ( );

	static ext::list < ext::pair < std::string, bool > > listCastsFrom ( const std::string & type );
	static ext::list < ext::pair < std::string, bool > > listCastsTo ( const std::string & type );
	static ext::list < ext::tuple < std::string, std::string, bool > > listCasts ( );

	static ext::list < ext::tuple < AlgorithmFullInfo, std::string > > listOverloads ( const std::string & algorithm, const ext::vector < std::string > & templateParams );

	static ext::list < ext::pair < Operators::BinaryOperators, AlgorithmFullInfo > > listBinaryOperators ( );
	static ext::list < ext::pair < Operators::PrefixOperators, AlgorithmFullInfo > > listPrefixOperators ( );
	static ext::list < ext::pair < Operators::PostfixOperators, AlgorithmFullInfo > > listPostfixOperators ( );

	static std::shared_ptr < abstraction::OperationAbstraction > getContainerAbstraction ( const std::string & container, const std::string & type );
	static std::shared_ptr < abstraction::OperationAbstraction > getAlgorithmAbstraction ( const std::string & name, const ext::vector < std::string > & templateParams, const ext::vector < std::string > & paramTypes, const ext::vector < abstraction::TypeQualifiers::TypeQualifierSet > & typeQualifiers, AlgorithmCategories::AlgorithmCategory );
	static std::shared_ptr < abstraction::OperationAbstraction > getBinaryOperatorAbstraction ( Operators::BinaryOperators type, const ext::vector < std::string > & paramTypes, const ext::vector < abstraction::TypeQualifiers::TypeQualifierSet > & typeQualifiers, AlgorithmCategories::AlgorithmCategory );
	static std::shared_ptr < abstraction::OperationAbstraction > getPrefixOperatorAbstraction ( Operators::PrefixOperators type, const ext::vector < std::string > & paramTypes, const ext::vector < abstraction::TypeQualifiers::TypeQualifierSet > & typeQualifiers, AlgorithmCategories::AlgorithmCategory );
	static std::shared_ptr < abstraction::OperationAbstraction > getPostfixOperatorAbstraction ( Operators::PostfixOperators type, const ext::vector < std::string > & paramTypes, const ext::vector < abstraction::TypeQualifiers::TypeQualifierSet > & typeQualifiers, AlgorithmCategories::AlgorithmCategory );
	static std::shared_ptr < abstraction::OperationAbstraction > getCastAbstraction ( const std::string & target, const std::string & param );
	static bool isCastNoOp ( const std::string & target, const std::string & param );
	static std::shared_ptr < abstraction::OperationAbstraction > getNormalizeAbstraction ( const std::string & param );
	static bool hasNormalize ( const std::string & param );
	static std::shared_ptr < abstraction::OperationAbstraction > getValuePrinterAbstraction ( const std::string & param );
};

} /* namespace abstraction */


