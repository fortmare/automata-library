/*
 * RTEFromStringLexer.h
 *
 *  Created on: Nov 23, 2013
 *      Author: Jan Travnicek
 */

#pragma once

#include <alib/string>
#include <alib/istream>
#include <common/lexer.hpp>

namespace rte {

class RTEFromStringLexer :  public ext::Lexer < RTEFromStringLexer > {
public:
	enum class TokenType {
		LPAR,
		RPAR,
		PLUS,
		STAR,
		DOT,
		COMMA,
		EMPTY,
		RANK,
		TEOF,
		ERROR
	};

	static Token next(std::istream& input);
};

} /* namespace rte */

