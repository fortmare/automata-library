/*
 * LeftRG.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include "LeftRG.h"
#include <grammar/Grammar.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister < grammar::LeftRG < > > ( );
auto stringReader = registration::StringReaderRegister < grammar::Grammar, grammar::LeftRG < > > ( );

} /* namespace */
