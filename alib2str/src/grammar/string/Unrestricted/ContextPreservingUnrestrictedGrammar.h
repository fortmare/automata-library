/*
 * ContextPreservingUnrestrictedGrammar.h
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#pragma once

#include <grammar/Unrestricted/ContextPreservingUnrestrictedGrammar.h>
#include <core/stringApi.hpp>

#include <grammar/GrammarFromStringLexer.h>

#include <grammar/string/common/GrammarFromStringParserCommon.h>
#include <grammar/string/common/GrammarToStringComposerCommon.h>

namespace core {

template<class SymbolType >
struct stringApi < grammar::ContextPreservingUnrestrictedGrammar < SymbolType > > {
	static grammar::ContextPreservingUnrestrictedGrammar < SymbolType > parse ( std::istream & input );
	static bool first ( std::istream & input );
	static void compose ( std::ostream & output, const grammar::ContextPreservingUnrestrictedGrammar < SymbolType > & grammar );
};

template<class SymbolType >
grammar::ContextPreservingUnrestrictedGrammar < SymbolType > stringApi < grammar::ContextPreservingUnrestrictedGrammar < SymbolType > >::parse ( std::istream & input ) {
	grammar::GrammarFromStringLexer::Token token = grammar::GrammarFromStringLexer::next(input);
	if(token.type != grammar::GrammarFromStringLexer::TokenType::CONTEXT_PRESERVING_UNRESTRICTED_GRAMMAR)
		throw exception::CommonException("Unrecognised ContextPreservingUnrestrictedGrammar token.");

	return grammar::GrammarFromStringParserCommon::parsePreservingCSLikeGrammar < grammar::ContextPreservingUnrestrictedGrammar < SymbolType > > ( input );
}

template<class SymbolType >
bool stringApi < grammar::ContextPreservingUnrestrictedGrammar < SymbolType > >::first ( std::istream & input ) {
	grammar::GrammarFromStringLexer::Token token = grammar::GrammarFromStringLexer::next ( input );
	bool res = token.type == grammar::GrammarFromStringLexer::TokenType::CONTEXT_PRESERVING_UNRESTRICTED_GRAMMAR;
	grammar::GrammarFromStringLexer::putback ( input, token );
	return res;
}

template<class SymbolType >
void stringApi < grammar::ContextPreservingUnrestrictedGrammar < SymbolType > >::compose ( std::ostream & output, const grammar::ContextPreservingUnrestrictedGrammar < SymbolType > & grammar ) {
	output << "CONTEXT_PRESERVING_UNRESTRICTED_GRAMMAR";
	grammar::GrammarToStringComposerCommon::composePreservingCSLikeGrammar ( output, grammar );
}

} /* namespace core */

