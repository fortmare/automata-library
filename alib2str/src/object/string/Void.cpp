/*
 * Void.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include "Void.h"
#include <object/Object.h>

#include <registration/StringRegistration.hpp>

namespace core {

object::Void stringApi < object::Void >::parse ( std::istream & ) {
	throw exception::CommonException("parsing Void from string not implemented");
}

bool stringApi < object::Void >::first ( std::istream & ) {
	return false;
}

void stringApi < object::Void >::compose ( std::ostream & output, const object::Void & ) {
	output << "Void";
}

} /* namespace core */

namespace {

auto stringWrite = registration::StringWriterRegister < object::Void > ( );
auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup < object::Object, object::Void > ( );

} /* namespace */
