/*
 * PrimitiveFromStringLexer.h
 *
 *  Created on: Nov 23, 2013
 *      Author: Jan Travnicek
 */

#pragma once

#include <alib/string>
#include <alib/istream>
#include <common/lexer.hpp>

namespace primitive {

class PrimitiveFromStringLexer : public ext::Lexer < PrimitiveFromStringLexer > {
public:
	enum class TokenType {
		STRING,
		INTEGER,
		TEOF,
		ERROR
	};

	static Token next(std::istream& input);
};

} /* namespace primitive */

