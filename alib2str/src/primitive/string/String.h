/*
 * String.h
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#pragma once

#include <core/stringApi.hpp>

#include <primitive/PrimitiveFromStringLexer.h>

namespace core {

template < >
struct stringApi < std::string > {
	static std::string parse ( std::istream & input );
	static bool first ( std::istream & input );
	static void compose ( std::ostream & output, const std::string & primitive );
};

} /* namespace core */

