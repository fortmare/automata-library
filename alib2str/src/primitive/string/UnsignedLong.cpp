/*
 * UnsignedLong.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include "UnsignedLong.h"
#include <object/Object.h>

#include <registration/StringRegistration.hpp>

namespace core {

unsigned long stringApi < unsigned long >::parse ( std::istream & ) {
	throw exception::CommonException("parsing unsigned long from string not implemented");
}

bool stringApi < unsigned long >::first ( std::istream & ) {
	return false;
}

void stringApi < unsigned long >::compose ( std::ostream & output, unsigned long primitive ) {
	output << primitive;
}

} /* namespace core */

namespace {

auto stringWrite = registration::StringWriterRegister < unsigned long > ( );

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup < object::Object, unsigned long > ( );

} /* namespace */
