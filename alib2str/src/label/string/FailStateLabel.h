/*
 * FailStateLabel.h
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#pragma once

#include <label/FailStateLabel.h>
#include <core/stringApi.hpp>

namespace core {

template < >
struct stringApi < label::FailStateLabel > {
	static label::FailStateLabel parse ( std::istream & input );
	static bool first ( std::istream & input );
	static void compose ( std::ostream & output, const label::FailStateLabel & label );
};

} /* namespace core */

