/*
 * InitialStateLabel.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include "InitialStateLabel.h"
#include <label/InitialStateLabel.h>
#include <primitive/string/String.h>
#include <object/Object.h>

#include <registration/StringRegistration.hpp>

namespace core {

label::InitialStateLabel stringApi < label::InitialStateLabel >::parse ( std::istream & ) {
	throw exception::CommonException("parsing InitialStateLabel from string not implemented");
}

bool stringApi < label::InitialStateLabel >::first ( std::istream & ) {
	return false;
}

void stringApi < label::InitialStateLabel >::compose ( std::ostream & output, const label::InitialStateLabel & label ) {
	stringApi < std::string >::compose ( output, (std::string) label );
}

} /* namespace core */

namespace {

auto stringWrite = registration::StringWriterRegister < label::InitialStateLabel > ( );

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup < object::Object, label::InitialStateLabel > ( );

} /* namespace */
