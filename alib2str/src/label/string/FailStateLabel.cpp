/*
 * FailStateLabel.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include "FailStateLabel.h"
#include <label/FailStateLabel.h>
#include <primitive/string/String.h>
#include <object/Object.h>

#include <registration/StringRegistration.hpp>

namespace core {

label::FailStateLabel stringApi < label::FailStateLabel >::parse ( std::istream & ) {
	throw exception::CommonException("parsing FailStateLabel from string not implemented");
}

bool stringApi < label::FailStateLabel >::first ( std::istream & ) {
	return false;
}

void stringApi < label::FailStateLabel >::compose ( std::ostream & output, const label::FailStateLabel & label ) {
	stringApi < std::string >::compose ( output, (std::string) label );
}

} /* namespace core */

namespace {

auto stringWrite = registration::StringWriterRegister < label::FailStateLabel > ( );

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup < object::Object, label::FailStateLabel > ( );

} /* namespace */
