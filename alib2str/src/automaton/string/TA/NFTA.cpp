/*
 * NFTA.cpp
 *
 * Created on: Apr 13, 2020
 * Author: Jan Travnicek
 */

#include "NFTA.h"
#include <automaton/Automaton.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister < automaton::NFTA < > > ( );
auto stringReader = registration::StringReaderRegister < automaton::Automaton, automaton::NFTA < > > ( );

} /* namespace */
