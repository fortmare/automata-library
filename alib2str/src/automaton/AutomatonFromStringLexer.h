/*
 * AutomatonFromStringLexer.h
 *
 *  Created on: Nov 23, 2013
 *      Author: Jan Travnicek
 */

#pragma once

#include <alib/string>
#include <alib/istream>
#include <common/lexer.hpp>

namespace automaton {

class AutomatonFromStringLexer : public ext::Lexer < AutomatonFromStringLexer > {
public:
	enum class TokenType {
		EPSILON_NFA,
		MULTI_INITIAL_STATE_NFA,
		MULTI_INITIAL_STATE_EPSILON_NFA,
		NFA,
		DFA,
		NFTA,
		DFTA,
		OUT,
		IN,
		EPSILON,
		SEPARATOR,
		LEFT_BRACKET,
		RIGHT_BRACKET,
		NONE,
		COMMA,
		NEW_LINE,
		RANK,
		TEOF,
		ERROR
	};

	static Token next(std::istream & input);
};

} /* namepsace automaton */

