/*
 * RegExpFromStringLexer.h
 *
 *  Created on: Nov 23, 2013
 *      Author: Jan Travnicek
 */

#pragma once

#include <alib/string>
#include <alib/istream>
#include <common/lexer.hpp>

namespace regexp {

class RegExpFromStringLexer : public ext::Lexer < RegExpFromStringLexer > {
public:
	enum class TokenType {
		LPAR,
		RPAR,
		PLUS,
		STAR,
		EPS,
		EMPTY,
		TEOF,
		ERROR
	};

	static Token next(std::istream& input);
};

} /* namespace regexp */

