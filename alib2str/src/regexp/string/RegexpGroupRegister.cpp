/*
 * RegExpGroupGegister.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include <regexp/RegExp.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringReaderGroup = registration::StringReaderGroupRegister < regexp::RegExp > ( );

} /* namespace */
