/*
 * ContainerFromStringLexer.h
 *
 *  Created on: Nov 23, 2013
 *      Author: Jan Travnicek
 */

#pragma once

#include <alib/string>
#include <alib/istream>
#include <common/lexer.hpp>

namespace container {

class ContainerFromStringLexer : public ext::Lexer < ContainerFromStringLexer > {
public:
	enum class TokenType {
		VECTOR_BEGIN,
		VECTOR_END,
		SET_BEGIN,
		SET_END,
		PAIR_BEGIN,
		PAIR_END,
		COMMA,
		TEOF,
		ERROR
	};

	static Token next(std::istream& input);
};

} /* namespace container */

