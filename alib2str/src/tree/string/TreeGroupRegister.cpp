/*
 * TreeGroupGegister.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include <tree/Tree.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringReaderGroup = registration::StringReaderGroupRegister < tree::Tree > ( );

} /* namespace */
