/*
 * BarSymbol.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include "BarSymbol.h"
#include <alphabet/BarSymbol.h>
#include <object/Object.h>

#include <registration/StringRegistration.hpp>

namespace core {

alphabet::BarSymbol stringApi < alphabet::BarSymbol >::parse ( std::istream & ) {
	throw exception::CommonException("parsing BarSymbol from string not implemented");
}

bool stringApi < alphabet::BarSymbol >::first ( std::istream & ) {
	return false;
}

void stringApi < alphabet::BarSymbol >::compose ( std::ostream & output, const alphabet::BarSymbol & ) {
	output << "#|";
}

} /* namespace core */

namespace {

auto stringWrite = registration::StringWriterRegister < alphabet::BarSymbol > ( );

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup < object::Object, alphabet::BarSymbol > ( );

} /* namespace */
