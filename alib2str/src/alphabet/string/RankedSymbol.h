/*
 * RankedSymbol.h
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#pragma once

#include <common/ranked_symbol.hpp>
#include <core/stringApi.hpp>
#include <primitive/string/UnsignedLong.h>

namespace core {

template < class SymbolType >
struct stringApi < common::ranked_symbol < SymbolType > > {
	static common::ranked_symbol < SymbolType > parse ( std::istream & input );
	static bool first ( std::istream & input );
	static void compose ( std::ostream & output, const common::ranked_symbol < SymbolType > & symbol );
};

template < class SymbolType >
common::ranked_symbol < SymbolType > stringApi < common::ranked_symbol < SymbolType > >::parse ( std::istream & ) {
	throw exception::CommonException("Parsing of ranked symbol from string not implemented.");
}

template < class SymbolType >
bool stringApi < common::ranked_symbol < SymbolType > >::first ( std::istream & ) {
	return false;
}

template < class SymbolType >
void stringApi < common::ranked_symbol < SymbolType > >::compose ( std::ostream & output, const common::ranked_symbol < SymbolType > & symbol ) {
	core::stringApi < SymbolType >::compose ( output, symbol.getSymbol ( ) );
	core::stringApi < size_t >::compose ( output, symbol.getRank ( ) );
}

} /* namespace core */

