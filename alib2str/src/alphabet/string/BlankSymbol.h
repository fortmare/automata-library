/*
 * BlankSymbol.h
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#pragma once

#include <alphabet/BlankSymbol.h>
#include <core/stringApi.hpp>

namespace core {

template < >
struct stringApi < alphabet::BlankSymbol > {
	static alphabet::BlankSymbol parse ( std::istream & input );
	static bool first ( std::istream & input );
	static void compose ( std::ostream & output, const alphabet::BlankSymbol & symbol );
};

} /* namespace core */

