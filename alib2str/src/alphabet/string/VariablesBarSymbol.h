/*
 * VariablesBarSymbol.h
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#pragma once

#include <alphabet/VariablesBarSymbol.h>
#include <core/stringApi.hpp>

namespace core {

template < >
struct stringApi < alphabet::VariablesBarSymbol > {
	static alphabet::VariablesBarSymbol parse ( std::istream & input );
	static bool first ( std::istream & input );
	static void compose ( std::ostream & output, const alphabet::VariablesBarSymbol & symbol );
};

} /* namespace core */

