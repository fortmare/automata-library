/*
 * BarSymbol.h
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#pragma once

#include <alphabet/BarSymbol.h>
#include <core/stringApi.hpp>

namespace core {

template < >
struct stringApi < alphabet::BarSymbol > {
	static alphabet::BarSymbol parse ( std::istream & input );
	static bool first ( std::istream & input );
	static void compose ( std::ostream & output, const alphabet::BarSymbol & symbol );
};

} /* namespace core */

