/*
 * BlankSymbol.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include "BlankSymbol.h"
#include <alphabet/BlankSymbol.h>
#include <object/Object.h>

#include <registration/StringRegistration.hpp>

namespace core {

alphabet::BlankSymbol stringApi < alphabet::BlankSymbol >::parse ( std::istream & ) {
	throw exception::CommonException("parsing BlankSymbol from string not implemented");
}

bool stringApi < alphabet::BlankSymbol >::first ( std::istream & ) {
	return false;
}

void stringApi < alphabet::BlankSymbol >::compose ( std::ostream & output, const alphabet::BlankSymbol & ) {
	output << "#B";
}

} /* namespace core */

namespace {

auto stringWrite = registration::StringWriterRegister < alphabet::BlankSymbol > ( );

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup < object::Object, alphabet::BlankSymbol > ( );

} /* namespace */
