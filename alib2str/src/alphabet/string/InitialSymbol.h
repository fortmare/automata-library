/*
 * InitialSymbol.h
 *
 * Created on: Nov 21, 2017
 * Author: Tomas Pecka
 */

#pragma once

#include <alphabet/InitialSymbol.h>
#include <core/stringApi.hpp>

namespace core {

template < >
struct stringApi < alphabet::InitialSymbol > {
	static alphabet::InitialSymbol parse ( std::istream & input );
	static bool first ( std::istream & input );
	static void compose ( std::ostream & output, const alphabet::InitialSymbol & symbol );
};

} /* namespace core */

