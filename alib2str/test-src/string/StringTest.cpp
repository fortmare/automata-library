#include <catch2/catch.hpp>

#include <alib/list>

#include "sax/SaxParseInterface.h"
#include "sax/SaxComposeInterface.h"

#include "string/string/LinearString.h"

#include "factory/StringDataFactory.hpp"

TEST_CASE ( "String Test", "[unit][str][string]" ) {
	SECTION ( "Equal" ) {
		{
			std::string input = "\"aa\"";
			string::LinearString < > string = factory::StringDataFactory::fromString (input);

			std::string output = factory::StringDataFactory::toString(string);

			CAPTURE ( input, output );
			CHECK( input == output );

			string::LinearString < > string2 = factory::StringDataFactory::fromString (output);

			CHECK( string == string2 );
		}
		{
			std::string input = "\"aaaa aaa\"";
			string::LinearString < > string = factory::StringDataFactory::fromString (input);

			std::string output = factory::StringDataFactory::toString(string);

			CAPTURE ( input, output );
			CHECK( input == output );

			string::LinearString < > string2 = factory::StringDataFactory::fromString (output);

			CHECK( string == string2 );
		}
	}
}
