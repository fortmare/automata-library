// EuclideanDistance.cpp
//
//     Created on: 15. 03. 2018
//         Author: Jan Uhlik
//    Modified by:
//
// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.
// Git repository: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library

#include "EuclideanDistance.hpp"

#include <registration/AlgoRegistration.hpp>
#include <common/DefaultTypes.hpp>

namespace {

auto EuclideanDistance = registration::AbstractRegister<graph::heuristic::EuclideanDistance,
                                                        std::function<DefaultWeightType(const ext::pair<
                                                            DefaultCoordinateType,
                                                            DefaultCoordinateType> &,
                                                                                        const ext::pair<
                                                                                            DefaultCoordinateType,
                                                                                            DefaultCoordinateType> &)> >(
    graph::heuristic::EuclideanDistance::euclideanDistanceFunction);

}
