// DefaultSquareGridWeightedEdgeType.hpp
//
//     Created on: 04. 03. 2018
//         Author: Jan Uhlik
//    Modified by:
//
// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.
// Git repository: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library

#pragma once

#include <alib/pair>
#include <edge/weighted/WeightedEdge.hpp>
#include "DefaultSquareGridNodeType.hpp"
#include "DefaultWeightType.hpp"

typedef edge::WeightedEdge<DefaultSquareGridNodeType, DefaultWeightType>
    DefaultSquareGridWeightedEdgeType;

