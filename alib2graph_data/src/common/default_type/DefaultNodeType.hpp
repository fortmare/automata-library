/*
 * DefaultNodeType.h
 *
 *  Created on: Jan 12, 2017
 *      Author: Jan Travnicek
 */

#pragma once

#include <object/Object.h>

typedef object::Object DefaultNodeType;

