/*
 * DefaultEdgeType.h
 *
 *  Created on: Jan 12, 2017
 *      Author: Jan Travnicek
 */

#pragma once

#include <alib/pair>
#include <object/Object.h>
#include "DefaultNodeType.hpp"

typedef ext::pair<DefaultNodeType, DefaultNodeType> DefaultEdgeType;

