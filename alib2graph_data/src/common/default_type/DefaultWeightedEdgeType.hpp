// DefaultWeightedEdgeType.h
//
//     Created on: 01. 03. 2018
//         Author: Jan Uhlik
//    Modified by:
//
// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.
// Git repository: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library

#pragma once

#include <common/default_type/DefaultNodeType.hpp>
#include <common/default_type/DefaultWeightType.hpp>
#include <edge/weighted/WeightedEdge.hpp>

typedef edge::WeightedEdge<DefaultNodeType, DefaultWeightType> DefaultWeightedEdgeType;

