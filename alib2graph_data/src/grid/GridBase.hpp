// GridBase.hpp
//
//     Created on: 29. 03. 2018
//         Author: Jan Uhlik
//    Modified by:
//
// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.
// Git repository: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library

#pragma once

#include <alib/compare>

namespace grid {

/**
 * Represents Grid.
 */
class GridBase {
public:
	virtual ~GridBase ( ) noexcept = default;

// ---------------------------------------------------------------------------------------------------------------------

 public:
// ---------------------------------------------------------------------------------------------------------------------
	friend std::ostream & operator << ( std::ostream & os, const GridBase & instance ) {
		instance >> os;
		return os;
	}

	virtual void operator >> ( std::ostream & os ) const = 0;

	virtual operator std::string ( ) const = 0;
};

} // namespace grid

