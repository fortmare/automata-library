// GridClasses.hpp
//
//     Created on: 23. 02. 2018
//         Author: Jan Uhlik
//    Modified by:
//
// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.
// Git repository: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library

#pragma once

#include "square/SquareGrid4.hpp"
#include "square/SquareGrid8.hpp"
#include "square/WeightedSquareGrid4.hpp"
#include "square/WeightedSquareGrid8.hpp"

