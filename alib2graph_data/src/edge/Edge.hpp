// Edge.hpp
//
//     Created on: 26. 11. 2017
//         Author: Jan Uhlik
//    Modified by:
//
// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.
// Git repository: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library

#pragma once

#include <sstream>
#include <alib/pair>
#include <alib/tuple>
#include <object/Object.h>

#include "EdgeBase.hpp"
#include "EdgeFeatures.hpp"

namespace edge {

template<typename TNode>
class Edge : public ext::pair<TNode, TNode>, public EdgeBase {
// ---------------------------------------------------------------------------------------------------------------------
 public:
  using node_type = TNode;
  using normalized_type = Edge<>;

// ---------------------------------------------------------------------------------------------------------------------
// =====================================================================================================================
// Constructor, Destructor, Operators

 public:
  explicit Edge(TNode _first, TNode _second);

// =====================================================================================================================
// EdgeBase interface

 public:
	auto operator <=> (const Edge &other) const {
		return ext::tie(this->first, this->second) <=> ext::tie(other.first, other.second);
	}

	bool operator == (const Edge &other) const {
		return ext::tie(this->first, this->second) == ext::tie(other.first, other.second);
	}

  void operator>>(std::ostream &ostream) const override;

  explicit operator std::string() const override;

// =====================================================================================================================
 public:

  virtual std::string name() const;

// ---------------------------------------------------------------------------------------------------------------------
};
// =====================================================================================================================

template<typename TNode>
Edge<TNode>::Edge(TNode _first, TNode _second)
    : ext::pair<TNode, TNode>(_first, _second) {

}

// ---------------------------------------------------------------------------------------------------------------------

template<typename TNode>
std::string Edge<TNode>::name() const {
  return "Edge";
}

// ---------------------------------------------------------------------------------------------------------------------

template<typename TNode>
void Edge<TNode>::operator>>(std::ostream &ostream) const {
  ostream << "(" << name() << "(first=" << this->first << ", second=" << this->second << "))";
}

template<typename TNode>
Edge<TNode>::operator std::string() const {
  std::stringstream ss;
  ss << "(" << name() << "(first=" << this->first << ", second=" << this->second << "))";
  return std::move(ss).str();
}

// ---------------------------------------------------------------------------------------------------------------------

} // namespace edge

// =====================================================================================================================

