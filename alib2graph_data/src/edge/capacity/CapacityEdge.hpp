// CapacityEdge.hpp
//
//     Created on: 28. 03. 2018
//         Author: Jan Uhlik
//    Modified by:
//
// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.
// Git repository: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library

#pragma once

#include <ostream>
#include <alib/pair>
#include <object/Object.h>
#include <alib/tuple>

#include <edge/EdgeFeatures.hpp>
#include <edge/EdgeBase.hpp>

namespace edge {

template<typename TNode, typename TCapacity>
class CapacityEdge : public ext::pair<TNode, TNode>, public EdgeBase {
// ---------------------------------------------------------------------------------------------------------------------
 public:
  using node_type = TNode;
  using capacity_type = TCapacity;
  using normalized_type = WeightedEdge<>;

// ---------------------------------------------------------------------------------------------------------------------

 protected:
  TCapacity m_capacity;

// =====================================================================================================================
// Constructor, Destructor, Operators

 public:
  explicit CapacityEdge(TNode _first, TNode _second, TCapacity capacity);

// ---------------------------------------------------------------------------------------------------------------------

  TCapacity capacity() const;
  void capacity(TCapacity &&capacity);

// =====================================================================================================================
// EdgeBase interface

 public:
	auto operator <=> (const CapacityEdge &other) const {
		return ext::tie(this->first, this->second, m_capacity) <=> ext::tie(other.first, other.second, other.m_capacity);
	}

	bool operator == (const CapacityEdge &other) const {
		return ext::tie(this->first, this->second, m_capacity) == ext::tie(other.first, other.second, other.m_capacity);
	}

  void operator>>(std::ostream &ostream) const override;

  explicit operator std::string() const override;

// =====================================================================================================================
 public:

  virtual std::string name() const;

// ---------------------------------------------------------------------------------------------------------------------
};
// =====================================================================================================================

template<typename TNode, typename TCapacity>
CapacityEdge<TNode, TCapacity>::CapacityEdge(TNode _first, TNode _second, TCapacity capacity)
    : ext::pair<TNode, TNode>(_first, _second), m_capacity(capacity) {

}

// ---------------------------------------------------------------------------------------------------------------------

template<typename TNode, typename TCapacity>
TCapacity CapacityEdge<TNode, TCapacity>::capacity() const {
  return m_capacity;
}

template<typename TNode, typename TCapacity>
void CapacityEdge<TNode, TCapacity>::capacity(TCapacity &&capacity) {
  m_capacity = std::forward<TCapacity>(capacity);
}

// ---------------------------------------------------------------------------------------------------------------------

template<typename TNode, typename TCapacity>
std::string CapacityEdge<TNode, TCapacity>::name() const {
  return "CapacityEdge";
}

// ---------------------------------------------------------------------------------------------------------------------

template<typename TNode, typename TCapacity>
void CapacityEdge<TNode, TCapacity>::operator>>(std::ostream &ostream) const {
  ostream << "(" << name() << "(first=" << this->first << ", second=" << this->second << ", weight="
          << m_capacity << "))";
}

template<typename TNode, typename TCapacity>
CapacityEdge<TNode, TCapacity>::operator std::string() const {
  std::stringstream ss;
  ss << "(" << name() << "(first=" << this->first << ", second=" << this->second << ", weight="
     << m_capacity << "))";
  return std::move(ss).str();
}

// ---------------------------------------------------------------------------------------------------------------------

} // namespace edge

// =====================================================================================================================


