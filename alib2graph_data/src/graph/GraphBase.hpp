// GraphBase.hpp
//
//     Created on: 28. 02. 2018
//         Author: Jan Uhlik
//    Modified by:
//
// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.
// Git repository: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library

#pragma once

#include <string>
#include <ostream>

namespace graph {

/**
 * Represents graph.
 */
class GraphBase {
public:
	virtual ~GraphBase ( ) noexcept = default;

// ---------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------
	friend std::ostream & operator << ( std::ostream & os, const GraphBase & instance ) {
		instance >> os;
		return os;
	}

 public:

	virtual void operator >> ( std::ostream & os ) const = 0;

	virtual operator std::string ( ) const = 0;
};

} // namespace graph

