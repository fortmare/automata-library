// GraphFeatures.hpp
//
//     Created on: 01. 03. 2018
//         Author: Jan Uhlik
//    Modified by:
//
// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.
// Git repository: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library

#pragma once

#include <common/DefaultTypes.hpp>

namespace graph {

class GraphBase;

template<typename TNode = DefaultNodeType, typename = DefaultEdgeType>
class GraphInterface;

template<typename TNode = DefaultNodeType, typename = DefaultEdgeType>
class UndirectedGraph;

template<typename TNode = DefaultNodeType, typename = DefaultEdgeType>
class UndirectedMultiGraph;

template<typename TNode = DefaultNodeType, typename = DefaultEdgeType>
class DirectedGraph;

template<typename TNode = DefaultNodeType, typename = DefaultEdgeType>
class DirectedMultiGraph;

template<typename TNode = DefaultNodeType, typename = DefaultEdgeType>
class MixedGraph;

template<typename TNode = DefaultNodeType, typename = DefaultEdgeType>
class MixedMultiGraph;

} // namespace graph

