// GraphInterface.hpp
//
//     Created on: 29. 01. 2018
//         Author: Jan Uhlik
//    Modified by:
//
// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.
// Git repository: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library

#pragma once

#include <alib/set>
#include <alib/vector>
#include <functional>

#include "GraphBase.hpp"

namespace graph {

template<typename TNode, typename TEdge>
class GraphInterface : public GraphBase {
// ---------------------------------------------------------------------------------------------------------------------
 public:
  using node_type = TNode;
  using edge_type = TEdge;

// ---------------------------------------------------------------------------------------------------------------------

  virtual size_t nodeCount() const = 0;
  virtual size_t edgeCount() const = 0;

// ---------------------------------------------------------------------------------------------------------------------

  virtual ext::set<TNode> getNodes() const = 0;
  virtual ext::vector<TEdge> getEdges() const = 0;

// ---------------------------------------------------------------------------------------------------------------------

  virtual ext::set<TNode> successors(const TNode &n) const = 0;
  virtual ext::vector<TEdge> successorEdges(const TNode &n) const = 0;
  virtual ext::set<TNode> predecessors(const TNode &n) const = 0;
  virtual ext::vector<TEdge> predecessorEdges(const TNode &n) const = 0;

// ---------------------------------------------------------------------------------------------------------------------

  virtual std::string name() const = 0;

// ---------------------------------------------------------------------------------------------------------------------
};

// =====================================================================================================================

}

