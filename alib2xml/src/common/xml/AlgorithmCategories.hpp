#pragma once

#include <common/AlgorithmCategories.hpp>
#include <core/xmlApi.hpp>

namespace core {

template < >
struct xmlApi < abstraction::AlgorithmCategories::AlgorithmCategory > {
	static abstraction::AlgorithmCategories::AlgorithmCategory parse ( ext::deque < sax::Token >::iterator & input );
	static bool first ( const ext::deque < sax::Token >::const_iterator & input );
	static std::string xmlTagName ( );
	static void compose ( ext::deque < sax::Token > & output, abstraction::AlgorithmCategories::AlgorithmCategory category );
};

} /* namespace core */

