/*
 * XmlComposerRegistry.hpp
 *
 *  Created on: 28. 8. 2017
 *	  Author: Jan Travnicek
 */

#pragma once

#include <alib/memory>
#include <alib/string>
#include <alib/set>
#include <alib/map>
#include <alib/typeinfo>

#include <abstraction/OperationAbstraction.hpp>

namespace abstraction {

class XmlComposerRegistry {
public:
	class Entry {
	public:
		virtual std::shared_ptr < abstraction::OperationAbstraction > getAbstraction ( ) const = 0;

		virtual ~Entry ( ) = default;
	};

private:
	template < class Param >
	class EntryImpl : public Entry {
	public:
		EntryImpl ( ) = default;

		std::shared_ptr < abstraction::OperationAbstraction > getAbstraction ( ) const override;
	};

	static ext::map < std::string, std::unique_ptr < Entry > > & getEntries ( );

public:
	static void unregisterXmlComposer ( const std::string & param );

	template < class ParamType >
	static void unregisterXmlComposer ( ) {
		std::string param = ext::to_string < ParamType > ( );
		unregisterXmlComposer ( param );
	}

	static void registerXmlComposer ( std::string param, std::unique_ptr < Entry > entry );

	template < class ParamType >
	static void registerXmlComposer ( std::string param ) {
		registerXmlComposer ( std::move ( param ), std::unique_ptr < Entry > ( new EntryImpl < ParamType > ( ) ) );
	}

	template < class ParamType >
	static void registerXmlComposer ( ) {
		std::string param = ext::to_string < ParamType > ( );
		registerXmlComposer < ParamType > ( std::move ( param ) );
	}

	static std::shared_ptr < abstraction::OperationAbstraction > getAbstraction ( const std::string & param );

	static ext::set < std::string > listGroup ( const std::string & group );

	static ext::set < std::string > list ( );
};

} /* namespace abstraction */

#include <abstraction/XmlComposerAbstraction.hpp>

namespace abstraction {

template < class Param >
std::shared_ptr < abstraction::OperationAbstraction > XmlComposerRegistry::EntryImpl < Param >::getAbstraction ( ) const {
	return std::make_shared < abstraction::XmlComposerAbstraction < const Param & > > ( );
}


} /* namespace abstraction */

