/*
 * XmlRegistry.h
 *
 *  Created on: 10. 7. 2017
 *	  Author: Jan Travnicek
 */

#pragma once

#include <abstraction/OperationAbstraction.hpp>

#include <alib/set>
#include <string>

namespace abstraction {

class XmlRegistry {
public:
	static ext::set < std::string > listDataTypes ( );
	static ext::set < std::string > listDataTypeGroup ( const std::string & group );

	static std::shared_ptr < abstraction::OperationAbstraction > getXmlComposerAbstraction ( const std::string & param );
	static std::shared_ptr < abstraction::OperationAbstraction > getXmlParserAbstraction ( const std::string & type );

	static std::shared_ptr < abstraction::OperationAbstraction > getXmlContainerParserAbstraction ( const std::string & container, const std::string & type );
};

} /* namespace abstraction */


