/*
 * XmlParserRegistry.hpp
 *
 *  Created on: 11. 7. 2017
 *	  Author: Jan Travnicek
 */

#pragma once

#include <alib/memory>
#include <alib/string>
#include <alib/map>

#include <abstraction/OperationAbstraction.hpp>

#include <core/xmlApi.hpp>

namespace abstraction {

class XmlParserRegistry {
public:
	class Entry {
	public:
		virtual std::shared_ptr < abstraction::OperationAbstraction > getAbstraction ( ) const = 0;

		virtual ~Entry ( ) = default;
	};

private:
	template < class Return >
	class EntryImpl : public Entry {
	public:
		EntryImpl ( ) = default;

		std::shared_ptr < abstraction::OperationAbstraction > getAbstraction ( ) const override;
	};

	static ext::map < std::string, std::unique_ptr < Entry > > & getEntries ( );

public:
	static void unregisterXmlParser ( const std::string & result );

	template < class ReturnType >
	static void unregisterXmlParser ( ) {
		std::string ret = core::xmlApi < ReturnType >::xmlTagName ( );
		unregisterXmlParser ( ret );
	}

	static void registerXmlParser ( std::string result, std::unique_ptr < Entry > entry );

	template < class ReturnType >
	static void registerXmlParser ( std::string result ) {
		registerXmlParser ( std::move ( result ), std::unique_ptr < Entry > ( new EntryImpl < ReturnType > ( ) ) ) ;
	}

	template < class ReturnType >
	static void registerXmlParser ( ) {
		std::string ret = core::xmlApi < ReturnType >::xmlTagName ( );
		registerXmlParser < ReturnType > ( std::move ( ret ) );
	}

	static std::shared_ptr < abstraction::OperationAbstraction > getAbstraction ( const std::string & typeName );
};

} /* namespace abstraction */

#include <abstraction/XmlParserAbstraction.hpp>

namespace abstraction {

template < class Return >
std::shared_ptr < abstraction::OperationAbstraction > XmlParserRegistry::EntryImpl < Return >::getAbstraction ( ) const {
	return std::make_shared < abstraction::XmlParserAbstraction < Return > > ( );
}

} /* namespace abstraction */

