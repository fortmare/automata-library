/*
 * StringLabel.h
 *
 *  Created on: Mar 26, 2013
 *      Author: Jan Travnicek
 */

#pragma once

#include <core/xmlApi.hpp>

namespace core {

template < >
struct xmlApi < std::string > {
	static std::string parse ( ext::deque < sax::Token >::iterator & input );
	static bool first ( const ext::deque < sax::Token >::const_iterator & input );
	static std::string xmlTagName ( );
	static void compose ( ext::deque < sax::Token > & output, const std::string & data );
};

} /* namespace core */

