/*
 * FromXMLParserHelper.h
 *
 *  Created on: Oct 12, 2013
 *      Author: Jan Travnicek
 */

#pragma once

#include <alib/string>
#include <alib/deque>
#include "Token.h"

namespace sax {

/**
 * Parser used to get UnknownAutomaton from XML parsed into list of Tokens.
 */
class FromXMLParserHelper {
public:
	static bool isToken(ext::deque<Token>::const_iterator input, Token::TokenType type, const std::string& data);
	static bool isTokenType(ext::deque<Token>::const_iterator input, Token::TokenType type);
	static void popToken(ext::deque<Token>::iterator& input, Token::TokenType type, const std::string& data);
	static std::string popTokenData(ext::deque<Token>::iterator& input, Token::TokenType type);
	static std::string getTokenData(ext::deque<Token>::const_iterator input, Token::TokenType type);

	static void skipAttributes ( ext::deque < Token >::const_iterator & input, Token::TokenType type );
	static void skipAttributes ( ext::deque < Token >::iterator & input, Token::TokenType type );
};

} /* namespace sax */


