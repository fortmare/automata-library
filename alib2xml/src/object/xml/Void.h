/*
 * Void.h
 *
 *  Created on: Mar 26, 2013
 *      Author: Jan Travnicek
 */

#pragma once

#include <object/Void.h>
#include <core/xmlApi.hpp>

namespace core {

template < >
struct xmlApi < void > {
	static void parse ( ext::deque < sax::Token >::iterator & input );
	static bool first ( const ext::deque < sax::Token >::const_iterator & input );
	static std::string xmlTagName ( );
	static void compose ( ext::deque < sax::Token > & output );
};

template < >
struct xmlApi < object::Void > {
	static object::Void parse ( ext::deque < sax::Token >::iterator & input );
	static bool first ( const ext::deque < sax::Token >::const_iterator & input );
	static std::string xmlTagName ( );
	static void compose ( ext::deque < sax::Token > & output, const object::Void & data );
};

} /* namespace core */

