/*
 * Multimap.cpp
 *
 * Created on: Apr 1, 2013
 * Author: Jan Travnicek
 */

#include "ObjectsMultimap.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < ext::multimap < object::Object, object::Object > > ( );
auto xmlRead = registration::XmlReaderRegister < ext::multimap < object::Object, object::Object > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, ext::multimap < object::Object, object::Object > > ( );

} /* namespace */
