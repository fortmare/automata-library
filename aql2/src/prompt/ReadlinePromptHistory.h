/*
 * ReadlinePromptHistory.h
 *
 *  Created on: 26. 1. 2019
 *	  Author: Jan Travnicek
 */

#pragma once

#include <string>

class ReadlinePromptHistory {
	static char esc_char [];
	static char essc_str [];
	static size_t esc_char_size;

	static char * descape ( const char * buffer );

	static char * escape ( const char * buffer);

	template < class Callable >
	static void history_transform ( Callable callable );

	std::string m_history_file;

public:
	explicit ReadlinePromptHistory ( std::string history_file );

	ReadlinePromptHistory ( const ReadlinePromptHistory & ) = delete;

	ReadlinePromptHistory ( ReadlinePromptHistory && ) = delete;

	ReadlinePromptHistory & operator = ( const ReadlinePromptHistory & ) = delete;

	ReadlinePromptHistory & operator = ( ReadlinePromptHistory && ) = delete;

	~ ReadlinePromptHistory ( );

	static void readHistory ( const std::string & history_file );

	static void writeHistory ( const std::string & history_file );

	static void addHistory ( const std::string & line );

};

