/*
 * ReadlineLineInterface.cpp
 *
 *  Created on: 20. 3. 2017
 *	  Author: Jan Travnicek
 */

#include "ReadlineLineInterface.h"

#include <iostream>
#include <alib/string>

#include <readline/readline.h>

#include "ReadlinePromptHistory.h"

bool ReadlineLineInterface::readline ( std::string & line, bool first ) {
	char * read = ::readline ( first ? "> ": "+ " );
	if ( read == nullptr ) {
		std::cout << std::endl;
		return false;
	}

	line = read;
	free ( read );

	return true;
}

void ReadlineLineInterface::lineCallback ( const std::string & line ) const {
	if ( m_allowHistory )
		ReadlinePromptHistory::addHistory ( ( std::string ) ext::trim ( line ) );
}
