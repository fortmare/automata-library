/*
 * Prompt.h
 *
 *  Created on: 20. 3. 2017
 *	  Author: Jan Travnicek
 */

#pragma once

#include <deque>

#include <environment/Environment.h>

class Prompt {
	std::deque < std::shared_ptr < cli::LineInterface > > m_lineInterfaces;

	cli::Environment m_environment;

	Prompt ( cli::Environment environment );

public:
	Prompt ( const Prompt & ) = delete;

	Prompt ( Prompt && ) = delete;

	Prompt & operator = ( const Prompt & ) = delete;

	Prompt & operator = ( Prompt && ) = delete;

	static Prompt & getPrompt ( ) {
		static Prompt instance { cli::Environment ( ) };
		return instance;
	}

	template < class LineInterface >
	void appendCharSequence ( LineInterface && lineInterface ) {
		m_lineInterfaces.push_back ( std::make_shared < LineInterface > ( std::forward < LineInterface > ( lineInterface ) ) );
	}

	template < class LineInterface >
	void prependCharSequence ( LineInterface && lineInterface ) {
		m_lineInterfaces.push_front ( std::make_shared < LineInterface > ( std::forward < LineInterface > ( lineInterface ) ) );
	}

	cli::CommandResult run ( );

	cli::Environment & getEnvironment ( ) {
		return m_environment;
	}
};

