project(alt-aql VERSION ${CMAKE_PROJECT_VERSION})
find_package(LibXml2 REQUIRED)
find_package(Readline REQUIRED)
alt_executable(aql2
	DEPENDS alib2elgo alib2graph_algo alib2algo alib2aux alib2raw_cli_integration alib2raw alib2str_cli_integration alib2str alib2graph_data alib2data alib2cli alib2xml alib2common alib2abstraction alib2measure alib2std LibXml2::LibXml2 ${READLINE_LIBRARY}
	INCLUDES ${READLINE_INCLUDE_DIR} ${Tclap_INCLUDE_DIR}
)
