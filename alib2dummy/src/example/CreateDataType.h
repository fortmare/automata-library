/*
 * CreateDataType.h
 *
 *  Created on: Dec 7, 2017
 *      Author: Jan Travnicek
 */

#pragma once
#include "DataType.h"

namespace example {

class CreateDataType {
public:
	static DataType create ( );
};

class CreateDataType2 {
public:
	template < typename T >
	static DataType create ( T a ) {
		return DataType ( a );
	}

	static DataType create ( int a );
};

} /* namespace example */

