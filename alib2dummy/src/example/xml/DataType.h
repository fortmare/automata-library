/*
 * DataType.h
 *
 *  Created on: Dec 8, 2017
 *      Author: Jan Travnicek
 */

#pragma once

#include <example/DataType.h>
#include <core/xmlApi.hpp>

namespace core {

template < >
struct xmlApi < example::DataType > {
	static example::DataType parse ( ext::deque < sax::Token >::iterator & input );
	static bool first ( const ext::deque < sax::Token >::const_iterator & input );
	static std::string xmlTagName ( );
	static void compose ( ext::deque < sax::Token > & output, const example::DataType & data );
};

} /* namespace core */

